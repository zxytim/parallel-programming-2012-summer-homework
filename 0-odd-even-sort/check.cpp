#include <cstdio>
#include <cassert>

int main(int argc, char *argv[])
{
	assert(argc == 2);
	FILE *file = fopen(argv[1], "r");
	assert(file);

	int prev, now, linenu = 0, nwrong = 0;
	fscanf(file, "%d", &prev);
	linenu ++;
	while (fscanf(file, "%d", &now) == 1)
	{
		linenu ++;
		if (prev > now)
		{
			printf("Wrong at line %d: %d %d\n", linenu, prev, now);
			nwrong ++;
		}
		prev = now;
	}

	if (nwrong == 0)
		printf("Right\n");
	else printf("Total mistakes: %d", nwrong);
	fclose(file);
	return 0;
}


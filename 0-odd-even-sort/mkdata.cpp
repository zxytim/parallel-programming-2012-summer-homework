#include <cstdio>
#include <sys/time.h>
#include <cassert>
#include <cstdlib>

void write_num(int num)
{
	fwrite(&num, sizeof(int), 1, stdout);
}
int main(int argc, char *argv[])
{
	timeval tv;
	gettimeofday(&tv, 0);
	srand(tv.tv_sec * 1000000 + tv.tv_usec);

	assert(argc > 1);
	
	int n = atoi(argv[1]);
	assert(n > 0);

	write_num(n);
	for (int i = 0; i < n; i ++)
		write_num(rand());

	return 0;
}


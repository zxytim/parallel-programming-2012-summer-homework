%
% $File: report.tex
% $Date: Thu Aug 16 15:49:04 2012 +0800
% $Author: Xinyu Zhou <zxytim@gmail.com>
%

\documentclass{article}
\usepackage{fontspec}
\usepackage{zhspacing,url,amsmath,amssymb,verbatim}
\zhspacing
\usepackage{listings}
\usepackage[hyperfootnotes=false,colorlinks,linkcolor=blue,anchorcolor=blue,citecolor=blue]{hyperref}
\usepackage[sorting=none]{biblatex}
%\usepackage[dvips]{graphicx}
\usepackage{minted}
\usepackage{subfigure}
\usepackage{indentfirst}
\usepackage{cases}
\usepackage{environ}
\usepackage{array}
\usepackage{graphicx}
\usepackage[top=1in, bottom=1in, left=1.25in, right=1.25in]{geometry}
%\usepackage{tikz}
%\usepackage{dot2texi}

\input{mint-defs.tex}

\newcommand{\figref}[1]{\hyperref[fig:#1]{Figure\ref*{fig:#1}}}
\newcommand{\tableref}[1]{\hyperref[table:#1]{Table\ref*{table:#1}}}
\newcommand{\centerize}[1]{\begin{center} #1 \end{center}}

\newcommand{\cmd}[1]{{\it #1}}
\newcommand{\ccmd}[1]{\centerize{\cmd{#1}}}

\title{Parallel Odd-even Sort}
\author{Xinyu Zhou(周昕宇)\\ Dept. of CST, THU\\ ID: 2011011252}
\date{\today}

\addbibresource{refs.bib}
\begin{document}
\maketitle

\begin{abstract}
	{\bf Odd-even Sort} is a sorting algorithm with time complexity $O(n^2)$ on
	sequential machines.  Here I migrated {\bf Odd-even Sort} parallel version,
	with time complexity $O(\dfrac{n^2}{m})$ on $m$ computation nodes, in order
	to test parallel performance on supercomputer.

	Meanwhile, an algorithm with time complexity $O(\dfrac{n \log{n}}{m})$
	oriented from {\bf Odd-even Sort} with linear merging operations is presented
	for comparison.

	This is {\it homework 1} for course {\it Parallel Programming}

	{\bf Keyword} Odd-even Sort, parallel
\end{abstract}

\tableofcontents

\clearpage

\section{Instruction}
	\subsection{Prerequisite}
		This program uses {\it MPI} as its back end, so you {\bf MUST} have
		an implementation of {\it MPI}, either \cmd{openmpi} or \cmd{intelmpi}
		is advisable.
	\subsection{Compilation}
		Invoke \cmd{make} to compile the source code. 
		Executable is sorted in \cmd{bin/odd-even-sort}, and symbolic linked
		to \cmd{run/odd-even-sort}
	\subsection{Execution}
		Invoke \ccmd{make run} to run with the default setting.
		Running parameters can be set using environment variables \cmd{NP} and \cmd{NN},
        for specifying number of processes and number of numbers to be sorted respectively. 
		Example: \ccmd{NP=8 NN=100000 make run}
		or you can enter \cmd{run} directory and issue \ccmd{mpirun -np <number of processes> ./odd-even-sort <number of numbers>}
	\subsection{Logging}
		logging information are written into \cmd{run/log}. 

		\cmd{ose-np-<NP>-nn-<NN>.log} file contains \cmd{stdout} from \cmd{process 0}.

		if sort succeeds, \cmd{oes-np-<NP>-nn-<NN>.sorted} file will be
		create, containing statistical data including:
		\begin{itemize}
			\item number of processes
			\item number of numbers
			\item time consumption
				\begin{itemize}
					\item for distributing numbers
					\item for main sort process
					\item for checking result
					\item altogether
				\end{itemize}
		\end{itemize}

\section{Design}
	\subsection{Assumption}
		\begin{enumerate}
			\item
				There are $n$ numbers need to be sorted
			\item
				There are $m$ processes running
		\end{enumerate}
	\subsection{Routine}
	Program consists of three subroutines:
	\begin{itemize}
		\item
			{\bf Data Distribution}

			Due to experimental purpose of this program and
			hard-disk io saving, data is generated within each
			process. 

			\cmd{process 0} send randomed random-seed to
			\cmd{process 1 to m}, within which using this 
			random seed for pseudo-random-number-generator
			to generate data. 

			Each process is assigned with $\dfrac{n}{m}$ numbers.

			Here I forced same-even-number of numbers per process
			for convinience of later sorting routine, 
			superfluous positions are filled with 
			\cmd{std::numeric\_limits<int>::max()}.

		\item
			{\bf Sorting}
			
				I implemented two ways of sorting routings:
				\begin{itemize}
					\item 
						{\bf naive Odd-even Sort}

						Sorting consists of consecutive n phases.  In each
						phase, sorting proceeded in $m$ processes concurrently.
						Every process sorted its own numbers using odd even
						sort according to its parity. If the first number in a
						process needs comparing with number ahead, then the
						process ahead send its last number to current process,
						and after comparison on current process, send the
						smaller one between the two back to process ahead.

						Notice that, at most two numbers are needed to be
						exchanged in each process each phase, using blocking
						send and receive after sorting other pairs within
						process is the optimal scheduling, because number
						transforming is mandatory in that phase, we put it as
						late as possible.  Thus there is no need to set
						barriers for synchronization.

					\item
						{\bf Odd-even Sort with Linear Merging}

						Unlike naive algorithm above, sort numbers within a
						process using $O(n*log(n))$ sorting algorithm, such as
						heap-sort or quick-sort, then using naive Odd-even sort
						algorithm above with subsitutions of numbers turned into
						all numbers within a process, and comparison
						between numbers turned into merging numbers in 
						two consecutive processes, and put smaller and greater
						half into process ahead and behind respectively.

						As numbers within a process is ordered, merging method
						used in merge-sort can be extended to merge numbers
						between processes.

				\end{itemize}

		\item
			{\bf Checking}
			
			Checking is similar to sorting, except for seeing
			whether consecutive numbers are in the correct order.

	\end{itemize}

\section{Analysis \& Result}
	\subsection{Analysis on Time Complexity}
	\label{sec:time-complexity}
	\begin{table}[h]
		\centering
		\begin{tabular}{>{\centering\arraybackslash}p{1.2in}|>{\centering\arraybackslash}p{1.5in}|>{\centering\arraybackslash}p{1.5in}}
			& {\bf naive Odd-even Sort} & {\bf Odd-even Sort with Linear Merging} \\\hline
			time complexity &  {$O(\dfrac{n^2}{m})$} &  {$O(\dfrac{n \log{n}}{m})$} \\\hline
			space complexity &  $O(n)$ &  $O(n)$ \\\hline
			{sequential version time complexity} &  $O(n^2)$ &  $O(n \log{n})$ \\\hline
			{sequential version space complexity} &  $O(n)$ &  $O(n)$ \\\hline
			cost & $O(n^2)$ & $O(n \log{n})$ 
		\end{tabular}
		\caption{Time complexity comparison}
	\end{table}

	For {\bf naive Odd-even Sort}, apparently, it is an $O(\frac{n^2}{m})$
	algorithm, for algorithm consists of $n$ phases and each phase is
	$O(\dfrac{n}{m})$. Compared to complexity on sequential machine, which
	is $O(n^2)$, a multiplication factor of $\dfrac{1}{m}$ presented due to
	$m$ process parallel computing. Thus the parallel version of naive
	Odd-even Sort is cost-optimal.

	In {\bf Odd-even Sort with Linear Merging},
	sorting within each process is $O(\dfrac{n}{m}*\log{\dfrac{n}{m}}) \subseteq
	O(\dfrac{n \log{n}}{m})$.
	and a single merging is $O(\dfrac{2 n}{m})$ times $m$ phases, comes up
	with $O(n)$ time complexity. Altogether, this algorithm is dominated by
	the sorting procedure within each process, thus time complexity is
	$O(\dfrac{n \log{n}}{m})$. You can easily infer that, this parallel algorithm
	is also cost-optimal.

	One thing to be noted is that, space consumption of {\bf Odd-even Sort with
	Linear Merging} described above is actually $3 n$ (neglect buffer used 
	by MPI implementation), for three arrays, one for storing intial numbers,
	the other two is send and receive buffer.
	
	\subsection{Result}
	The programs has been both tested both on
	\href{http://i.top500.org/system/177160}{Inspur TS10000 HPC Server} located
	in Tsinghua University and my laptop. 
	{\bf Cluster setting:}
		\begin{itemize}
			\item CPU: Intel Xeon X5670, 2.93 GHz, 6 core
			\item RAM: 32/48 GB
			\item Connection: InfiniBand QDR network
			\item OS: RedHat Linux AS 5.5
			\item Compiler: \cmd{mpic++} from \cmd{mvapich} with
				\cmd{icpc}\footnote{intel c++ compiler} version 11.1
		\end{itemize}

	{\bf Laptop setting:}
		\begin{itemize}
			\item CPU: Intel(R) Core(TM) i7-3610QM CPU @ 2.30GHz
				\begin{itemize}
					\item number of processor: 4
					\item number of cpu thread: 8
				\end{itemize}
			\item RAM: 8.0GB, 7.71GB available
			\item OS: Arch Linux with 3.4.7-1 kernel
			\item Compiler: \cmd{mpic++} from \cmd{openmpi} with 
				\cmd{g++} version 4.7.1
		\end{itemize}

	Here are the results in general:
		\input{data/autogen/figure.general.tex}
		for specific results, see Appedix~\ref{app:spec-rst}.
	
	\clearpage
	\subsection{Analysis on Result}
		\subsubsection{Result on Clusters}
			Graph for clusters result is plotted in log scale on both axis.
			As for {\bf naive Odd-even Sort}, with a nearly linear line plotted
			for large data set, we can infer that this experiment result
			conforms with time complexity $O(\dfrac{n^2}{m})$ and $O(\dfrac{n \log{n}}{m})$ given in
			section~\ref{sec:time-complexity}. 

			For small data set, increment in time consuming as increasing
			number of processes may come from constant omitted in time
			complexity and data transfer among processes, 
			for they dorminates the running time when data set are
			small.

		\subsubsection{Result on Laptop}
			As there are only 4 {\it real} cores in my laptop's CPU, 
			time consumption with less equal to 4 processes is relatively
			conformed to theoretical analysis on both two algorithms.
			Comparing result using 4 and 8 processes respectively, a slight
			lower in time consumption can be seen from graph, which is gained
			by {\it Hyper-Threading}\cite{ht_intel}. For detailed information
			about {\it Hyper-Threading}, please consult online resourses.
		
		
\section{Experience}
	It is the first time I wrote {\it MPI programs}. I used to write {\it
	pthread} programs, but {\it MPI} has a much more different model.
	
	I wrote the {\bf Odd-even Sort with Linear Merging} algorithm first,
	and tested it using laptops in dormitory. As a faster algorithm, 
	large datasets are needed in order to test the efficiency of the program,
	but the bottleneck lies in transmission between laptops connected by
	cables. So a even worse time consumption result may get when more processes
	are used, but distributed among laptops in different room.

	After the clusters were opened to us, much more better experiment results
	had gained, benefited from infiniband between computers in clusters.

	{\bf P.S.} Run an $O(\dfrac{n^2}{m})$ sorting program with large
	datasets on a supercomputer is such an amazing thing...


\clearpage
\appendix
\section{Specific Result}
	\label{app:spec-rst}
	\input{data/autogen/figure.specific.tex}

\section{Source Code}
	\subsection{naive Odd-even Sort}
		\cppsrc{src/noes.cc}
	\subsection{Odd-even Sort with Linear Merging}
		\cppsrc{src/oeslm.cc}

\section{Acknowledgement}
	Thanks to
	\begin{itemize}
		\item
			Prof. and TAs for teaching this course.
		\item
			{\bf \href{http://www.tsinghua.edu.cn/}{Tsinghua University}}
			for providing with computation resourse
		\item
			{\bf \href{http://www.latex-project.org/}{\LaTeX}}
			for typesetting
		\item
			{\bf \href{http://www.gnuplot.info/}{Gnuplot}} 
			for plotting data
	\end{itemize}

% \nocite{cg_textbook}

\clearpage
\printbibliography

\end{document}


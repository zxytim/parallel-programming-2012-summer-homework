/*
 * $File: main.cc
 * $Date: Wed Aug 15 15:20:48 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 *
 * This is the entrance of odd-even-sort program
 */

#include <mpi.h>
#include <sstream>
#include <cctype>
#include <unistd.h>
#include <limits>
#include <cstdlib>
#include <cstdio>
#include <sys/time.h>
#include <stdlib.h>
#include <cmath>
#include <algorithm>
#include <cassert>
#include <string>

#include "utils.hh"
#include "timer.hh"

// USE_FILE_IO and ASYNC_DATA_DIST can not
// be chosen at the same time
//#define USE_FILE_IO
//#define PRINT_OUTPUT
//#define STDIN
#define ASYNC_DATA_DIST
//#define DEBUG_CHECK

const int LOG_MSG_LEN_MAX = 1000;
char log_msg[1000];
FILE *flog;

#define log_printf(fmt, ...) \
	({ \
	snprintf(log_msg, LOG_MSG_LEN_MAX - 1, "[%s] [%s:%d@proc %d] " fmt "\n", get_cur_time_str(), __FUNCTION__, __LINE__, proc_id, ##__VA_ARGS__);\
	 printf("%s", log_msg); \
	 fprintf(flog, "%s", log_msg); \
	 })

/*
 * Consts
 */
#define INPUT_FILE		"sort.in"
#define OUTPUT_FILE		"sort.out"

const int TAG_DIST = 10000000,
	  TAG_NNUMBER = 10000001,
	  TAG_IN_PHASE = 20000002,
	  TAG_GATHER = 10000003, // tag for distribution
	  TAG_SEED = 10000004,
	  TAG_CHECK = 10000006;

/*
 * Variables
 */
int nprocs, proc_id;
int *numbers = NULL; // numbers stored in this
int *number_recv_buf = NULL;
int *number_send_buf = NULL;

size_t nnumber; // number of total numbers

int nnumber_per_proc;


// for reading int
FILE *fin;
const int READ_BUF_SIZE = 10000;
int buf[READ_BUF_SIZE],
	 *cur_buf, *buf_end;

/*
 * Function Declarations
 */
void __log_printf(const char *func, const char *fmt, ...);
void distribute_numbers(); 
void do_sort();


bool read_int(int &x);
void read_init();
void read_finalize();
void gather_result();
void check_result();


void sendint(int num, int dest , int tag)
{
	MPI_Send(&num, 1, MPI_INT, dest, tag, MPI_COMM_WORLD);
}

void sendint(int *num, int cnt, int dest , int tag)
{
	MPI_Send(num, cnt, MPI_INT, dest, tag, MPI_COMM_WORLD);
}

void recvint(int &num, int source, int tag)
{
	MPI_Status status;
	MPI_Recv(&num, 1, MPI_INT, source, tag, MPI_COMM_WORLD, &status);
}

void recvint(int *num, int cnt, int source, int tag)
{
	MPI_Status status;
	MPI_Recv(num, cnt, MPI_INT, source, tag, MPI_COMM_WORLD, &status);
}

const char *get_cur_time_str()
{
	time_t cur_time = time(NULL);
	tm * timeinfo;
	timeinfo = localtime(&cur_time);
	static char timestr[255];
	strftime(timestr, 254, "%F %T", timeinfo);

	return timestr;
}

void alloc_array()
{
	assert(nnumber_per_proc > 0);
	numbers = new int[nnumber_per_proc + 1];
	number_recv_buf = new int[nnumber_per_proc + 1];
	number_send_buf = new int[nnumber_per_proc + 1];
}

void release_array()
{
	delete [] numbers;
	delete [] number_recv_buf;
	delete [] number_send_buf;
}

std::string itoa(int num)
{
	std::stringstream ss;
	ss << num;
	std::string str;
	ss >> str;
	return str;
}

char log_file_name[1000];

void init_logfile()
{
	snprintf(log_file_name, 999, "log/oes-np-%d-nn-%lu.log", nprocs, nnumber);
	flog = fopen(log_file_name, "w");
}

size_t str2size_t(char *str)
{
	size_t ret = 0;
	while (isdigit(*str))
		ret = ret * 10 + *(str ++) - '0';
	return ret;
}

/*
 * Main
 */
int main(int argc, char *argv[])
{

#ifndef USE_FILE_IO
	assert(argc == 2);
	nnumber = str2size_t(argv[1]);
	assert(nnumber > 0);
#endif

	/* init {{{ */
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);

	/* }}} init */

	int ret = 0;

	Timer timer;
	timer.begin();

	Timer timer_all, timer_dist_num,
		  timer_do_sort, timer_check_res;
	try
	{


		timer_all.begin();

		timer_dist_num.begin();
		distribute_numbers();
		timer_dist_num.end();

		timer_do_sort.begin();
		do_sort();
		timer_do_sort.end();

		timer_check_res.begin();
		check_result();
		timer_check_res.end();

		timer_all.end();

	} catch (const char *msg)
	{
		log_printf("%s", msg);
		ret = -1;
	}

	if (ret == 0)
	{
		char fsn[1000];
		snprintf(fsn, 999, "log/oes-np-%d-nn-%lu.sorted", nprocs, nnumber);
		FILE *fsorted = fopen(fsn, "w");
		fprintf(fsorted, 
				"nprocs %d\nnnumber %lu\ntime-dist %lld\ntime-sort %lld\ntime-check %lld\ntime-all %lld\n", 
				nprocs, nnumber, 
				timer_dist_num.duration(),
				timer_do_sort.duration(),
				timer_check_res.duration(),
				timer_all.duration());
		fclose(fsorted);
	}
	fclose(flog);
	release_array();
	MPI_Finalize();
	return ret;
}


void send(void *buf, int count, int dest, int tag)
{
	MPI_Send(buf, count, MPI_INT, dest, tag, MPI_COMM_WORLD);
}

void send_size_t(void *buf, int count, int dest, int tag)
{
	MPI_Send(buf, count, MPI_LONG, dest, tag, MPI_COMM_WORLD);
}


void recv(void *buf, int count, int source, int tag)
{
	MPI_Status status;
	MPI_Recv(buf, count, MPI_INT, source, tag, MPI_COMM_WORLD, &status);
}

void recv_size_t(void *buf, int count, int source, int tag)
{
	MPI_Status status;
	MPI_Recv(buf, count, MPI_LONG, source, tag, MPI_COMM_WORLD, &status);
}


void calc_npp()
{
	long long tmp = (long long)ceil(nnumber / (double)(nprocs));
	if (tmp > std::numeric_limits<int>::max())
	{
		log_printf("number of numbers in one process exceeds transfer limit: total: %lu, per-proc: %lld", nnumber, tmp);
		MPI_Finalize();
		exit(-1);
	}
	nnumber_per_proc = tmp;
}

/*
 * Function Definitions
 * TODO
 */

void distribute_numbers()
{
/* asynclic data distribution */

	if (proc_id == 0)
	{
		init_logfile();
		calc_npp();
		log_printf("number per procs/total number: %d/%lu", nnumber_per_proc, nnumber);
		alloc_array();

		Timer master_seed;
		srand(master_seed.time());

		log_printf("sending random seed");
		for (int i = 1; i < nprocs; i ++)
		{
			int seed = rand();
			send(&seed, 1, i, TAG_SEED);
		}
		for (int i = 0; i < nnumber_per_proc; i ++)
			numbers[i] = rand();
	}
	else
	{
		init_logfile();
		calc_npp();
		alloc_array();

		int seed;
		recv(&seed, 1, 0, TAG_SEED);
		srand(seed);

		log_printf("generating numbers");
		for (int i = 0; i < nnumber_per_proc; i ++)
			numbers[i] = rand();
	}
}


#ifdef DEBUG_CHECK
#define check_order(a, n) ({log_printf("checking order"); __check_order(a, n);})
void __check_order(int *a, int n)
{
#ifdef DEBUG_CHECK
	for (int i = 0; i < n - 1; i ++)
	{
		if (a[i] > a[i + 1])
			log_printf("i=%d,a[i]=%d,a[i+1]=%d",i,a[i],a[i+1]);
		assert(a[i] <= a[i + 1]);
	}
#endif
}
#else
#define check_order(a, n)
#endif

void do_sort()
{

	log_printf("intial sort");
	std::sort(numbers, numbers + nnumber_per_proc);
	for (int phase = 0; phase < nprocs; phase ++)
	{
		log_printf("proc=%d,phase=%d", proc_id, phase);
		if ((phase ^ proc_id) & 1) 
		{
			// 1. send to process behind
			// 2. wait for finish
			// 3. recv result
			if (proc_id < nprocs - 1)
			{
				log_printf("[phase %d] sending numbers", phase);
				send(numbers, nnumber_per_proc, proc_id + 1, TAG_IN_PHASE +
						phase);
				log_printf("[phase %d] waiting for feedback", phase);
				recv(numbers, nnumber_per_proc, proc_id + 1, TAG_IN_PHASE +
						phase);

				check_order(numbers, nnumber_per_proc);
			}
			else log_printf("[phase %d] skip type 0", phase);
		}
		else 
		{
			// 1. recv data from previous process 
			// 2. merge
			// 3. send data back to previous process
			if (proc_id > 0)
			{
				log_printf("[phase %d] wating for numbers", phase);
				recv(number_recv_buf, nnumber_per_proc, proc_id - 1, TAG_IN_PHASE + phase);

				//for (int i = 0; i < nnumber_per_proc - 1; i ++)
				//	assert(number_recv_buf[i] <= number_recv_buf[i + 1]);

				log_printf("[phase %d] first merge", phase);
				// merge
				int p = 0, q = 0, r = 0;

				int *a = numbers,
					*b = number_recv_buf,
					*c = number_send_buf;

				check_order(a, nnumber_per_proc);
				check_order(b, nnumber_per_proc);
				while (r < nnumber_per_proc && 
						p < nnumber_per_proc && 
						q < nnumber_per_proc)
				{
					if (a[p] < b[q])
						c[r ++] = a[p ++];
					else c[r ++] = b[q ++];

#ifdef DEBUG_CHECK
					if (r > 2)
					{
						if (c[r - 1] < c[r - 2])
							log_printf("[phase %d] r=%d,c[r]=%d,c[r-1]=%d", phase, r, c[r], c[r - 1]);
						assert(c[r - 1] >= c[r - 2]);
					}
#endif
				}

				check_order(c, r);

				// send the smaller half back to proc_id - 1
				log_printf("[phase %d] send partial result", phase);
				send(c, nnumber_per_proc, proc_id - 1, TAG_IN_PHASE + phase);

				// merge the rest
				log_printf("[phase %d] second merge", phase);
				r = 0;
				while (p < nnumber_per_proc && q < nnumber_per_proc)
				{
					if (a[p] < b[q])
						c[r ++] = a[p ++];
					else c[r ++] = b[q ++];
				}
				while (p < nnumber_per_proc)
					c[r ++] = a[p ++];
				while (q < nnumber_per_proc)
					c[r ++] = b[q ++];

				assert(r == nnumber_per_proc);

				check_order(c, r);

				log_printf("[phase %d] saving result", phase);
				memcpy(a, c, sizeof(a[0]) * r);

#ifdef DEBUG_CHECK
				assert(r == nnumber_per_proc);
				for (int i = 0; i < r; i ++)
				{
					if (a[i] != c[i])
						log_printf("[phase %d] i=%d,a[i]=%d,c[i]=%d",proc_id, i,a[i],c[i]);
					assert(a[i] == c[i]);
				}
#endif

				check_order(a, r);
			}
			else log_printf("[phase %d] skip type 1", phase);
		}
	}
}

#define _check_order(a, n) do { log_printf("checking order"); __check_order(a, n);} while (0)

void __check_order(int *num, int n)
{
	for (int i = 1; i < n; i ++)
	{
		if (num[i - 1] > num[i])
			log_printf("i = %d, num[i- 1] = %d, num[i] = %d", i, num[i - 1], num[i]);
		assert(num[i - 1] <= num[i]);
	}
}

void check_result()
{
	if (proc_id == 0)
		log_printf("checking result");

	if (proc_id == 0)
		log_printf("asynclic check");
		_check_order(numbers, nnumber_per_proc);
		if ((proc_id & 1))
		{
			if (proc_id > 0)
			{
				sendint(numbers[0], proc_id - 1, TAG_CHECK);
			}
		}
		if (!(proc_id & 1))
		{
			if (proc_id < nprocs - 1)
			{
				int last_num = 0;
				recvint(last_num, proc_id + 1, TAG_CHECK);
				assert(numbers[nnumber_per_proc - 1] <= last_num);
			}
		}

	MPI_Barrier(MPI_COMM_WORLD);
	if (proc_id == 0)
		log_printf("seq sorted");
}

void gather_result()
{
	if (proc_id == 1)
	{
#ifdef USE_FILE_IO
		FILE *fout = fopen(OUTPUT_FILE, "w");
#endif

		size_t cnt = 0;

		int *rbuf = number_recv_buf;
		int prev = std::numeric_limits<int>::min();
		for (int i = 1; i < nprocs; i ++)
		{
			log_printf("gathering result from proc %d", i);
			recv(rbuf, nnumber_per_proc, i, TAG_GATHER);
			log_printf("received from proc %d", i);
			for (int j = 0; j < nnumber_per_proc && cnt < nnumber; j ++, cnt ++)
			{
				if (rbuf[j] < prev)
					log_printf("wrong on data from proc %d, index %d: prev=%d, now=%d", i, j, prev, rbuf[j]);

				assert(rbuf[j] >= prev);
				prev = rbuf[j];
#if defined(USE_FILE_IO) && defined(PRINT_OUTPUT)
				fprintf(fout, "%d\n", rbuf[j]);
#endif
			}
		}
		log_printf("gathering and checking finished");
		log_printf("numbers are sorted");


#ifdef USE_FILE_IO
		fclose(fout);
#endif

	}
	else
	{
		log_printf("sending result back to main proc");
		check_order(numbers, nnumber_per_proc);
		send(numbers, nnumber_per_proc, 0, TAG_GATHER);
	}
}


void read_init()
{
#ifdef STD_INPUT
	fin = stdin;
#else
	fin = fopen(INPUT_FILE, "r");
	if (fin == NULL)
		throw "can not open " INPUT_FILE ;
#endif

	cur_buf = buf_end = buf + READ_BUF_SIZE;
}

bool read_int(int &num)
{
	if (cur_buf == buf_end)
	{
		size_t size = fread(buf, 1, READ_BUF_SIZE, fin) / sizeof(int);
		buf_end = buf + size;
		cur_buf = buf;
		if (buf_end == buf)
			cur_buf = NULL;
	}


	if (cur_buf == NULL)
		return false;

	num = *(cur_buf ++);
	return true;
}

void read_finalize()
{
	fclose(fin);
}

/**
 * vim: syntax=cpp foldmethod=marker
 */


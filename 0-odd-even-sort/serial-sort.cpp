#include <cstdio>
#include <cassert>
#include <sys/time.h>
#include <cstdlib>
#include <algorithm>

void usage()
{
	printf("Usage: <program> <0|1> <number of numbers>\n      0 for O(n^2), 1 for O(n*logn)\n");
}

int main(int argc, char *argv[])
{
	if (argc != 3)
	{
		usage();
		return 0;
	}
	timeval tv;
	gettimeofday(&tv, 0);
	srand(tv.tv_sec * 1000000 + tv.tv_usec);

	int n = atoi(argv[2]);
	assert(n > 0);
	printf("n = %d\n", n);

	int *data = new int[n];
	for (int i = 0; i < n; i ++)
		data[i] = rand();
	
	if (argv[1][0] == '0')
	{
		for (int phase = 0; phase < n; phase ++)
		{
			int start = phase & 1;
			if (start == 0)
				start = 2;
			for (int i = start; i < n; i += 2)
				if (data[i] < data[i - 1])
					std::swap(data[i], data[i - 1]);
		}
	}
	else std::sort(data, data + n);

	for (int i = 0; i < n - 1; i ++)
		assert(data[i] <= data[i + 1]);
	printf("finished\n");
}


/*
 * $File: main.cc
 * $Date: Tue Aug 14 23:10:34 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 *
 * This is the entrance of odd-even-sort program
 */

#include <mpi.h>
#include <sstream>
#include <cctype>
#include <unistd.h>
#include <limits>
#include <cstdlib>
#include <cstdio>
#include <sys/time.h>
#include <stdlib.h>
#include <cmath>
#include <algorithm>
#include <cassert>
#include <string>

#include "utils.hh"
#include "timer.hh"

//#define SEQUENTIAL_CHECK // otherwise use asynclic check

const int LOG_MSG_LEN_MAX = 1000;
char log_msg[1000];
FILE *flog;

#define log_printf(fmt, ...) \
	do{ \
	snprintf(log_msg, LOG_MSG_LEN_MAX - 1, "[%s] [%s:%d@proc %d] " fmt "\n", get_cur_time_str(), __FUNCTION__, __LINE__, proc_id, ##__VA_ARGS__);\
	 printf("%s", log_msg); \
	 fprintf(flog, "%s", log_msg); \
	 } while (0)

const char *get_cur_time_str()
{
	time_t cur_time = time(NULL);
	tm * timeinfo;
	timeinfo = localtime(&cur_time);
	static char timestr[255];
	strftime(timestr, 254, "%F %T", timeinfo);

	return timestr;
}


enum Tag
{
	TAG_SEED = 10000000,
	TAG_IN_PHASE,
	TAG_CHECK
};
/*
 * Variables
 */
int nprocs, proc_id;
size_t nnumber; // number of total numbers
int nnpp; // number of numbers per process

int *number_pool; 
int *number;
// number is actually `number_pool + 1`,
// for check_result convenience
		

void sendint(int num, int dest , int tag)
{
	MPI_Send(&num, 1, MPI_INT, dest, tag, MPI_COMM_WORLD);
}

void sendint(int *num, int cnt, int dest , int tag)
{
	MPI_Send(num, cnt, MPI_INT, dest, tag, MPI_COMM_WORLD);
}

void recvint(int &num, int source, int tag)
{
	MPI_Status status;
	MPI_Recv(&num, 1, MPI_INT, source, tag, MPI_COMM_WORLD, &status);
}

void recvint(int *num, int cnt, int source, int tag)
{
	MPI_Status status;
	MPI_Recv(num, cnt, MPI_INT, source, tag, MPI_COMM_WORLD, &status);
}


char log_file_name[1000];

void init_logfile()
{
	snprintf(log_file_name, 999, "log/oes-np-%d-nn-%lu.log", nprocs, nnumber);
	flog = fopen(log_file_name, "w");
}
void distribute_numbers()
{
	init_logfile();
	nnpp = (long long)ceil(nnumber / (double)(nprocs));

	// for even number
	nnpp += (nnpp & 1);

	log_printf("number per procs/total number: %d/%lu", nnpp, nnumber);

	number_pool = new int [nnpp + 1];
	number = number_pool + 1;

	if (proc_id == 0)
	{
		Timer master_seed;
		srand(master_seed.time());
		log_printf("sending random data");
		for (int i = 1; i < nprocs; i ++)
		{
			int seed = rand();
			sendint(seed, i, TAG_SEED);
		}
		log_printf("generating data");
		for (int i = 0; i < nnpp; i ++)
			number[i] = rand();
	}
	else
	{
		int seed;
		recvint(seed, 0, TAG_SEED);
		srand(seed);

		log_printf("generating data");
		for (int i = 0; i < nnpp; i ++)
			number[i] = rand();
	}
}

void swap(int &a, int &b)
{
	int t = a;
	a = b;
	b = t;
}

void do_sort()
{
	log_printf("start sort");
	int first_num;
	size_t unit_percent = nnumber / 20;
	for (size_t phase = 0; phase < nnumber; phase ++)
	{
#define log_phase(fmt, ...) \
		do { \
			if (phase % unit_percent != 0) \
				break; \
			log_printf(fmt "<phase %lu/%lu %d%%>", ##__VA_ARGS__, phase, nnumber, (int)floor(phase / (double)nnumber * 100)); \
		} while (0)

		log_phase("");

		int pairity = phase & 1; // 1 is active

		for (int i = pairity + 1; i < nnpp; i += 2)
		{
			if (number[i - 1] > number[i])
				swap(number[i - 1], number[i]);
		}
		
		if (pairity == 1)
		{
			for (int i = 0; i < 2; i ++)
			{
				if ((proc_id & 1) == i)
				{
					if (proc_id > 0)
					{
						recvint(first_num, proc_id - 1, TAG_IN_PHASE);
						if (first_num > number[0])
							swap(first_num, number[0]);
						sendint(first_num, proc_id - 1, TAG_IN_PHASE);
					}
				}
				else
				{
					if (proc_id < nprocs - 1)
					{
						sendint(number[nnpp - 1], proc_id + 1, TAG_IN_PHASE);
						recvint(number[nnpp - 1], proc_id + 1, TAG_IN_PHASE);
					}
				}
			}
		}
	}
}

#define check_order(a, n) do { log_printf("checking order"); __check_order(a, n);} while (0)

void __check_order(int *num, int n)
{
	for (int i = 1; i < n; i ++)
	{
		if (num[i - 1] > num[i])
			log_printf("i = %d, num[i- 1] = %d, num[i] = %d", i, num[i - 1], num[i]);
		assert(num[i - 1] <= num[i]);
	}
}

size_t str2size_t(char *str)
{
	size_t ret = 0;
	while (isdigit(*str))
		ret = ret * 10 + *(str ++) - '0';
	return ret;
}

void check_result()
{
	if (proc_id == 0)
		log_printf("checking result");
#ifdef SEQUENTIAL_CHECK
	if (proc_id == 0)
	{
		log_printf("sequential check");
		check_order(number, nnpp);
		for (int i = 1; i < nprocs; i ++)
		{
			number[-1] = number[nnpp - 1];
			recvint(number, nnpp, i, TAG_CHECK);
			check_order(number - 1, nnpp + 1);
		}
	}
	else
	{
		sendint(number, nnpp, 0, TAG_CHECK);
	}
#else
	/* asynclic check {{{ */
	if (proc_id == 0)
		log_printf("asynclic check");
	check_order(number, nnpp);
	if ((proc_id & 1))
	{
		if (proc_id > 0)
		{
			sendint(number[0], proc_id - 1, TAG_CHECK);
		}
	}
	if (!(proc_id & 1))
	{
		if (proc_id < nprocs - 1)
		{
			int last_num = 0;
			recvint(last_num, proc_id + 1, TAG_CHECK);
			assert(number[nnpp - 1] <= last_num);
		}
	}
	/* }}} */
#endif

	if (proc_id == 0)
		log_printf("seq sorted");
}

int main(int argc, char *argv[])
{
	assert(argc == 2);
	nnumber = str2size_t(argv[1]);
	assert(nnumber > 0);

	/* init {{{ */
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);

	/* }}} init */

	Timer timer_all, timer_dist_num,
		  timer_do_sort, timer_check_res;

	timer_all.begin();

	timer_dist_num.begin();
	distribute_numbers();
	timer_dist_num.end();

	timer_do_sort.begin();
	do_sort();
	timer_do_sort.end();

	timer_check_res.begin();
	check_result();
	timer_check_res.end();

	timer_all.end();

	delete [] number_pool;
	number = NULL;

	char fsn[1000];
	snprintf(fsn, 999, "log/oes-np-%d-nn-%lu.sorted", nprocs, nnumber);
	FILE *fsorted = fopen(fsn, "w");
	fprintf(fsorted, 
			"nprocs %d\nnnumber %lu\ntime-dist %lld\ntime-sort %lld\ntime-check %lld\ntime-all %lld\n", 
			nprocs, nnumber, 
			timer_dist_num.duration(),
			timer_do_sort.duration(),
			timer_check_res.duration(),
			timer_all.duration());
	fclose(fsorted);

	fclose(flog);

	MPI_Finalize();

	return 0;
}


/*
 * $File: utils.cc
 * $Date: Fri Aug 10 20:20:57 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include <sys/time.h>

#include "utils.hh"

#define For(i, n) for (int i = 0; i < (n); i ++)

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


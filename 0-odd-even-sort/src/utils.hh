/*
 * $File: utils.hh
 * $Date: Fri Aug 10 20:20:51 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __MISC_HH__
#define __MISC_HH__

#include <cstdlib>

inline bool is_odd(const int &p)
{
	return p & 1;
}


#endif // __MISC_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


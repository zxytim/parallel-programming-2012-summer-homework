#!/bin/bash
cd run
for scheme in "1 1" "1 2" "1 4" "1 8" "1 16" "1 32" "1 64" "1 128" "1 256" "2 1" "2 2" "2 4" "2 8" "2 16" "2 32" "2 64" "2 128" "2 256" "3 1" "3 2" "3 4" "3 8" "3 16" "3 32" "3 64" "3 128" "3 256" "4 1" "4 2" "4 4" "4 8" "4 16" "4 32" "4 64" "4 128" "4 256" "5 1" "5 2" "5 4" "5 8" "5 16" "5 32" "5 64" "5 128" "5 256" "6 1" "6 2" "6 4" "6 8" "6 16" "6 32" "6 64" "6 128" "6 256" "7 1" "7 2" "7 4" "7 8" "7 16" "7 32" "7 64" "7 128" "7 256" "8 1" "8 2" "8 4" "8 8" "8 16" "8 32" "8 64" "8 128" "8 256"; do
	nproc=`echo $scheme | cut -d' ' -f1`
	nnum=$(python -c "print(`echo $scheme | cut -d' ' -f2` * 10000)")
	log_prefix=log/oes-np-$nproc-nn-$nnum
	[ -e $log_prefix.sorted ] && continue
	echo $nproc $nnum
	mpirun -np $nproc ./odd-even-sort $nnum 2>log/oes-np-$nproc-nn-$nnum.stderr | tee log/oes-np-$nproc-nn-$nnum.stdout
done

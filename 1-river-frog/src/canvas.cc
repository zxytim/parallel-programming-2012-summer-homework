/*
 * $File: canvas.cc
 * $Date: Mon Aug 20 13:29:41 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include <iostream>
#include <cassert>

#include <gdkmm/general.h> // set_source_pixbuf
#include <glibmm/fileutils.h>
#include <gdk/gdkkeysyms.h>


#include "canvas.hh"
#include "game.hh"

Canvas::Canvas(Game *game) :
	m_width_ratio(1),
	m_height_ratio(1),
	m_game(game)
{
	//set_resize_mode(Gtk::RESIZE_QUEUE);
}

Canvas::~Canvas()
{
}


void Canvas::set_scale(real_t width_ratio, real_t height_ratio)
{
	m_width_ratio = width_ratio, m_height_ratio = height_ratio;
}

void Canvas::put_img(ImageDrawerble * obj, DrawPosition position)
{
	obj->set_size(obj->swidth() * m_width_ratio, obj->sheight() * m_height_ratio);

	sassert(obj->width() <= m_game->width());
	sassert(obj->height() <= m_game->height());

	put(obj->m_img, position.x * m_width_ratio, position.y * m_height_ratio);
}

void Canvas::move_img(ImageDrawerble * obj, DrawPosition position)
{
	obj->set_size(obj->swidth() * m_width_ratio, obj->sheight() * m_height_ratio);

	sassert(obj->width() <= m_game->width());
	sassert(obj->height() <= m_game->height());

	move(obj->m_img, position.x * m_width_ratio, position.y * m_height_ratio);
}

bool ImageDrawerble::set_size(int width, int height)
{
	if (width == -1 && height == -1)
		return true;
	if (width == -1 && height == m_height)
		return true;
	if (width == m_width && height == -1)
		return true;
	if (width == m_width && m_height == height)
		return true;

	if (m_swidth == -1)
		m_swidth = width;
	if (m_sheight == -1)
		m_sheight = height;

	if (width != -1) m_width = width;
	if (height != -1) m_height = height;

	if (width == -1) width = m_width;
	if (height == -1) height = m_height;

	if (width == -1 || height == -1)
		return false;

	Glib::RefPtr<Gdk::Pixbuf> buf = m_img_orig.get_pixbuf()->copy();
	m_img.set(buf->scale_simple(width, height, Gdk::INTERP_HYPER));

	return true;
}

bool ImageDrawerble::set_img(const char *img_path)
{
	m_img_orig.set(img_path);
	m_img.set(m_img_orig.get_pixbuf());

	return true;
}

void ImageDrawerble::save_size()
{
	m_swidth = m_width;
	m_sheight = m_height;
}

ImageDrawerble::ImageDrawerble() :
	m_width(-1), m_height(-1),
	m_swidth(-1), m_sheight(-1)
{
}





/**
 * vim: syntax=cpp11 foldmethod=marker
 */


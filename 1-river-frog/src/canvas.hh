/*
 * $File: canvas.hh
 * $Date: Mon Aug 20 13:00:00 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __CANVAS_HH__
#define __CANVAS_HH__

#include <gtkmm/drawingarea.h>
#include <gdkmm/pixbuf.h>
#include <gtkmm/image.h>
#include <gtkmm/fixed.h>
#include <vector>

#include "util.hh"

struct DrawPosition
{
	int x, y;
};

class GameInfo;
class Game;

class Canvas;
class ImageDrawerble
{
	protected:
		Gtk::Image m_img_orig;

		int m_width, m_height; // real pixbuf size
		int m_swidth, m_sheight; // saved 
	public:
		ImageDrawerble();
		int width() const { return m_width; }
		int height() const { return m_height; }
		int swidth() const { return m_swidth; }
		int sheight() const { return m_sheight; }
		Gtk::Image m_img;
		bool set_size(int width, int height);
		void save_size();
		bool set_img(const char *img_path);
};

class Canvas : public Gtk::Fixed
{
	public:
		Canvas(Game *game);
		virtual ~Canvas();
		int width() const { return get_width(); }
		int height() const { return get_height(); }
		void put_img(ImageDrawerble * obj, DrawPosition position);
		void move_img(ImageDrawerble * obj, DrawPosition position);
		void set_scale(real_t width_ratio, real_t height_ratio);

	protected:
		real_t m_width_ratio,
			   m_height_ratio;
		Game *m_game;
};

#endif // __CANVAS_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


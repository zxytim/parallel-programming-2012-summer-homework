/*
 * $File: game.cc
 * $Date: Fri Aug 24 09:56:11 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include <algorithm>
#include <limits>
#include <queue>

#include <gtkmm.h>

#include "game.hh"
#include "canvas.hh"



void Game::start()
{
	m_game_status = GAME_RUNNING;

	set_title("River and Frog Arcade Game");

	set_default_size(m_width, m_height);
	set_resizable(true);
	//set_size_request(DEFAULT_WIDTH, DEFAULT_HEIGHT);
	//set_decorated(false);


	m_game_info = new GameInfo(this, m_nlayer);

	// canvas setting
	real_t width_ratio = width() / (real_t)m_width,
		   height_ratio = height() / (real_t)m_height;
	m_canvas.set_scale(width_ratio, height_ratio);

	// background
	m_canvas.put_img(m_game_info->m_bg, get_draw_position(
				m_game_info->m_nlayer + 1, m_width / 2, m_game_info->m_bg->swidth(),
				m_game_info->m_bg->sheight()));
	for (size_t i = 0; i < m_game_info->m_layers.size(); i ++)
	{
		Layer *layer = m_game_info->m_layers[i];
		for (size_t j = 0; j < layer->m_wood.size(); j ++)
		{
			Wood *wood = layer->m_wood[j];
			m_canvas.put_img(wood, 
					get_draw_position(layer->m_layer_id, wood->m_position, wood->swidth(), wood->sheight()));
		}
	}

	for (size_t i = 0; i < m_game_info->m_frog.size(); i ++)
	{
		Frog *frog = m_game_info->m_frog[i];
		m_canvas.put_img(frog, 
				get_draw_position(frog->m_layer, frog->get_position(), frog->swidth(), frog->sheight()));
	}

	m_threads.resize(m_game_info->m_layers.size());
	m_targs.resize(m_game_info->m_layers.size());

	add(m_canvas);

	//m_canvas.show_all();

	show_all();


	slot_advc = sigc::mem_fun(*this, &Game::advance);

	refresh_interval = 1000.0 / m_fps;

	conn_advc = Glib::signal_timeout().connect(slot_advc, refresh_interval);

	timer.begin();
}

Game::Game(int nplayer) :
	m_canvas(this),
	m_nplayer(nplayer)
{
	m_game_info = NULL;
	m_col_type = COLLISION_PHYSICAL;
	set_size(DEFAULT_WIDTH, DEFAULT_HEIGHT);
	m_fps = Fps;
	m_speedup = 1.0;

	srand(timer.time());
}

Game::~Game()
{
	if (m_game_info)
		delete m_game_info;
}

static int transform_key(int key)
{
	if (key == GDK_KEY_Up) return P0_KEY_UP;
	if (key == GDK_KEY_Down) return P0_KEY_DOWN;
	if (key == GDK_KEY_Left) return P0_KEY_LEFT;
	if (key == GDK_KEY_Right) return P0_KEY_RIGHT;

	if (key == GDK_KEY_w) return P1_KEY_UP;
	if (key == GDK_KEY_s) return P1_KEY_DOWN;
	if (key == GDK_KEY_a) return P1_KEY_LEFT;
	if (key == GDK_KEY_d) return P1_KEY_RIGHT;

	return KEY_EMPTY;
}

bool Game::on_key_press_event(GdkEventKey *event)
{
	int key = transform_key(event->keyval);
	switch (key)
	{
		case P0_KEY_UP:
		case P0_KEY_DOWN:
		case P0_KEY_LEFT:
		case P0_KEY_RIGHT:
			try
			{
				m_game_info->m_frog[0]->move(key);
			} 
			catch (GameSigFrogDie)
			{
				m_game_info->m_frog[0]->reset(false);
			}
			break;

		case P1_KEY_LEFT :
		case P1_KEY_RIGHT :
		case P1_KEY_UP : 
		case P1_KEY_DOWN :
			if (nplayer() == 1)
				break;
			try
			{
				m_game_info->m_frog[1]->move(key / 16);
			} 
			catch (GameSigFrogDie)
			{
				m_game_info->m_frog[1]->reset(false);
			}
			break;
	}
	return true;
}

bool Game::on_key_release_event(GdkEventKey *event)
{

	int key = transform_key(event->keyval);
	switch (key)
	{
		case P0_KEY_LEFT:
			m_game_info->m_frog[0]->left_released();
			break;
		case P0_KEY_RIGHT:
			m_game_info->m_frog[0]->right_released();
			break;

		case P1_KEY_LEFT :
			if (nplayer() == 1)
				break;
			m_game_info->m_frog[1]->left_released();
			break;
		case P1_KEY_RIGHT :
			if (nplayer() == 1)
				break;
			m_game_info->m_frog[1]->right_released();
			break;
	}
	return true;
}

void Game::update_canvas()
{
	real_t width_ratio = width() / (real_t)m_width,
		   height_ratio = height() / (real_t)m_height;

	m_canvas.set_scale(width_ratio, height_ratio);

	DrawPosition pos;

	pos = get_draw_position(m_game_info->m_nlayer + 1, m_width/ 2,
			m_game_info->m_bg->swidth(), m_game_info->m_bg->sheight());

	m_canvas.move_img(m_game_info->m_bg, pos);

	std::vector<Layer *> &layers = m_game_info->m_layers;
	for (size_t i = 0; i < layers.size(); i ++)
	{
		Layer *&layer = layers[i];
		for (size_t j = 0; j < layer->m_wood.size(); j ++)
		{
			Wood *&wood = layer->m_wood[j];
			m_canvas.move_img(wood, 
					get_draw_position(layer->m_layer_id, wood->m_position ,
						wood->swidth(), wood->sheight()));
		}
	}

	for (size_t i = 0; i < m_game_info->m_frog.size(); i ++)
	{
		Frog *frog = m_game_info->m_frog[i];

		m_canvas.move_img(frog, 
				get_draw_position(frog->m_layer, frog->get_position(),
					frog->swidth(), frog->sheight()));
	}
}

static void *ThreadCallWrapper (void *arg_)
{
	Game::ThreadArg *arg = (Game::ThreadArg *)arg_;
	arg->layer->advance(arg->advc_time);
	pthread_exit(NULL);
}

bool Game::advance()
{
	real_t advc_time = timer.end() / 1000.0;
	switch (m_game_status)
	{
		case GAME_RUNNING:
			{

				for (size_t i = 0; i < m_game_info->m_frog.size(); i ++)
				{
					Frog *frog = m_game_info->m_frog[i];
					if (frog->m_layer == m_game_info->m_nlayer + 1) // win
					{
						printf("Time used: %.3lfs!\n", frog->time_used());
						frog->reset(true);
					}
				}


				// move wood first
				// if wood moves, frog moves

#ifdef USE_PTHREAD
#define check_ret(expr)   \
				do { \
					int ret = (expr); \
					if (ret) \
					{ \
						log_printf(#expr " error: %d, %s", ret, strerror(ret)); \
					} \
				} while (0)
				for (size_t i = 0; i < m_game_info->m_layers.size(); i ++)
				{
					Layer *layer = m_game_info->m_layers[i];
					m_targs[i].layer = layer;
					m_targs[i].advc_time = advc_time;
					check_ret(pthread_create(&m_threads[i], NULL, ThreadCallWrapper, &m_targs[i]));
				}

				for (size_t i = 0; i < m_threads.size(); i ++)
					check_ret(pthread_join(m_threads[i], NULL));
#else

				for (size_t i = 0; i < m_game_info->m_layers.size(); i ++)
					m_game_info->m_layers[i]->advance(advc_time);
#endif
				for (size_t i = 0; i < m_game_info->m_frog.size(); i ++)
					m_game_info->m_frog[i]->advance(advc_time);

			}
			break;

		case GAME_OVER:
			break;
	}
	update_canvas();
	timer.begin();
	return true;
}

DrawPosition Game::get_draw_position(int layer, real_t pos_on_layer,
		int obj_width, int obj_height)
{
	real_t nlayer = (m_game_info->m_nlayer + LAND_START_NLAYER + 
			LAND_FINISH_NLAYER) ;
	real_t layer_height = m_height / nlayer;

	DrawPosition pos;
	pos.x = pos_on_layer - obj_width / 2;
	assert(pos.x >= 0);

	real_t factor;
	if (layer == 0) // start layer
		factor = LAND_FINISH_NLAYER + m_game_info->m_nlayer;
	else if (layer == m_game_info->m_nlayer + 1) // finish layer
		factor = 0;
	else factor = LAND_FINISH_NLAYER + m_game_info->m_nlayer - layer;

	pos.y = layer_height * factor;

	return pos;
}

int Game::height() const
{
	int w, h;
	get_size(w, h);
	return h - 4;
}

int Game::width() const
{
	int w, h;
	get_size(w, h);
	return w - 4;
}

GameInfo * Game::get_game_info() const
{
	return m_game_info;
}

int GameInfo::game_status() const
{
	return m_game->game_status();
}


// Class GameInfo
GameInfo::GameInfo(Game *game, int nlayer)
{
	m_game = game;
	m_nlayer = nlayer;

	m_bg = new ImageDrawerble();
	m_bg->set_img(BackgroundImgPath);
	m_bg->set_size(m_game->m_width, m_game->m_height - 4);
	m_bg->save_size();

	if (game->nplayer() == 1)
	{
		Frog *frog = new Frog(this, m_game->layer_width_max() / 2, FrogDefaultSpeed * m_game->m_speedup,
				FrogImgPath);
		frog->set_size(FrogDefaultWidth, m_game->layer_draw_height());
		m_frog.push_back(frog);
	}
	else
	{
		Frog *frog = new Frog(this, m_game->layer_width_max() / 4 * 3, FrogDefaultSpeed * m_game->m_speedup, FrogImgPath1);
		frog->set_size(FrogDefaultWidth, m_game->layer_draw_height());
		frog->save_size();
		m_frog.push_back(frog);

		frog = new Frog(this, m_game->layer_width_max()/ 4, FrogDefaultSpeed * m_game->m_speedup, FrogImgPath);
		frog->set_size(FrogDefaultWidth, m_game->layer_draw_height());
		frog->save_size();
		m_frog.push_back(frog);

	}

	int layer_width_max = game->layer_width_max();
	// start
	Layer *layer = new Layer(0, m_game->m_width / 2, layer_width_max, m_game);
	Wood *wood = new Wood(layer, layer_width_max, 0, m_game->m_width / 2, LandImgPath);
	wood->set_size(layer_width_max - 0.01, m_game->layer_draw_height() * LAND_START_NLAYER);
	wood->save_size();
	layer->add_wood(wood);
	m_layers.push_back(layer);

	for (int i = 0; i < m_nlayer; i ++)
	{
		layer = new Layer(i + 1, m_game->m_width / 2, layer_width_max, m_game);
		real_t width_factor = 1.0 / (i / 2 + 1),
			   speed_factor = i + 1;

		real_t min_width = WoodDefaultWidth * width_factor / 4,
			   max_width = WoodDefaultWidth * width_factor,
			   min_speed = WoodDefaultSpeed / 2 * speed_factor,
			   max_speed = WoodDefaultSpeed * speed_factor,
			   min_spacing = min_width / 2,
			   max_spacing = max_width * 1.618;

		min_width = std::max(min_width, 20.0);
		max_width = std::max(min_width, max_width);
		max_speed = std::min(300.0, max_speed);
		min_speed = std::min(min_speed, max_speed);
		min_spacing = std::max(min_spacing, 40.0);
		max_spacing = std::max(min_spacing, max_spacing);
		max_spacing = std::min(layer->m_layer_width, max_spacing);
		min_spacing = std::min(min_spacing, max_spacing);

		layer->init_wood(min_width, max_width, min_speed, max_speed, min_spacing, max_spacing);

		m_layers.push_back(layer);
	}

	// finish 
	layer = new Layer(m_nlayer + 1, m_game->m_width / 2, layer_width_max, m_game);
	wood = new Wood(layer, layer_width_max, 0, m_game->m_width / 2, LandImgPath);
	wood->set_size(layer_width_max - 0.01, m_game->layer_draw_height() * LAND_FINISH_NLAYER);
	wood->save_size();
	layer->add_wood(wood);
	m_layers.push_back(layer);

	for (size_t i = 0; i < m_frog.size(); i ++)
		m_frog[i]->reset(true);
}

GameInfo::~GameInfo()
{
	for (size_t i = 0; i < m_frog.size(); i ++)
		delete m_frog[i];
	for (size_t i = 0; i < m_layers.size(); i ++)
		delete m_layers[i];
}

int GameInfo::width() const
{
	return m_game->width() - 10;
}

int GameInfo::height() const
{
	return m_game->height() - 10;
}

void Frog::reset(bool reset_time)
{
	if (m_wood)
		m_wood->remove_frog(this);
	m_wood = NULL;
	m_layer = 0;
	init();

	if (reset_time)
		timer.begin();
}

void Frog::set_position(real_t position)
{
	m_mctrl->set_position(position);
}

real_t Frog::get_position() const
{
	return m_mctrl->get_position();
}

/* Frog key event {{{ */
void Frog::left_pressed()
{
	key_status |= KEY_LEFT;
}

void Frog::right_pressed()
{
	key_status |= KEY_RIGHT;
}

void Frog::up_pressed()
{
	if (!layer_move(1))
		throw GameSigFrogDie();
}

void Frog::down_pressed()
{
	if (!layer_move(-1))
		throw GameSigFrogDie();
}

void Frog::left_released()
{
	key_status -= (key_status & KEY_LEFT ? 1 : 0) * KEY_LEFT;
}

void Frog::right_released()
{
	key_status -= (key_status & KEY_RIGHT ? 1 : 0) * KEY_RIGHT;
}

void Frog::up_released()
{
}

void Frog::down_released()
{
}

/* }}} */

int Frog::layer_move(int delta)
{
	if (m_layer + delta >= 0 && m_layer + delta <= m_game_info->m_nlayer + 1)
	{
		m_layer += delta;
		m_wood->remove_frog(this);
		Layer *layer = m_game_info->m_layers[m_layer];
		m_wood = layer->find_frog(this);
		if (m_wood == 0)
			return false;
		m_wood->add_frog(this);
	}
	return true;
}

bool Frog::init()
{
	Layer *layer = m_game_info->m_layers[m_layer];
	for (size_t j = 0; j < layer->m_wood.size(); j ++) 
	{
		Wood *wood = layer->m_wood[j];
		if (on_wood(wood))
		{
			m_wood = wood;
			wood->add_frog(this);
			return true;
		}
	}
	return false;
}

bool Frog::on_wood(Wood *wood)
{
	return m_position >= wood->head() && m_position <= wood->tail();
}


void Frog::advance(real_t advc_time)
{
	if (!m_wood)
		return;
	m_position = m_mctrl->advance(key_status, advc_time);
	if (m_position < m_wood->head())
		m_position = m_wood->head();
	if (m_position > m_wood->tail())
		m_position = m_wood->tail();
	m_mctrl->set_position(m_position);
}

void Frog::move(int key)
{
	if (m_game_info->game_status() == GAME_OVER)
		return;

	switch (key)
	{
		case KEY_UP:
			up_pressed();
			break;
		case KEY_DOWN:
			down_pressed();
			break;
		case KEY_LEFT:
			left_pressed();
			break;
		case KEY_RIGHT:
			right_pressed();
			break;
	}
}

Frog::Frog(GameInfo *game_info,
		real_t m_position, real_t m_move_speed,
		const char *img_path) :
	key_status(KEY_EMPTY),
	m_mctrl(new MoveControllerAccelerated()),
	m_position(m_position),
	m_move_speed(m_move_speed),
	m_game_info(game_info),
	m_wood(NULL),
	m_layer(0)
{
	set_img(img_path);

	m_mctrl->set_position(m_position);
	m_mctrl->set_speed_max(m_move_speed);
}

Frog::~Frog()
{
	delete m_mctrl;
}

Layer::Layer(int layer_id, real_t layer_pos, real_t layer_width,
		Game *game) :
	m_layer_pos(layer_pos),
	m_layer_width(layer_width),
	m_layer_id(layer_id),
	m_game(game)
{
}

void Layer::add_wood(Wood *wood)
{
	m_wood.push_back(wood);
}

void Layer::init_wood(
		int wood_width_min, 
		int wood_width_max,
		int wood_speed_min,
		int wood_speed_max,
		int min_spacing,
		int max_spacing)
{
	clear_wood();

	for (real_t pos = m_layer_width * 0.05; pos + wood_width_min - 1 < m_layer_width * 0.95; )
	{
		real_t width = RandRange(wood_width_min, wood_width_max),
			   speed =	(Rand(2) ? 1 : -1) * RandRange(wood_speed_min, wood_speed_max);

		if (pos + width >= m_layer_width)
			break;

		Wood *wood = new Wood(this, width, speed * m_game->m_speedup, head() + pos + width / 2);
		wood->set_size(width, m_game->layer_draw_height());
		wood->save_size();
		if (m_wood.size())
			assert(wood->head() >= m_wood.back()->tail());
		m_wood.push_back(wood);

		pos += width + RandRange(min_spacing, max_spacing);
	}

}

void Layer::clear_wood()
{
	for (size_t i = 0; i < m_wood.size(); i ++)
		delete m_wood[i];
	m_wood.clear();
}


Wood *Layer::find_frog(Frog *frog)
{
	for (size_t i = 0; i < m_wood.size(); i ++)
	{
		Wood *wood = m_wood[i];
		if (frog->on_wood(wood))
			return wood;
	}
	return NULL;
}


bool Layer::advance(real_t advc_time)
{
	/*
	 * hit wood behind
	 */
	struct Collide
	{
		real_t time;
		Wood *wood, *wood_collide;
		Collide(){}
		Collide(real_t time, Wood *wood, Wood *wood_collide = NULL) :
			time(time), wood(wood), wood_collide(wood_collide)
		{}
	};

	real_t time_remain = advc_time;
	Collide last_col;
	last_col.time = std::numeric_limits<real_t>::max();
	last_col.wood = last_col.wood_collide = NULL;
	static int cnt = 0;
	int cnt_start = cnt;
	do
	{
		cnt ++;
#ifdef DEBUG
#define wassert(expr) \
		do { \
			int ret = (expr); \
			if (!ret) { \
				log_printf("Assertion failed: " #expr " i = %lu, cnt = %d", i, cnt); \
				log_printf("w->head()=%lf,w->tail()=%lf,wn->head()=%lf,%wn->tail()=%lf", w->head(), w->tail(), wn->head(), wn->tail()); \
				log_printf("this->head()=%lf,this->tail()=%lf",this->head(),this->tail()); \
				assert(ret); \
			} \
		} while (0)
#define CHECK_WOOD() \
		for (size_t i = 0; i < m_wood.size(); i ++) \
		{ \
			Wood *w = m_wood[i]; \
			wassert(w->head() >= this->head()); \
			wassert(w->tail() <= this->tail()); \
			if (i != m_wood.size() - 1) \
			{ \
				Wood *wn = m_wood[i + 1]; \
				wassert(w->tail () <= wn->head()); \
			} \
		} 
#else
#define wassert(expr)
#define CHECK_WOOD() 
#endif

		Collide col;

		col.time = std::numeric_limits<real_t>::max();
		col.wood = col.wood_collide = NULL;
		for (size_t i = 0; i < m_wood.size(); i ++)
		{
			Wood *w0 = m_wood[i];
			if (i == 0 && w0->m_speed < 0)
			{
				real_t s = w0->head() - this->head(),
					   v = -w0->m_speed,
					   t = s / v;
				if (t < col.time)
					col = Collide(t, w0);
			}
			if (i == m_wood.size() - 1 && w0->m_speed > 0)
			{
				real_t s = this->tail() - w0->tail(),
					   v = w0->m_speed,
					   t = s / v;
				if (t < col.time)
					col = Collide(t, w0);
			}
			if (i < m_wood.size() - 1)
			{
				Wood *w1 = m_wood[i + 1];
				if (fabs(w0->m_speed - w1->m_speed) > EPS)
				{
					real_t s = w1->head() - w0->tail(),
						   v = w0->m_speed - w1->m_speed,
						   t = s / v;
					if (t > 0 && t < col.time)
						col = Collide(t, w0, w1);
				}
			}
		}

		col.time -= EPS / 2;
		if (col.time > time_remain)
			break;
		time_remain -= col.time;
		for (size_t i = 0; i < m_wood.size(); i ++)
		{
			Wood *wood = m_wood[i];
			wood->advance(col.time);
		}

		// calculate speed
		do
		{
#define SQR(x) ((x) * (x))

			Wood *w0 = col.wood,
				 *w1 = col.wood_collide;
			if (w1 == NULL)
				w0->m_speed *= -1;
			else
			{
				if (m_game->get_collision_type() == COLLISION_SIMPLE)
				{
					w0->m_speed *= -1;
					w1->m_speed *= -1;
				}
				else
				{
					real_t m0 = w0->m_width, v0 = w0->m_speed,
						   m1 = w1->m_width, v1 = w1->m_speed;
					w0->m_speed = (2*m1*v1+(m0-m1)*v0)/(m0+m1);
					w1->m_speed = (2*m0*v0+(m1-m0)*v1)/(m0+m1);
				}
			}

		} while (0);
		last_col = col;
	} while (time_remain > EPS);

	for (size_t i = 0; i < m_wood.size(); i ++)
	{
		Wood *w0 = m_wood[i];
		w0->advance(time_remain);
	}

#ifdef DEBUG
	for (size_t i = 0; i < m_wood.size(); i ++)
	{
		Wood *w0 = m_wood[i];
		assert(w0->head() >= head() && w0->tail() <= tail());

		if (i != m_wood.size() - 1) 
		{ 
			Wood *w1 = m_wood[i + 1]; 
			if (w0->tail() > w1->head())
			{
				assert(w1->head() + EPS > w0->tail());
				w1->move(EPS);
			}
		} 
	}

#endif

#undef wassert
#undef CHECK_WOOD

	return true;
}

Layer::~Layer()
{
	clear_wood();
};


Wood::Wood(Layer *layer, real_t width, real_t speed, real_t position,
		const char *img_path) :
	m_layer(layer),
	m_width(width), m_speed(speed), m_position(position)
{
	set_img(img_path);
}

void Wood::move(real_t dist)
{
	m_position += dist;
	for (size_t i = 0; i < m_frog.size(); i ++)
		m_frog[i]->set_position(m_frog[i]->get_position() + dist);
}

bool Wood::advance(real_t advc_time)
{
	move(m_speed * advc_time);
	return true;
}

bool Wood::add_frog(Frog *frog)
{
	for (size_t i = 0; i < m_frog.size(); i ++)
		if (m_frog[i] == frog)
			return false;
	m_frog.push_back(frog);

	return true;
}

bool Wood::remove_frog(Frog *frog)
{
	for (size_t i = 0; i < m_frog.size(); i ++)
		if (m_frog[i] == frog)
		{
			m_frog.erase(m_frog.begin() + i);
			return true;
		}
	return false;
}

Wood::~Wood()
{
}

/**
 * vim: syntax=cpp11 foldmethod=marker
 */



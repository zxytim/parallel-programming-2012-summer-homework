/*
 * $File: game.hh
 * $Date: Fri Aug 24 09:53:32 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __GAME_HH__
#define __GAME_HH__

#include <vector>
#include <queue>

#include <gtkmm.h>
#include <pthread.h>
#include <gtkmm/window.h>

#include "util.hh"
#include "canvas.hh"

EXCEPTION_DEFINE(GameSigFrogDie, Exception);

#define DEFAULT_WIDTH 1024
#define DEFAULT_NLAYER 7

#define LAND_START_NLAYER 2
#define LAND_FINISH_NLAYER 1

#define DEFAULT_HEIGHT (50 * (LAND_START_NLAYER + LAND_FINISH_NLAYER + DEFAULT_NLAYER) + 16)

#define Fps 40


#define FrogDefaultSpeed 200
#define FrogDefaultPosition DEFAULT_WIDTH * 0.5
#define FrogDefaultWidth 50

#define WoodDefaultWidth 150
#define WoodDefaultSpeed 50


#define FrogImgPath "res/frog.png"
#define FrogImgPath1 "res/frog1.png"
#define WoodImgPath "res/wood.png"
#define LandImgPath "res/land.png"
#define BackgroundImgPath "res/bg.png"
#define TransparentImgPath "res/transparent.png"

#define KEY_EMPTY 0

#define KEY_LEFT 1
#define KEY_RIGHT 2
#define KEY_UP 4
#define KEY_DOWN 8

#define P0_KEY_LEFT 1
#define P0_KEY_RIGHT 2
#define P0_KEY_UP 4
#define P0_KEY_DOWN 8

#define P1_KEY_LEFT 16
#define P1_KEY_RIGHT 32
#define P1_KEY_UP 64
#define P1_KEY_DOWN 128


#define COLLISION_SIMPLE	1
#define COLLISION_PHYSICAL	2

#define GAME_RUNNING 0
#define GAME_OVER -1


class Wood;
class GameInfo;
class MoveController;

class Frog : public ImageDrawerble
{
	protected:

		int key_status;
		MoveController *m_mctrl;

		real_t m_position; // the center of the frog
		real_t m_move_speed;

		Timer timer;

	public:

		virtual ~Frog();
		Frog(GameInfo *game_info, 
				real_t position = FrogDefaultPosition, 
				real_t move_speed = FrogDefaultSpeed,
				const char *img_path = FrogImgPath);

		real_t head() const { return m_position - width() / 2; }
		real_t tail() const { return m_position + width() / 2; }
		bool init();

		real_t time_used() { return timer.end() / 1000.0; }

		void set_position(real_t position);
		real_t get_position() const; 

		GameInfo *m_game_info;
		Wood * m_wood;

		int m_layer;
		void move(int dir);

		void left_pressed();
		void right_pressed();
		void up_pressed();
		void down_pressed();

		void left_released();
		void right_released();
		void up_released();
		void down_released();

		void advance(real_t advc_time);

		bool on_wood(Wood *wood);


		// return false if failed, which means
		// the game is over
		int layer_move(int delta);

		void reset(bool reset_time);
};

class MoveController
{
	public:
		virtual real_t advance(int key, real_t advc_time) = 0;
		virtual real_t set_position(real_t pos) = 0;
		virtual real_t get_position() = 0;
		virtual real_t set_speed_max(real_t speed_max) = 0;
		virtual ~MoveController() {}
};

class MoveControllerLinear : public MoveController
{
	public:
		real_t m_position, m_speed;
		virtual real_t get_position()
		{
			return m_position;
		}

		virtual real_t set_position(real_t pos)
		{ 
			return m_position = pos;
		}

		virtual real_t set_speed_max(real_t speed_max)
		{
			return m_speed = speed_max;
		}

		virtual real_t advance(int key, real_t advc_time)
		{
			if (key & KEY_LEFT)
				return m_position - m_speed * advc_time;

			if (key & KEY_RIGHT)
				return m_position + m_speed * advc_time;

			return m_position;
		}
};

class MoveControllerAccelerated : public MoveController
{
	public:
		real_t m_position, m_speed_max, m_speed, m_acc;

		MoveControllerAccelerated() : m_speed(0) {}

		virtual real_t get_position()
		{
			return m_position;
		}

		virtual real_t set_position(real_t pos)
		{ 
			return m_position = pos;
		}

		virtual real_t set_speed_max(real_t speed_max)
		{
			m_speed_max = speed_max;
			m_acc = m_speed_max * 2;
			return m_speed_max;
		}

		static int sign(real_t val) { return val > EPS ? 1 : ((val < -EPS) ? -1 : 0); }
		virtual real_t advance(int key, real_t advc_time)
		{
			real_t n_speed = m_speed;

			if (key & KEY_LEFT)
				n_speed += -m_acc * advc_time;
			if (key & KEY_RIGHT)
				n_speed += m_acc * advc_time;
			if (key == KEY_EMPTY || ((key & KEY_LEFT) && (key & KEY_RIGHT)))
				n_speed = 0;

			real_t move_speed_min = m_speed_max / 3;
			if (key != (KEY_LEFT | KEY_RIGHT)
					&& key != KEY_EMPTY && fabs(n_speed) < move_speed_min)
				n_speed = move_speed_min * sign(n_speed);

			if (fabs(n_speed) > m_speed_max)
				n_speed = m_speed_max * sign(n_speed);

			m_speed = n_speed;
			return m_position + m_speed * advc_time;
		}
};

class Layer;
class Wood : public ImageDrawerble
{
	public:
		std::vector<Frog *> m_frog;
		Layer *m_layer;

		real_t m_width, m_speed, m_position; // center
		Wood(Layer *layer, real_t width, real_t speed, real_t position,
				const char *img_path = WoodImgPath);

		real_t head() const { return m_position - m_width / 2; }
		real_t tail() const { return m_position + m_width / 2; }

		bool add_frog(Frog *frog);
		bool remove_frog(Frog *frog);

		// not dealt with collision
		bool advance(real_t advc_time);
		void move(real_t dist);

		virtual ~Wood();
};

class Layer
{
	public:
		std::deque<Wood *> m_wood;
		real_t m_layer_pos;
		real_t m_layer_width;
		int m_layer_id;

		real_t head() const { return m_layer_pos - m_layer_width / 2; }
		real_t tail() const { return m_layer_pos + m_layer_width / 2; }

		Game *m_game;
		Layer(int layer_id, real_t layer_pos, real_t layer_width, Game *game);
		void init_wood(
				int wood_width_min,
				int wood_width_max,
				int wood_speed_min,
				int wood_speed_max,
				int min_spacing,
				int max_spacing);
		void clear_wood();
		void add_wood(Wood *wood);

		bool advance(real_t advc_time);

		Wood *find_frog(Frog *frog);
		virtual ~Layer();
};

class GameInfo
{
	private:
		GameInfo(const GameInfo &);
		GameInfo & operator = (const GameInfo &);

		Game *m_game;

	public:

		int m_nlayer; // this is number of layers of woods, 
		// real layer is from 0 to `nlayer + 1`

		std::vector<Frog *> m_frog;
		std::vector<Layer *> m_layers;
		ImageDrawerble *m_bg;

		int width() const;
		int height() const;

		int game_status() const;

		GameInfo(Game *game, int nlayer);
		virtual ~GameInfo();
};



/*
 * Class Game responsible for game logic and event
 */
class Game : public Gtk::Window
{
	public:
		void set_speedup(double speedup) { m_speedup = speedup; }
		friend class GameInfo;
		Game(int nplayer = 1);
		virtual ~Game();

		GameInfo * get_game_info() const;
		DrawPosition get_draw_position(
				int layer, real_t pos_on_layer,
				int width, int height);


		void start();
		void set_nplayer(int nplayer) { m_nplayer = nplayer; }
		void set_nlayer(int nlayer) { m_nlayer = nlayer; }
		int height() const;
		int width() const;

		int game_status() const { return m_game_status; }

		struct ThreadArg
		{
			Layer *layer;
			real_t advc_time;
		};

		int nplayer() const { return m_nplayer; }
		real_t nlayer_draw() const { return m_nlayer + LAND_START_NLAYER + LAND_FINISH_NLAYER; }
		real_t layer_draw_height() const { return height() / nlayer_draw(); }
		int layer_width_max() const { return m_layer_width_max; }

		int m_width, m_height;

		void set_size(int width, int height)
		{
			m_width = width;
			m_height = height;
			m_layer_width_max = m_width - FrogDefaultWidth;
		}

		void set_fps(int fps)
		{
			m_fps = fps;
			refresh_interval = 1000.0 / fps;
		}

		void set_collision(int type)
		{
			m_col_type = type;
		}

		int get_collision_type() const { return m_col_type; }

		double m_speedup;
	protected:

		int m_game_status;
		int m_layer_width_max;
		int m_col_type;

		GameInfo * m_game_info;

		real_t refresh_interval;
		Canvas m_canvas;

		std::vector<pthread_t> m_threads;
		std::vector<ThreadArg> m_targs;


		sigc::connection conn_advc;
		sigc::slot<bool> slot_advc;
		int m_fps;

		int m_nplayer;
		int m_nlayer;

		bool advance();
		void update_canvas();


		Timer timer;

		// callbacks
		virtual bool on_key_press_event(GdkEventKey *event);
		virtual bool on_key_release_event(GdkEventKey *event);

};

#endif // __GAME_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


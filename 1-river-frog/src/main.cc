/*
 * $File: main.cc
 * $Date: Fri Aug 24 09:57:23 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include<getopt.h>

#include <gtkmm/application.h>
#include <gtkmm/window.h>

#include "canvas.hh"
#include "game.hh"

using namespace Glib;
using namespace Gtk;

const char *progname;
void print_help()
{
	printf("Usage: %s [option]\n", progname);
	printf("Options:\n");
	printf("        --nplayer=NUM, -p    specify number of players\n");
	printf("        --nlayer=NUM, -l     specify number of layers\n");
	printf("        --size=SIZE, -s      specify size of the screen, in format\n");
	printf("                             `<width>x<height>', eg. 1024x500\n");
	printf("        --fps=NUM, -f        specify frames per second of the game\n");
	printf("        --collision=COL, -c  specify collision type,\n");
	printf("                             COL can be `simple' or `physical',\n");
	printf("                             `physical' by default\n");
	printf("        --speedup=NUM, -d    set game speed, 1.0 by default\n");
	printf("        --help, -h           print this help and exit\n");
}

int main(int argc, char *argv[])
{
	progname = argv[0];
	Glib::RefPtr<Application> app = Application::create(argc, argv, "River and Frog Arcade Game");

	Game game;

	option long_options[] = {
		{"nplayer",		required_argument,	NULL, 'p'},
		{"nlayer",		required_argument,	NULL, 'l'},
		{"size",		required_argument,	NULL, 's'},
		{"fps",			required_argument,	NULL, 'f'},
		{"help",		no_argument,	NULL, 'h'},
		{"collision",	required_argument,	NULL, 'c'},
		{"speedup",		required_argument,	NULL, 'd'},
	};

	int opt;
	game.set_nplayer(1);
	game.set_nlayer(7);
	while((opt = getopt_long(argc, argv, "d:p:l:hs:f:c:", long_options, NULL)) != -1)
	{
		switch (opt)
		{
			case 'd':
				{
					double speedup;
					sscanf(optarg, "%lf", &speedup);
					game.set_speedup(speedup);
				}
				break;
			case 'c':
				if (strcmp(optarg, "simple") == 0)
					game.set_collision(COLLISION_SIMPLE);
				else if (strcmp(optarg, "physical") == 0)
					game.set_collision(COLLISION_PHYSICAL);
				else
				{
					fprintf(stderr, "invalid collison type `%s', abort.\n", optarg);
					return -1;
				}
				break;
			case 'f':
				{
					int fps = atoi(optarg);
					if (fps < 1) fps = 1;
					game.set_fps(fps);
				}
				break;
			case 'p':
				{
					int np = atoi(optarg);
					if (np > 2) np = 2;
					if (np < 1) np = 1;
					game.set_nplayer(np);
				}
				break;
			case 'l':
				{
					int np = atoi(optarg);
					if (np > 40) np = 40;
					if (np < 1) np = 1;
					game.set_nlayer(np);
				}
				break;
			case 's':
				int width, height;
				if (sscanf(optarg, "%dx%d", &width, &height) != 2)
				{
					fprintf(stderr, "invalid size %s, abort.\n", optarg);
					return -1;
				}
				game.set_size(width, height);
				break;
			case 'h':
				print_help();
				return 0;
				break;
			default:
				break;
		}
	}
	game.start();
	return app->run(game);
}


/**
 * vim: syntax=cpp11 foldmethod=marker
 */


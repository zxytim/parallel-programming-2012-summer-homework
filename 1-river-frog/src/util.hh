/*
 * $File: util.hh
 * $Date: Sun Aug 19 00:27:21 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __UTIL_HH__
#define __UTIL_HH__

#include <cstdio>
#include <cstdlib>

#include "util/autoptr.hh"
#include "util/type.hh"
#include "util/vector.hh"
#include "util/sassert.hh"
#include "util/log.hh"
#include "util/misc.hh"
#include "util/exception.hh"
#include "util/timer.hh"

#endif // __UTILS_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


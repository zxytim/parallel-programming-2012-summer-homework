/*
 * $File: exception.cc
 * $Date: Sun Aug 19 00:48:46 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "exception.hh"

#include <cstdio>

Exception::Exception()
{
	m_msg = what();
}

Exception::~Exception()
{
}

const char *Exception::prompt() const throw ()
{
	return "[Exception]";
}

Exception::Exception(const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	vsnprintf(msg_buf, EXCEPTION_MSG_LENGTH_MAX - 1, fmt, args);
	va_end(args);

	m_msg = prompt();
   	m_msg += msg_buf;
}

string Exception::msg() const
{ return m_msg; }


const char *Exception::what() const throw()
{
	return " exception happened"; 
}


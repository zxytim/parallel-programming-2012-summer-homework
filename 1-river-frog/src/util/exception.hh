/*
 * $File: exception.hh
 * $Date: Sun Aug 19 00:37:17 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __EXCEPTION_HH__
#define __EXCEPTION_HH__

#include <cstdarg>
#include <string>

using std::string;

class Exception 
{
	protected:
		static const int EXCEPTION_MSG_LENGTH_MAX = 256;
		string m_msg;
		virtual const char *prompt() const throw ();
		char msg_buf[EXCEPTION_MSG_LENGTH_MAX];
	public:
		Exception();
		Exception(const char *fmt, ...);
		virtual string msg() const;
		virtual const char *what() const throw();

		virtual ~Exception();
};

#define EXCEPTION_DEFINE_CONSTRUCTOR_IN_CLASS(class_name) \
	class_name(const char *fmt, ...) \
	{ \
		va_list args; \
		va_start(args, fmt); \
		vsnprintf(msg_buf, EXCEPTION_MSG_LENGTH_MAX - 1, fmt, args); \
		va_end(args); \
		m_msg = prompt(); \
		m_msg += msg_buf; \
	} 

#define EXCEPTION_DEFINE(class_name, base) \
	class class_name : public base\
{ \
	public: \
		EXCEPTION_DEFINE_CONSTRUCTOR_IN_CLASS(class_name) \
		class_name() {} \
} 

#endif // __EXCEPTION_HH__
/**
 * vim: syntax=cpp11 foldmethod=marker
 */


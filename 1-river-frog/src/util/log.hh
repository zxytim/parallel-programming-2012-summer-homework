/*
 * $File: log.hh
 * $Date: Thu Aug 16 23:23:52 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __LOG_HH__
#define __LOG_HH__

#define log_printf(fmt, ...) \
	do { \
		static const int LOG_MSG_LEN_MAX = 256; \
		char log_msg[LOG_MSG_LEN_MAX]; \
		snprintf(log_msg, LOG_MSG_LEN_MAX - 1, "[%s] " fmt "\n", __PRETTY_FUNCTION__, ##__VA_ARGS__); \
		printf("%s", log_msg); \
		fflush(stdout); \
	} while (0)


#endif // __LOG_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


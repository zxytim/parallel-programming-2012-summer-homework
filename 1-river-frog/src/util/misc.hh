/*
 * $File: misc.hh
 * $Date: Sat Aug 18 14:46:26 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __MISC_HH__
#define __MISC_HH__

#include "type.hh"


const real_t EPS = 1e-6;

int Rand(int max);
int RandRange(int min, int max);


#endif // __MISC_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


/*
 * $File: sassert.hh
 * $Date: Thu Aug 16 22:46:08 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __SASSERT_HH__
#define __SASSERT_HH__

#include <cassert>

#define sassert(expr) assert(expr)

#endif // __SASSERT_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


/*
 * $File: type.hh
 * $Date: Thu Aug 16 19:49:40 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __TYPE_HH__
#define __TYPE_HH__


typedef double real_t;

#endif // __TYPE_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


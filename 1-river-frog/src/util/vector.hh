/*
 * $File: vector.hh
 * $Date: Thu Aug 16 22:32:37 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __VECTOR_HH__
#define __VECTOR_HH__

#include <cmath>

class Vector
{
	public:
		real_t x, y;
		
		Vector(){}
		Vector(real_t x, real_t y)
			: x(x), y(y) {}

		Vector operator + (const Vector &vec) const
		{ return Vector(x + vec.x, y + vec.y); }

		Vector operator - (const Vector &vec) const
		{ return Vector(x - vec.x, y - vec.y); }

		Vector operator * (const real_t factor) const
		{ return Vector(x * factor, y * factor); }

		Vector operator / (const real_t factor) const
		{ return Vector(x / factor, y / factor); }

		Vector & operator += (const Vector &v) 
		{ x += v.x, y += v.y; return *this; }

		Vector & operator -= (const Vector &v) 
		{ x -= v.x, y -= v.y; return *this; }

		Vector & operator *= (const real_t factor)
		{ x *= factor, y *= factor; return *this; }

		Vector & operator /= (const real_t factor)
		{ x /= factor, y /= factor; return *this; }

		real_t dot(const Vector &v) const
		{ return x * v.x + y * v.y; }

		real_t cross(const Vector &v) const
		{ return x * v.y - y * v.x; }

		real_t length() const
		{ return sqrt(x * x + y * y); }

		real_t lengthsqr() const
		{ return x * x + y * y; }
};


#endif // __VECTOR_HH__

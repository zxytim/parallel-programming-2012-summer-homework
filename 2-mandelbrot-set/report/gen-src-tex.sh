#!/bin/bash

for f in `find -L src \( -name *.cc -o -name *.hh \)`; do
	echo "\\cppsrc{$f}"
done > src.tex

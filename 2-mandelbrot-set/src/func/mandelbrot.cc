/*
 * $File: mandelbrot.cc
 * $Date: Sun Aug 26 19:13:53 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include "mandelbrot.hh"
#include "lib/log.hh"

#include <cmath>

Mandelbrot::Mandelbrot(const Setting &setting) :
	m_setting(setting)
{
	int color_max = 8192;
	palette_gray = new GrayPalette(color_max);
	palette_colorful = new Palette(color_max);
}

Mandelbrot::~Mandelbrot()
{
	delete palette_gray;
	delete palette_colorful;
}


void Mandelbrot::render(RenderTask &task)
{
	static const ColorRGB black(0, 0, 0),
				 white(1, 1, 1);
	Complex &c = task.num;

	real_t x = c.real, y = c.imag;

	int niter = m_setting.n_iter_max;
	for (real_t x_sqr, y_sqr;
			((x_sqr = x * x) + (y_sqr = y * y)) <= 4 && niter; 
			niter --)
	{
		y = 2 * x * y + c.imag;
		x = x_sqr - y_sqr + c.real;
	}

	if (task.cpixel == NULL || task.gpixel == NULL)
		return;
	if (x * x + y * y > 4)
	{
#define ITER(x, y, c) \
		do { \
			real_t x_sqr = x * x, y_sqr = y * y; \
			y = 2 * x * y + c.imag; \
			x = x_sqr - y_sqr + c.real; \
			niter ++; \
		} while (0)

#if 0
		*task.gpixel = palette_gray->color(niter);
		*task.cpixel = palette_colorful->color(niter);
#else
		int index = niter;
		real_t dist = (m_setting.n_iter_max - niter) - log(log(x * x + y * y)) / M_LN2;

		*task.gpixel = ColorRGB((sin(0.08 * dist) + 1) * 0.5);
		//*task.gpixel = ColorRGB((niter / (real_t)m_setting.n_iter_max));
		
		*task.cpixel = ColorRGB(
				dist / m_setting.n_iter_max, 
				(cos(0.008 * dist) + 1) * 0.5, 
				(sin(0.008 * dist) + 1) * 0.5);
#endif
	}
	else 
	{
		*task.gpixel = black;
		*task.cpixel = black;
	}
}

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


/*
 * $File: mandelbrot.hh
 * $Date: Sat Aug 25 20:02:53 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __FUNC_MANDELBROT_HH__
#define __FUNC_MANDELBROT_HH__

#include "function.hh"
#include "palette.hh"

class Mandelbrot : public Function
{
	public:

		static const int TERM_FLAG_NITER = 1, // terminate by setting max number of iterations
			  TERM_FLAG_PREC = 2;          // terminate by setting presision

		static const int TERM_N_ITER_DEFAULT = 200;
		static const real_t TERM_EPS_DEFAULT = 1e-15;

		class Setting
		{
			public:
				int term_flag;
				int n_iter_max;
				real_t eps;
				Setting(int term_flag, int n_iter_max = TERM_N_ITER_DEFAULT,
						real_t eps = TERM_EPS_DEFAULT) :
					term_flag(term_flag), n_iter_max(n_iter_max), eps(eps) {}
		};

	protected:
		Setting m_setting;
		Palette *palette_gray,
				*palette_colorful;


	public:
		Mandelbrot(const Setting &setting = Setting(TERM_FLAG_NITER));
		virtual ~Mandelbrot();
		virtual void render(RenderTask &task);
};

#endif // __FUNC_MANDELBROT_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


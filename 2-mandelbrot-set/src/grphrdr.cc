/*
 * $File: grphrdr.cc
 * $Date: Thu Aug 23 22:59:37 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include "grphrdr.hh"

#include <cstdio>

void GrphRdrConf::show()
{
	fprintf(stderr, "image size: %dx%d\n", gwidth, gheight);
	fprintf(stderr, "domain: (%.18" REAL_T_FMT ",%.18" REAL_T_FMT ")\n        [%.18" REAL_T_FMT ",%.18" REAL_T_FMT "]\n", domain.x, domain.y, domain.width, domain.height);
	fprintf(stderr, "nworker: %d\n", nworker);
}

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


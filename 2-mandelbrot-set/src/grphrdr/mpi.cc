/*
 * $File: mpi.cc
 * $Date: Mon Aug 27 11:07:44 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifdef ENABLE_MPI
#include <cstdlib>

#include <mpi.h>
#include <cmath>

#include <algorithm>

#include "grphrdr/mpi.hh"
#include "lib/sassert.hh"
#include "lib/log.hh"
#include "lib/timer.hh"

#define mpi_printf(fmt, ...) \
	do { \
		static const int MSG_LEN_MAX = 1000; \
		static char msg[MSG_LEN_MAX + 1]; \
		sprintf(msg, "[worker %3d][%s@%s:%d]: " fmt, proc_id, __FUNCTION__, __FILE__, __LINE__, ##__VA_ARGS__); \
		fprintf(stderr, "%s\n", msg); \
		fflush(stderr); \
	} while (0)

enum RDR_TAG
{
	TAG_APPLY_FOR_JOB,
	TAG_NEW_JOB,
	TAG_JOB_FINISHED,
	TAG_IMG_DATA
};

MPIRdr::MPIRdr(int argc, char *argv[], int ntf_max)
{
	this->ntf_max = ntf_max;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &nproc);
	if (nproc == 1)
	{
		mpi_printf("please run with more than 1 processes when using mpi");
		exit(0);
	}
	MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);

	// commit new ColorRGB type
#if 0
	int count = 3;
	int lengths[3] = {1, 1, 1};
	int color_t_size = sizeof(ColorRGB().red);
	MPI_Aint offsets[3] = {0, color_t_size, color_t_size + color_t_size};
#ifdef COLOR_RGB_FLOAT
	MPI_Datatype types[3] = {MPI_FLOAT, MPI_FLOAT, MPI_FLOAT};
#elif defined(COLOR_RGB_DOUBLE)
	MPI_Datatype types[3] = {MPI_DOUBLE, MPI_DOUBLE, MPI_DOUBLE};
#endif

	MPI_Type_struct(count, lengths, offsets, types, &MPI_TYPE_COLOR_RGB);

#endif

#define check_ret(expr) \
	do { \
		int ret = (expr); \
		if (ret != MPI_SUCCESS) { \
			mpi_printf(#expr " failed"); \
			exit(0); \
		} \
	} while (0)

	check_ret(MPI_Type_contiguous(sizeof(ColorRGB), MPI_CHAR, &MPI_TYPE_COLOR_RGB));
	check_ret(MPI_Type_contiguous(sizeof(GrphRdrConf), MPI_CHAR, &MPI_TYPE_GRPHRDRCONF));

	MPI_Type_commit(&MPI_TYPE_COLOR_RGB);
	MPI_Type_commit(&MPI_TYPE_GRPHRDRCONF);
#undef check_ret
}

MPIRdr::~MPIRdr()
{
	if (proc_id == ROOT_PROC)
	{
		GrphRdrConf conf;
		conf.nworker = -1;
		send_new_conf(conf);
	}
	MPI_Finalize();
}

void MPIRdr::task_assign()
{
	int range[2];
	MPI_Recv(range, 2, MPI_INT, MPI_ANY_SOURCE, TAG_JOB_FINISHED, MPI_COMM_WORLD, &status);

	mpi_printf("[%d,%d] obtained", range[0], range[1]);
	int worker = status.MPI_SOURCE;
	if (range[0] != range[1])
	{
		sassert(range[1] > range[0]);

		if (conf.no_output == false)
		{
			ColorRGB *data;

			mpi_printf("[%d,%d] receiving", range[0], range[1]);

			data = output->rgb->data + range[0];
			MPI_Recv(data, range[1] - range[0], MPI_TYPE_COLOR_RGB, worker, 
					TAG_IMG_DATA, MPI_COMM_WORLD, &status);

			data = output->gray->data + range[0];
			MPI_Recv(data, range[1] - range[0], MPI_TYPE_COLOR_RGB, worker, 
					TAG_IMG_DATA, MPI_COMM_WORLD, &status);

			mpi_printf("[%d,%d] received", range[0], range[1]);
		}

		pixels_finished += range[1] - range[0];
	}
	ts->fetch_task(range[0], range[1]);

	// TODO Non-blocking
	//MPI_Isend(range, 2, MPI_INT, worker, TAG_NEW_JOB, MPI_COMM_WORLD, &send_request);
	mpi_printf("[%d,%d] sending task to worker %d", range[0], range[1], worker);
	MPI_Send(range, 2, MPI_INT, worker, TAG_NEW_JOB, MPI_COMM_WORLD);
	mpi_printf("[%d,%d] task sended to worker %d", range[0], range[1], worker);
}

void MPIRdr::send_result(const MPIRdr::Task &task)
{
	int range[2] = {task.start, task.end};
	MPI_Send(range, 2, MPI_INT, ROOT_PROC, TAG_JOB_FINISHED, MPI_COMM_WORLD);
	if (conf.no_output == false && task.start != task.end)
	{
		mpi_printf("[%d,%d] send result", task.start, task.end);

		MPI_Send(task.rgb, task.end - task.start, MPI_TYPE_COLOR_RGB, ROOT_PROC,
				TAG_IMG_DATA, MPI_COMM_WORLD);

		MPI_Send(task.gray, task.end - task.start, MPI_TYPE_COLOR_RGB, ROOT_PROC,
				TAG_IMG_DATA, MPI_COMM_WORLD);

		mpi_printf("[%d,%d] sended", task.start, task.end);
	}
}

void MPIRdr::task_get(MPIRdr::Task &task)
{
	int range[2];
	MPI_Recv(range, 2, MPI_INT, ROOT_PROC, TAG_NEW_JOB, MPI_COMM_WORLD, &status);
	task.start = range[0];
	task.end = range[1];
}

GrphRdrOutput *MPIRdr::render(Function *func, const GrphRdrConf &conf_)
{
	output = NULL;
	GrphRdrConf conf = conf_;
	Timer timer;

	this->conf = conf;
	if (proc_id == ROOT_PROC)
	{
		send_new_conf(conf);

		if (!conf.no_output)
			output = new GrphRdrOutput(conf.gwidth, conf.gheight);
		npixel = conf.gwidth * conf.gheight;

		ntf_max = std::max(10000, (int)floor(ceil((real_t)npixel / nproc)));
		ntf_max = std::min(ntf_max, 1000000);

		ts = new TaskScheduler(npixel, ntf_max);
		pixels_finished = 0;

		mpi_printf("render starts");
		timer.begin();
		while (pixels_finished != npixel)
			task_assign();
		timer.end();

		log_printf("render time: %lldms", timer.duration());

		if (conf.no_output == false)
		{
			for (int i = 0; i < conf.gwidth; i ++)
				for (int j = 0; j < conf.gheight / 2; j ++)
				{
					int p0 = i * conf.gheight + j,
						p1 = i * conf.gheight + (conf.gheight - 1 - j);
					std::swap(output->rgb->data[p0], output->rgb->data[p1]);
					std::swap(output->gray->data[p0], output->gray->data[p1]);
				}

		}
		delete ts;
	}
	else
	{
		/*
		 * wait for new GrphRdrConf
		 */
		output = NULL;
		int allocd_pixel = 0;
		Task task;
		for (; recv_new_conf(conf);) // loop for render configuration
		{
			npixel = conf.gwidth * conf.gheight;;

			task.start = task.end = -1;
			if (!conf.no_output && npixel > allocd_pixel)
			{
				allocd_pixel = npixel;
				if (task.rgb)
					delete task.rgb;
				if (task.gray)
					delete task.gray;
				task.rgb = new ColorRGB[npixel];
				task.gray = new ColorGray[npixel];
			}

			const Rectangle &domain = conf.domain;
			RenderTask rtask;

			for (; ;)
			{
				send_result(task);
				task_get(task);

				if (task.start == task.end)
					break;

				int i = task.start / conf.gheight,
					j = task.start % conf.gheight;
				int p = task.start;

				if (conf.no_output)
				{
					rtask.gpixel = NULL;
					rtask.cpixel = NULL;
				}

				for (int dpos = 0; p < task.end; p ++, dpos ++)
				{
					rtask.num.real = domain.x + (real_t)i / conf.gwidth * domain.width;
					rtask.num.imag = domain.y + (real_t)j / conf.gheight * domain.height;

					if (conf.no_output == false)
					{
						rtask.gpixel = task.gray + dpos;
						rtask.cpixel = task.rgb + dpos;
					}

					func->render(rtask);

					j ++;
					if (j == conf.gheight)
						i ++, j = 0;
				}
				mpi_printf("[%d,%d] task get", task.start, task.end);
			}
		}
		if (conf.no_output == false)
		{
			delete task.gray;
			delete task.rgb;
		}
	}
	return output;
}

void MPIRdr::send_new_conf(const GrphRdrConf &conf)
{
	GrphRdrConf buf = conf;
	MPI_Bcast(&buf, 1, MPI_TYPE_GRPHRDRCONF, ROOT_PROC, MPI_COMM_WORLD);
}

bool MPIRdr::recv_new_conf(GrphRdrConf &conf)
{
	MPI_Bcast(&conf, 1, MPI_TYPE_GRPHRDRCONF, ROOT_PROC, MPI_COMM_WORLD);
	if (conf.nworker == -1)
		return false;
	return true;
}

TaskScheduler::TaskScheduler(int ntask, int ntf_max) :
	ntask(ntask), cnt_task(0) , ntf_max(ntf_max)
{
}

void TaskScheduler::fetch_task(int &start, int &end)
{
	int n = ntask - cnt_task;
	n = (n > ntf_max ? ntf_max : n);
	start = cnt_task;
	end = cnt_task + n;
	cnt_task = end;
}

bool TaskScheduler::finished()
{
	return ntask == cnt_task;
}

#endif
/**
 * vim: syntax=cpp11 foldmethod=marker
 */


/*
 * $File: openmp.cc
 * $Date: Sat Aug 25 12:15:55 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifdef ENABLE_OPENMP
#include "grphrdr/openmp.hh"
#include "lib/timer.hh"
#include "lib/log.hh"

GrphRdrOutput *OpenMPRdr::render(Function *func, const GrphRdrConf &conf)
{
	const Rectangle &domain = conf.domain;
	int npixel = conf.gwidth * conf.gheight;
	RenderTask *task = new RenderTask[npixel];
	GrphRdrOutput *output = NULL;
	if (conf.no_output == false)
		output = new GrphRdrOutput(conf.gwidth, conf.gheight);

	int p = 0;
	for (int i = 0; i < conf.gwidth; i ++)
	{
		for (int j = 0; j < conf.gheight; j ++, p ++)
		{
			real_t x = domain.x + (real_t)i / conf.gwidth * domain.width,
				   y = domain.y + (real_t)j / conf.gheight * domain.height;
			task[p].num = Complex(x, y);
			int t = i * conf.gheight + (conf.gheight - 1 - j);
			if (conf.no_output)
			{
				task[p].cpixel = NULL;
				task[p].gpixel = NULL;
			}
			else	
			{
				task[p].cpixel = output->rgb->data + t;
				task[p].gpixel = output->gray->data + t;
			}
		}
	}

	Timer timer;
	timer.begin();

#pragma omp parallel for num_threads(conf.nworker)
	for (int i = 0; i < npixel; i ++)
		func->render(task[i]);

	timer.end();
	log_printf("render time: %lldms", timer.duration());

	return output;
}


#endif
/**
 * vim: syntax=cpp11 foldmethod=marker
 */


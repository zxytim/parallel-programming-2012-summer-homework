/*
 * $File: pthread.cc
 * $Date: Sat Aug 25 20:01:58 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifdef ENABLE_PTHREAD

#include <cstring>

#include "lib/sassert.hh"
#include "lib/timer.hh"
#include "lib/log.hh"

#include "grphrdr/pthread.hh"

PthreadRdr::PthreadRdr(int ntf_max) :
	m_ntf_max(ntf_max)
{
}

void PthreadRdr::set_ntf_max(int ntf_max)
{
	m_ntf_max = ntf_max;
}

struct PthreadArg
{
	PthreadRdr *rdr;
	TaskPool *taskpool;
	Function *func;
};

void *__render_call_wrapper(void *arg_)
{
	PthreadArg *arg = static_cast<PthreadArg *>(arg_);
	arg->rdr->render_do(arg->func, arg->taskpool);
	pthread_exit(NULL);
}

#include <cstdio>
GrphRdrOutput *PthreadRdr::render(Function *func, const GrphRdrConf &conf)
{
	int nthread = conf.nworker;

	GrphRdrOutput *output = NULL;
	if (conf.no_output == false)
		output = new GrphRdrOutput(conf.gwidth, conf.gheight);
	TaskPool *taskpool = new TaskPool(conf, output);
	pthread_t *threads = new pthread_t[nthread];

	PthreadArg *ptarg = new PthreadArg[nthread];

	Timer timer;
	timer.begin();

	for (int i = 0; i < nthread; i ++)
	{
		PthreadArg *arg = ptarg + i;
		arg->rdr = this;
		arg->taskpool = taskpool;
		arg->func = func;
		pthread_create(&threads[i], NULL, __render_call_wrapper, arg);
	}

	for (int i = 0; i < nthread; i ++)
		pthread_join(threads[i], NULL);

	timer.end();
	log_printf("render time: %lldms", timer.duration());

	delete taskpool;
	delete [] threads;
	delete [] ptarg;

	return output;
}

void PthreadRdr::render_do(Function *func, TaskPool *taskpool)
{
	RenderTask *tasks;

	int ntask, tot_task = 0;
	while ((ntask = taskpool->fetch_task(m_ntf_max, tasks)))
	{
		tot_task += ntask;
		for (int i = 0; i < ntask; i ++)
			func->render(tasks[i]);
	}
	//log_printf("pixels rendered: %d\n", tot_task);
}

TaskPool::TaskPool(const GrphRdrConf &conf, GrphRdrOutput *output)
{
	memset(&m_mutex, 0, sizeof(m_mutex));
	int npixel = conf.gwidth * conf.gheight;
	task = new RenderTask[npixel];
	ntask = npixel;
	cnt_task = 0;

	const Rectangle &domain = conf.domain;
	for (int i = 0, p = 0; i < conf.gwidth; i ++) 
		for (int j = 0; j < conf.gheight; j ++, p ++)
		{
			task[p].num.real = domain.x + (real_t)i / conf.gwidth * domain.width;
			task[p].num.imag = domain.y + (real_t)j / conf.gheight * domain.height;

			if (conf.no_output)
			{
				task[p].cpixel = NULL;
				task[p].gpixel = NULL;
			}
			else
			{
				int dpos = i * conf.gheight + (conf.gheight - 1 - j);
				task[p].cpixel = output->rgb->data + dpos;
				task[p].gpixel = output->gray->data + dpos;
			}
		}
}

TaskPool::~TaskPool()
{
	delete [] task;
}

int TaskPool::fetch_task(int nnum, RenderTask *&mem)
{
	pthread_mutex_lock(&m_mutex);
	int n = ntask - cnt_task;
	n = (nnum > n ? n : nnum);
	mem = task + cnt_task;
	cnt_task += n;
	pthread_mutex_unlock(&m_mutex);

	return n;

}

double TaskPool::progress()
{
	return (double)cnt_task / ntask;
}

#endif

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


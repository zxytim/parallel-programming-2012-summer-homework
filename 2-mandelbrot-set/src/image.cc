/*
 * $File: image.cc
 * $Date: Thu Aug 23 00:42:58 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include "image.hh"

Image::Image(int width, int height) :
	width(width), height(height)
{
	data = new ColorRGB[width * height];
}

Image::~Image()
{
	delete[] data;
}
/**
 * vim: syntax=cpp11 foldmethod=marker
 */


/*
 * $File: function.hh
 * $Date: Thu Aug 23 23:14:47 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __FUNCTION_HH__
#define  __FUNCTION_HH__

#include "lib/type.hh"
#include "rendertask.hh"


class Function
{
	protected:
		int m_pm;

	public:
		static const int PM_GRAY = 0,
					 PM_COLORFUL  = 1;
		virtual void paint_mode(int pm);
		/*
		 * IMPORTANT:
		 *		if use threads, function MUST be guaranteed to be THREAD SAFE
		 */
		virtual void render(RenderTask &task) = 0;
		virtual ~Function() {}
};

#endif // __FUNCTION_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


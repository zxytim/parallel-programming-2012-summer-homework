/*
 * $File: grphrdr.hh
 * $Date: Sat Aug 25 12:07:39 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 *
 * Graph Renderer and render specifications
 */

#ifndef __GRPHRDR_HH__
#define __GRPHRDR_HH__

#include <cstdlib>

#include "lib/type.hh"
#include "lib/rectangle.hh"
#include "rendertask.hh"
#include "function.hh"
#include "image.hh"

struct GrphRdrConf
{
	int gwidth, gheight; // size of the graph, aka image
	Rectangle domain;    // plot domain
	int nworker;
	bool no_output;

	GrphRdrConf() {}
	GrphRdrConf(int gwidth, int gheight, const Rectangle &domain, 
			int nworker, bool no_output = false) :
		gwidth(gwidth), gheight(gheight), domain(domain), nworker(nworker),
	no_output(no_output){}
	void show();
};

struct GrphRdrOutput
{
	Image *gray, *rgb;
	GrphRdrOutput(int width, int height)
	{
		gray = new Image(width, height);
		rgb = new Image(width, height);
	}

	GrphRdrOutput() : gray(NULL), rgb(NULL) {}
	~GrphRdrOutput()
	{
		if (gray)
			delete gray;
		if (rgb)
			delete rgb;
	}
};

class GrphRdr
{
	public:
		virtual GrphRdrOutput *render(Function *func, const GrphRdrConf &conf) = 0;
		virtual ~GrphRdr() {}
};


#endif // __GRPHRDR_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


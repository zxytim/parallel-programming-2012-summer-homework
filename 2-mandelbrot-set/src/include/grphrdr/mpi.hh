/*
 * $File: mpi.hh
 * $Date: Sat Aug 25 12:12:45 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __GRPHRDR_MPI_HH__
#define __GRPHRDR_MPI_HH__

#ifdef ENABLE_MPI

#include <mpi.h>
#include "grphrdr.hh"

struct TaskScheduler
{
	TaskScheduler(int ntask, int ntf_max);
	int ntask, cnt_task;
	int ntf_max;
	void fetch_task(int &start, int &end);
	bool finished();
};

class MPIRdr : public GrphRdr
{
	protected:
		static const int ROOT_PROC = 0;

		int nproc, proc_id;


		MPI_Datatype MPI_TYPE_COLOR_RGB, 
					 MPI_TYPE_GRPHRDRCONF;

		MPI_Status status;
		int ntf_max;

		int npixel;

		GrphRdrConf conf;

		/* master */
		TaskScheduler *ts;
		MPI_Request send_request;
		int pixels_finished;
		GrphRdrOutput *output;

		void task_assign(); // recv result and assign new task

		/* slave */
		struct Task
		{
			int start, end;
			ColorRGB *gray, *rgb;
			Task() : gray(NULL), rgb(NULL) {}
		};
		void send_result(const Task &task);
		void task_get(Task &task);

		/*
		 * slave for for GrphRdrConf, but for the same function
		 */
		void send_new_conf(const GrphRdrConf &conf);
		bool recv_new_conf(GrphRdrConf &conf);

	public:
		MPIRdr(int argc, char *argv[], int ntf_max);
		~MPIRdr();
		virtual GrphRdrOutput *render(Function *func, const GrphRdrConf &conf);
		int rank() const { return proc_id; }
};

#endif // ENABLE_MPI
#endif // __GRPHRDR_MPI_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


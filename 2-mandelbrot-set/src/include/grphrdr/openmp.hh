/*
 * $File: openmp.hh
 * $Date: Fri Aug 24 15:26:11 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __GRPHRDR_OPENMP_HH__
#define __GRPHRDR_OPENMP_HH__

#ifdef ENABLE_OPENMP

#include "grphrdr.hh"

class OpenMPRdr : public GrphRdr
{
	public:
		virtual GrphRdrOutput *render(Function *func, const GrphRdrConf &conf);

};

#endif 

#endif // __GRPHRDR_OPENMP_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


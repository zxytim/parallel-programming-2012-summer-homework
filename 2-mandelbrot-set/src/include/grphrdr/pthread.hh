/*
 * $File: pthread.hh
 * $Date: Sat Aug 25 20:01:34 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __GRPHRDR_PTHREAD_HH__
#define __GRPHRDR_PTHREAD_HH__

#ifdef ENABLE_PTHREAD

#include <pthread.h>

#include "grphrdr.hh"

const int NTF_MAX_DEFAULT = 10000;

class TaskPool
{
	private:
		pthread_mutex_t m_mutex;

	public:
		RenderTask *task;
		int cnt_task, ntask;
		TaskPool(const GrphRdrConf &conf, GrphRdrOutput *output);

		virtual double progress();

		/*
		 * return number of tasks actually fetched
		 * @mem will be modified to the start position
		 * of consecutive tasks
		 */
		virtual int fetch_task(int nnum, RenderTask *&mem);
		virtual ~TaskPool();
};

class PthreadRdr : public GrphRdr
{
	protected:
		int m_ntf_max; // max number of tasks to be fetched each time

		virtual void render_do(Function *func, TaskPool *taskpool);
		friend void *__render_call_wrapper(void *arg_);
	public:

		void set_ntf_max(int ntf_max);
		/*
		 * @ntf_max max number of tasks to be fetched each time
		 */
		PthreadRdr(int ntf_max = NTF_MAX_DEFAULT);
		virtual GrphRdrOutput *render(Function *func, const GrphRdrConf &conf);
};


#endif // ENABLE_PTHREAD

#endif // __GRPHRDR_PTHREAD_HH__
/**
 * vim: syntax=cpp11 foldmethod=marker
 */


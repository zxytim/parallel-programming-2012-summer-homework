/*
 * $File: image.hh
 * $Date: Fri Aug 24 15:24:47 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __IMAGE_HH__
#define __IMAGE_HH__

#include "lib/type.hh"

typedef float color_t;
#define COLOR_RGB_FLOAT


struct ColorRGB
{
	color_t red, green, blue;
	// range in [0, 1];

	ColorRGB() {red = green = blue = 0; }
	ColorRGB(color_t red, color_t green, color_t blue) :
		red(red), green(green), blue(blue) {}
	ColorRGB(color_t gray) :
		red(gray), green(gray), blue(gray) {}
};

typedef ColorRGB ColorGray;

struct Image
{
	int width, height;
	ColorRGB *data;
	Image(int width, int height);
	~Image();
};

#endif // __IMAGE_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


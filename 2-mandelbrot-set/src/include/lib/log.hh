/*
 * $File: log.hh
 * $Date: Wed Aug 22 20:56:31 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __LOG_HH__
#define __LOG_HH__

#include <cstdio>

#define log_printf(fmt, ...) \
	do { \
		static const int MSG_LEN_MAX = 1000; \
		static char msg[MSG_LEN_MAX + 1]; \
		sprintf(msg, "[%s@%s:%d]: " fmt, __PRETTY_FUNCTION__, __FILE__, __LINE__, ##__VA_ARGS__); \
		fprintf(stderr, "%s\n", msg); \
	} while (0)

#endif // __LOG_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


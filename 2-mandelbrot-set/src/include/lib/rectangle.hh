/*
 * $File: rectangle.hh
 * $Date: Wed Aug 22 13:24:37 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __LIB_RECTANGLE_HH__
#define __LIB_RECTANGLE_HH__

#include "type.hh"

class Rectangle
{
	public:
		real_t x, y,  // up left coordinate
			width, height; // size
		Rectangle() {}
		Rectangle(real_t x, real_t y, real_t width, real_t height) :
			x(x), y(y), width(width), height(height) {}
};

#endif // __LIB_RECTANGLE_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


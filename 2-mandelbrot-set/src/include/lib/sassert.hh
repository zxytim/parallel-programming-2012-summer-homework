/*
 * $File: sassert.hh
 * $Date: Thu Aug 23 12:44:39 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __LIB_SASSERT_HH__
#define __LIB_SASSERT_HH__

#include <cassert>

#include "lib/log.hh"

#ifdef ENABLE_SASSERT
#define sassert(expr) \
	do { \
		int ret = (expr); \
		if (!ret) { \
			log_printf("Assertion failed: " #expr); \
			assert(0); \
		} \
	} while (0)

#else
#define sassert(expr)
#endif

#endif // __LIB_SASSERT_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


/*
 * $File: type.hh
 * $Date: Thu Aug 23 23:01:30 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __TYPE_HH__
#define __TYPE_HH__


#ifdef REAL_T_LONG_DOUBLE
typedef long double real_t;
#define REAL_T_FMT "Lf"
#else
typedef double real_t;
#define REAL_T_FMT "lf"
#endif

struct Complex
{
	real_t real, imag;
	Complex() {}
	Complex(real_t real, real_t imag) :
		real(real), imag(imag) {}
};

#endif // __TYPE_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


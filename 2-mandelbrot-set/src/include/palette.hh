/*
 * $File: palette.hh
 * $Date: Fri Aug 24 23:25:21 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __PALETTE_HH__
#define __PALETTE_HH__

#include "image.hh"

/*
 * convert a number to rgb color
 */
class Palette
{
	protected:
		int m_size;

		ColorRGB *palette;

	public:
		Palette(){}
		Palette(int size);
		virtual ColorRGB color(int seed);
		virtual ~Palette();
};

class GrayPalette : public Palette
{
	public:
		GrayPalette(int size);
};

#endif // __PALETTE_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


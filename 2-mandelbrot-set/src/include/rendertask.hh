/*
 * $File: rendertask.hh
 * $Date: Fri Aug 24 15:07:07 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __RENDER_TASK_HH__
#define __RENDER_TASK_HH__

#include "lib/type.hh"
#include "image.hh"

struct RenderTask
{
	Complex num;
	int niter;
	ColorGray *gpixel; // gray pixel
	ColorRGB *cpixel; // rgb pixel (colored pixel)
};

#endif // __RENDER_TASK_HH__
/**
 * vim: syntax=cpp11 foldmethod=marker
 */

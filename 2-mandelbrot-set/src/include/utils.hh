/*
 * $File: utils.hh
 * $Date: Thu Aug 23 11:29:20 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include "lib/exception.hh"
#include "lib/log.hh"
#include "lib/misc.hh"
#include "lib/timer.hh"
#include "lib/type.hh"
#include "lib/rectangle.hh"
#include "lib/sassert.hh"


/**
 * vim: syntax=cpp11 foldmethod=marker
 */


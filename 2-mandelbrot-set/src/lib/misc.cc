/*
 * $File: misc.cc
 * $Date: Tue Aug 21 10:02:07 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include <cstdlib>

#include  "lib/misc.hh"


int Rand(int max)
{
	return rand() / (RAND_MAX + 1.0) * max;
}

int RandRange(int min, int max)
{
	return min + Rand(max - min + 1);
}


/**
 * vim: syntax=cpp11 foldmethod=marker
 */


/*
 * $File: main.cc
 * $Date: Sat Aug 25 20:13:06 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include <cstdio>
#include <cstdlib>
#include <string>

#include <getopt.h>
#include <unistd.h>
#include <cstdlib>

#ifdef USE_GTK
#include <cairo.h>
#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#endif

#include "utils.hh"
#include "grphrdr.hh"

#ifdef ENABLE_PTHREAD
#include "grphrdr/pthread.hh"
#endif


#ifdef ENABLE_OPENMP
#include "grphrdr/openmp.hh"
#endif

#ifdef ENABLE_MPI
#include "grphrdr/mpi.hh"
#endif

#include "func/mandelbrot.hh"


#define error_exit(exit_code, fmt, ...) \
	do { \
		static const int MSG_LEN_MAX = 1000; \
		static char msg[MSG_LEN_MAX + 1]; \
		sprintf(msg, "error %d: " fmt, exit_code, ##__VA_ARGS__); \
		fprintf(stderr, "%s\n", msg); \
		exit(exit_code); \
	} while (0)

#ifdef USE_GTK
GdkPixbuf *image_to_pixbuf(Image *image);
#endif

struct RenderConfig
{
	GrphRdr *rdr;
	Function *func;
	GrphRdrConf gc;
	GrphRdrOutput *output;
#ifdef USE_GTK
	GdkPixbuf *gpixbuf, *cpixbuf;
	GdkPixbuf *pixbuf; // for shown;
#endif

	int width() const { return output->rgb->width; }
	int height() const { return output->rgb->height; }

#ifdef USE_GTK
	static const int ST_GRAY = 0,
				 ST_RGB = 1;
	int show_type;
	void set_show_type(int type)
	{
		if (type == ST_RGB)
		{
			pixbuf = cpixbuf;
			show_type = type;
		}
		else if (type == ST_GRAY)
		{
			pixbuf = gpixbuf;
			show_type = type;
		}
	}
#endif

	RenderConfig()
	{
		memset(this, 0, sizeof(RenderConfig));
#ifdef USE_GTK
		show_type = ST_RGB;
#endif
	}
	~RenderConfig()
	{
		delete rdr;
		delete func;
		if (output)
			delete output;
#ifdef USE_GTK
		if (gpixbuf)
			g_object_unref(gpixbuf);
		if (cpixbuf)
			g_object_unref(cpixbuf);
#endif
	}
	bool render(bool to_pixbuf = false)
	{
		if (output)
			delete output;
#ifdef USE_GTK
		if (cpixbuf)
		{
			g_object_unref(cpixbuf);
			cpixbuf = NULL;
			if (show_type == ST_RGB)
				pixbuf = NULL;
		}
		if (gpixbuf)
		{
			g_object_unref(gpixbuf);
			gpixbuf = NULL;
			if (show_type == ST_GRAY)
				pixbuf = NULL;
		}
#endif

		gc.show();

		fprintf(stderr, "{%.18LF,%.18LF,%.18LF,%.18LF}\n",
				gc.domain.x, gc.domain.y,
				gc.domain.width, gc.domain.height);
		output = rdr->render(func, gc);
#ifdef USE_GTK
		if (to_pixbuf)
			this->to_pixbuf();
#endif
		
		if (output == NULL)
			return false;
		return true;
	}
#ifdef USE_GTK
	void to_pixbuf()
	{
		if (output)
		{
			if (cpixbuf)
				g_object_unref(cpixbuf);
			if (gpixbuf)
				g_object_unref(gpixbuf);
			cpixbuf = image_to_pixbuf(output->rgb);
			gpixbuf = image_to_pixbuf(output->gray);
			if (show_type == ST_GRAY)
				pixbuf = gpixbuf;
			else pixbuf = cpixbuf;
		}
	}
#endif
};


RenderConfig rconf;

#ifdef USE_GTK
/* Gtk callbacks {{{ */
static gboolean delete_event(
		GtkWidget *,
		GdkEvent *,
		gpointer )
{
	return FALSE;
}

static void destroy(GtkWidget *, gpointer )
{
	gtk_main_quit();
}


GdkPixbuf *image_to_pixbuf(Image *image)
{
	GdkPixbuf *pixbuf = gdk_pixbuf_new(
			GDK_COLORSPACE_RGB, false, 8, image->width, image->height);
	int n_channels = gdk_pixbuf_get_n_channels(pixbuf),
		rowstride = gdk_pixbuf_get_rowstride(pixbuf);

	guchar *pixel = gdk_pixbuf_get_pixels(pixbuf);
	for (int i = 0; i < image->width; i ++)
		for (int j = 0; j < image->height; j ++)
		{
			guchar *p = pixel + j * rowstride + i * n_channels;
			ColorRGB *c = image->data + i * image->height + j;
			p[0] = c->red * 255;
			p[1] = c->green * 255;
			p[2] = c->blue * 255;
		}
	return pixbuf;
}

static gboolean da_expose_callback(
		GtkWidget *widget, 
		GdkEventExpose *, 
		gpointer )
{
	GdkPixbuf *pixbuf = rconf.pixbuf;

	cairo_t *cr = gdk_cairo_create(widget->window);

	gdk_cairo_set_source_pixbuf(cr, pixbuf, 0, 0);

	cairo_paint(cr);

	cairo_destroy(cr);

	return FALSE;
}

static gboolean cb_timeout(GtkWidget *widget)
{
	if (widget->window == NULL)
		return FALSE;

	gtk_widget_queue_draw_area(widget, 0, 0, widget->allocation.width, widget->allocation.height);
	return TRUE;
}

real_t zoom_scale = 2.0;

static gboolean cb_clicked(GtkWidget *, GdkEventButton *event,
		gpointer )
{
	static const unsigned LEFT_BUTTON = 1,
				 MIDDLE_BUTTON= 2,
				 RIGHT_BUTTON = 3;

	if (event->button == LEFT_BUTTON || event->button == RIGHT_BUTTON 
			|| event->button == MIDDLE_BUTTON)
	{
		int cx = event->x,
			cy = event->y;

		log_printf("click: %d, %d", cx, cy);
		GrphRdrConf &gc = rconf.gc;
		Rectangle &dm = gc.domain;

		real_t rx = dm.x + (real_t)cx / gc.gwidth * dm.width,
			   ry = dm.y + (real_t)(gc.gheight - cy) / gc.gheight * dm.height;

		if (event->button == LEFT_BUTTON)
			dm.width /= zoom_scale, dm.height /= zoom_scale;
		else if (event->button == RIGHT_BUTTON)
			dm.width *= zoom_scale, dm.height *= zoom_scale;


		dm.x = rx - dm.width / 2;
		dm.y = ry - dm.height / 2;

		rconf.render(true);
	}

	return TRUE;
}


void image_init(int argc, char *argv[])
{
	gtk_init(&argc, &argv);
}

void image_show()
{
	/* Gtk window {{{ */
	GtkWidget *window;

	int border_width = 0;
	int window_width = rconf.width() + border_width * 2,
		window_height = rconf.height() + border_width * 2;
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_container_set_border_width(GTK_CONTAINER(window), border_width);
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size(GTK_WINDOW(window), window_width, window_height);

	//gtk_widget_set_app_paintable(window, TRUE);
	gtk_widget_set_size_request(window, window_width, window_height);

	GtkWidget *da = gtk_drawing_area_new();
	gtk_widget_set_size_request(da, rconf.width(), rconf.height());

	g_signal_connect(window, "delete-event",
			G_CALLBACK(delete_event), NULL);
	g_signal_connect(window, "destroy",
			G_CALLBACK(destroy), NULL);

	gtk_widget_add_events(da, GDK_BUTTON_PRESS_MASK);
	g_signal_connect(da, "button-press-event", 
			G_CALLBACK(cb_clicked), NULL);

	g_signal_connect(da, "expose_event",
			G_CALLBACK(da_expose_callback), NULL);

	g_timeout_add(1000/30, (GSourceFunc)cb_timeout, da);

	gtk_container_add(GTK_CONTAINER(window), da);
	gtk_widget_show_all(window);

	gtk_main();

	//img = gdk_pixbuf_new_from_file("frog.png", NULL);
	//img = gdk_pixbuf_scale_simple(img, 100, 100, GDK_INTERP_HYPER);
	/* }}} */
}


#endif

/*
 * '-' indicates stdout
 */
void image_save_to_ppm(Image *image, const string &filename)
{

	FILE *fout;
	if (filename == "-")
		fout = stdout;
	else fout = fopen((filename).c_str(), "w");

	fprintf(fout, "P6\n%d %d\n255\n", image->width, image->height);
	unsigned char pixel[3];
	for (int j = 0; j < image->height; j ++)
		for (int i = 0; i < image->width; i ++)
		{
			ColorRGB *c = image->data + i * image->height + j;
			pixel[0] = c->red * 255;
			pixel[1] = c->green * 255;
			pixel[2] = c->blue * 255;
			fwrite(pixel, sizeof(pixel), 1, fout);
		}

	if (fout != stdout)
		fclose(fout);
}

void image_save_to_pgm(Image *image, const string &filename)
{

	FILE *fout;
	if (filename == "-")
		fout = stdout;
	else fout = fopen((filename).c_str(), "w");

	fprintf(fout, "P5\n%d %d\n255\n", image->width, image->height);
	unsigned char pixel;
	for (int j = 0; j < image->height; j ++)
		for (int i = 0; i < image->width; i ++)
		{
			ColorRGB *c = image->data + i * image->height + j;
			pixel = c->red * 255;
			fwrite(&pixel, sizeof(char), 1, fout);
		}

	if (fout != stdout)
		fclose(fout);
}


/* }}} */

template<typename T>
void check_lower_bound(T &num, T lower_bound)
{
	if (num < lower_bound)
		num = lower_bound;
}

int str2num(const std::string &str)
{
	int ret = 0, sign = 1, start = 0;
	if (str[0] == '-')
		sign = -1, start = 1;
	for (size_t i = start; i < str.length(); i ++)
		ret = ret * 10 + str[i] - '0';
	return ret * sign;
}

real_t str2real(const std::string &str)
{
	real_t ret;
	sscanf(str.c_str(), "%" REAL_T_FMT, &ret);
	return ret;
}


GrphRdr *GrphRdrFactory(const std::string &name, int argc, char *argv[])
{
#ifdef ENABLE_PTHREAD
	if (name == "pthread")
		return new PthreadRdr();
#endif

#ifdef ENABLE_OPENMP
	if (name == "openmp")
		return new OpenMPRdr();
#endif

#ifdef ENABLE_MPI
	if (name == "mpi")
		return new MPIRdr(argc, argv, 100000);
#endif

	log_printf("unkown renderer: %s", name.c_str());
	exit(-1);
}

const char *progname;

void print_help()
{
	printf("Usage: %s [options]\n", progname);
	printf("Options:\n");
	printf("    --nworker=NUM, -n    number of workers. number of CPUs by default.\n");
	printf("                         be of no use when use MPI as parallel backend\n");
	printf("    --size=SIZE, -s      size of picture. SIZE is of format `<width>x<height>'\n");
	printf("                         eg. `800x800', \n");
	printf("    --domain=DOMAIN      plotting domain. DOMAIN is of format \n");
	printf("                         `<x>,<y>,<width>,<height>', where x and y are \n");
	printf("                         left-bottom coordinates\n");
#ifdef USE_GTK
	printf("    --show=COL, -w   pop up a window show the result after plot,\n");
	printf("                         COL either `gray' or `rgb'.\n");
	printf("                         you can click left/right mouse button to zoom in/out,\n");
	printf("                         and click middle mouse button to move\n");
	printf("    --zoom=ZOOM, -z      specifiy zoom scale, in real number. 2.0 by default\n");
#endif
	printf("    --niter=NUM, -i      set max number of iteration of function\n");
	printf("    --parallel=METHOD, -p set parallel computing method, one of `pthread', \n");
	printf("                         `openmp' and `mpi'. `pthread' by default.\n");
	printf("                         IMPORTANT: if you want to use MPI, please run with\n");
	printf("                         mpirun or other equavalent command\n");
	printf("    --goutput=FILE, -g   specify output file for gray pgm image,\n");
	printf("                         '-' for stdout, 'output-gray.pgm' by default.\n");
	printf("    --coutput=FILE, -c   specify output file for rgb ppm image,\n");
	printf("                         '-' for stdout, 'output-rgb.ppm' by default.\n");
	printf("                         output is in PPM file format\n");
	printf("    --nooutput, -O       suppress output file\n");
	printf("    --help, -h           show this help and quit\n");
}

int main(int argc, char *argv[])
{
	progname = argv[0];

	int width = 720, height = 720;
	int niter_max = 256;
	bool show = false;
	Rectangle rect(-1.5, -1, 2, 2);
	std::string parallel_method = "pthread";

	int nworker = sysconf(_SC_NPROCESSORS_ONLN);
	option long_options[] = {
		{"nworker",		required_argument,	NULL, 'n'},
		{"size",		required_argument,	NULL, 's'},
		{"domain",		required_argument,	NULL, 'd'},
		{"show",		required_argument,	NULL, 'w'},
		{"zoom",		required_argument,	NULL, 'z'},
		{"niter",		required_argument,	NULL, 'i'},
		{"parallel",	required_argument,	NULL, 'p'},
		{"goutput",		required_argument,	NULL, 'g'},
		{"coutput",		required_argument,	NULL, 'c'},
		{"nooutput",	no_argument,		NULL, 'O'},
		{"help",		no_argument,		NULL, 'h'},
	};

	string coutput = "output-rgb.ppm";
	string goutput = "output-gray.pgm";
	int opt;
	bool no_output = false;
	while ((opt = getopt_long(argc, argv, "Oz:g:c:o:p:wd:hs:n:i:", long_options, NULL)) != -1)
	{
		switch (opt)
		{
			case 'O':
				no_output = true;
				break;
#ifdef USE_GTK
			case 'z':
				zoom_scale = str2real(optarg);
				if (zoom_scale < 0)
					zoom_scale = 1;
				break;
#endif
			case 'h':
				print_help();
				return 0;
			case 'c':
				coutput = optarg;
				break;
			case 'g':
				goutput = optarg;
				break;
			case 'p':
				parallel_method = optarg;
				break;
#ifdef USE_GTK
			case 'w':
				{
					show = true;
					std::string param = optarg;
					rconf.set_show_type(RenderConfig::ST_RGB);
					if (param == "rgb")
						rconf.set_show_type(RenderConfig::ST_RGB);
					if (param == "gray")
						rconf.set_show_type(RenderConfig::ST_GRAY);
				}
				break;
#endif
			case 'i':
				niter_max = str2num(optarg);
				check_lower_bound(niter_max, 1);
				break;
			case 'n':
				nworker = str2num(optarg);
				check_lower_bound(nworker, 1);
				break;
			case 's':
				{
					int w, h;
					if (sscanf(optarg, "%dx%d", &w, &h) != 2)
						error_exit(-1, "invalid size `%s'", optarg);
					width = w, height = h;
				}
				break;
			case 'd':
				{
					real_t x, y, w, h;
					if (sscanf(optarg, "%" REAL_T_FMT ",%" REAL_T_FMT ",%" REAL_T_FMT ",%" REAL_T_FMT,
								&x, &y, &w, &h) != 4)
						error_exit(-1, "invalid domain `%s'", optarg);
					rect = Rectangle(x, y, w, h);
				}
				break;
		}
	}


	fprintf(stderr, "niter_max: %d\n", niter_max);
	rconf.rdr = GrphRdrFactory(parallel_method, argc, argv);
	rconf.func = new Mandelbrot(Mandelbrot::Setting(Mandelbrot::TERM_FLAG_NITER, niter_max));
	//rconf.func = new Tim(palette, Tim::Setting(Tim::TERM_FLAG_NITER, niter_max));
	rconf.gc = GrphRdrConf(width, height, rect, nworker, no_output);
	if (!rconf.render())
		return 0;

#ifdef USE_GTK
	image_init(argc, argv);
	rconf.to_pixbuf();
#endif
	if (no_output == false && rconf.output)
	{
		image_save_to_ppm(rconf.output->rgb, coutput);
		image_save_to_pgm(rconf.output->gray, goutput);
	}

#ifdef USE_GTK
	if (show)
		image_show();
#endif

	return 0;
}

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


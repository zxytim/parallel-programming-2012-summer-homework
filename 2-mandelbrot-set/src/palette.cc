/*
 * $File: palette.cc
 * $Date: Sat Aug 25 20:04:57 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include "palette.hh"

#include "lib/sassert.hh"

Palette::Palette(int size)
{

	ColorRGB col(1, 1, 1);
	int drgb[][3] = {
		{-1, 0, 0}, // 0 1 1
		{0, -1, 0}, // 0 0 1
		{1, 0, 0}, // 1 0 1
		{0, 0, -1}, // 1 0 0
		{0, 1, 0}, // 1 1 0
		{-1, 0, 0}, // 0 1 0
		{0, -1, 0} // 0 0 0
	};
	int nphase = sizeof(drgb) / sizeof(int) / 3;

	m_size = size / nphase * nphase;
	m_size += (m_size == size ? 0 : nphase);

	palette = new ColorRGB[m_size];

	int psize = m_size / nphase; // size of each transform phase
	real_t delta = 1.0 / psize;
	for (int i = 0, pos = 0, phase = 0; i < m_size; i += psize, phase ++)
	{
		int *udelta = drgb[phase]; // unit delta
		for (int j = 0; j < psize; j ++, pos ++)
		{
			col.red += udelta[0] * delta;
			col.green += udelta[1] * delta;
			col.blue += udelta[2] * delta;
			assert(pos < m_size);
			palette[pos] = col;
		}
	}
}

ColorRGB Palette::color(int seed)
{
	return palette[seed % m_size];
}

Palette::~Palette()
{
	delete [] palette;
}

GrayPalette::GrayPalette(int size)
{
	size ++;
	m_size = size;
	palette = new ColorRGB[size];
	for (int i = 0; i < size; i ++)
	{
		real_t col = (size - 1 - i) / (real_t)size;
		palette[i] = ColorRGB(col, col, col);
	}
}


/**
 * vim: syntax=cpp11 foldmethod=marker
 */


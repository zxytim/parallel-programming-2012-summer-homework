#!/bin/bash

DIR=../log
OUTDIR=../trimed
for para in openmp pthread mpi mpi.s; do
	echo $para
	dir=$DIR/$para
	outdir=$OUTDIR/$para
	mkdir -p $outdir
	for fname in  `cd $dir && ls *.stdout`; do
		np=`echo $fname | cut -d '-' -f 7 | cut -d '.' -f1`
		nbody=`echo $fname | cut -d'-' -f 5`
		echo $nbody-$np
		frames=`cat $dir/$fname | grep fps | sed -e 's/[^0-9\.]//g'`
		echo $frames > $outdir/$nbody-$np.log
	done
done

%
% $File: report.tex
% $Date: Fri Aug 31 09:28:00 2012 +0800
% $Author: Xinyu Zhou <zxytim@gmail.com>
%

\documentclass{article}
\usepackage{fontspec}
\usepackage{zhspacing,url,amsmath,amssymb,verbatim}
\zhspacing
\usepackage{listings}
\usepackage[hyperfootnotes=false,colorlinks,linkcolor=blue,anchorcolor=blue,citecolor=blue]{hyperref}
\usepackage[sorting=none]{biblatex}
%\usepackage[dvips]{graphicx}
\usepackage{minted}
\usepackage{subfigure}
\usepackage{indentfirst}
\usepackage{cases}
\usepackage{environ}
\usepackage{array}
\usepackage{graphicx}
\usepackage[top=1in, bottom=1in, left=1.25in, right=1.25in]{geometry}
%\usepackage{tikz}
%\usepackage{dot2texi}

\input{mint-defs.tex}

\newcommand{\figref}[1]{\hyperref[fig:#1]{Figure\ref*{fig:#1}}}
\newcommand{\tableref}[1]{\hyperref[table:#1]{Table\ref*{table:#1}}}
\newcommand{\centerize}[1]{\begin{center} #1 \end{center}}

\newcommand{\cmd}[1]{{\it #1}}
\newcommand{\ccmd}[1]{\centerize{\cmd{#1}}}

\title{N-body Simulation using OpenMP, Pthread and MPI}
\author{Xinyu Zhou(周昕宇)\\ Dept. of CST, THU\\ ID: 2011011252}
\date{\today}

\addbibresource{refs.bib}
\begin{document}
\maketitle

\newcommand{\nbs}{ {\bf N-body Simulation}  }
\newcommand{\pthread} { {\it pthread} }
\newcommand{\openmp} { {\it OpenMP} }
\newcommand{\mpi} { {\it MPI} }


\begin{abstract}
	An \nbs ~is a simulation of a dynamical system of particles under
	the influence of gravity.\cite{wiki_nbs}
	Here the author compare efficiency among programs using three kinds of
	parallel backends: \openmp, \pthread, and \mpi.

	This is {\it homework 4} for course {\it Parallel Programming}

	{\bf Keyword} N-body, simulation, parallel
\end{abstract}

\tableofcontents

\clearpage

\section{Instruction}
	You can simply follow $make->make~run$ to run the program. For more
	detailed instruction, seed Appedix~\ref{command_line_help}

\section{Design \& Approach}
	\subsection{Definitions}
		\begin{itemize}
			\item {\bf Body}
				A body is a sphere shaped particle with radius, mass, position
				and velocity in 3-Dimensional space with boundaries.
			\item {\bf Engine}
				An engine is the backend of calculating gravitational effect
				brings on a single body and collision between bodies.
			\item {\bf Worker}
				A worker schedules the calculation using engine. In case here,
				worker can be one of the three parallel workers.
		\end{itemize}

	\subsection{Overview}
		Simulation consists of multiple rounds. In each round, program
		simulates the state of the system within $\Delta t$ seconds, with the
		approximation of considering the motion as uniform linear motion within
		a round.

		Each round consists of two phases: 
		\begin{enumerate}
			\item 
				For each body, calculate the net force exerted on it, and
				update its velocity.
			\item
				Calculate collisions between bodies and between partile and
				space boundaries.  Here a strong assumption is used: each body
				will only collide once with another body (or boundary), which
				it will collide the earliest one.

				Since N-body collision problems needs to solve lots of
				equations, that's not the key point here. This assumption saves
				a great number of calculations and make simulation more
				feasible.
		\end{enumerate}

	\subsection{The Engine}
		There are two engines available: 
			\subsubsection{NaiveEngine} 

				NaiveEngine will calculate accelaration {\bf precisely}:
				accumulate all forces exerted on it using formula
				\centerize{$F_{net} = \Sigma \dfrac{G \cdot m \cdot m_{other}}{r^2}$}
				and the accelaration with 
				\centerize{$a_{net} = \Sigma \dfrac{F_{net}}{m} = \Sigma \dfrac{G \cdot m_{other}}{r^2} $}
				then the new velocity will be
				\centerize{$v\prime = v + a_{net} * \Delta t$}
				
				For collision, enumerate all possible collision with boundaries
				and other bodies, and then select the earliest one. Reponse to
				collision will be explained in next part.

				Apparently, this method gives an $O(n^2 + n^2) = O(n^2)$
				algorithm for $n$ bodies.

			\subsubsection{BSPTree}

				BSPTree(Binary Space Partitioning Tree) is a data structure
				which can arrange bodies in a unbounded space (comparing to 
				Octree which deals with only finite bounded space) in a 
				more reasonable way, due to which will lead to 
				quicker calculation.

				A node in tree holds information of the subtree rooted on that
				node. 
				Here are details of the tree:
				\begin{description}
					\item {\bf Axis Aligned Box(AABox)} 

						An axis aligned box is a cuboid whose faces are all
						perpendicular to one of the tree axis, say x, y and z
						axis.

					\item {\bf Construction of the tree}

						The tree is recursively constructed from the root with 
						all bodies given.
						\begin{enumerate}
							\item 
								Calculate infromation that need to be hold on
								this node: An AABox which can hold bodies
								given, the total mass and the center of mass of
								all given bodies.
							\item Choose either axis, and project center of all
								bodies, on that axis. 
							\item Find the body who lies in the middle, and
								divide the bodies into two quasi-equal part(at
								most differs from one body), each part forms a
								child node of current node. We also store the
								dividing plane in the node.
							\item recursively construct the tree with the two
								parts of the bodies.
						\end{enumerate}
						
						With $O(n)$ algorithm to find the median, and
						$O(\log{n})$ depth of the tree, construction of the
						tree can be done in $O(n \log{n})$ time. And with
						apparently $O(n)$ nodes in the tree with constant
						information stored on each node, space complexity is
						$O(n)$.

					\item {\bf Acceleration Calculation}

						For a given body, we calculate its acceleration due to
						all other bodies start from the root of the tree as
						follow:
						\begin{enumerate}
							\item
								If current node is a leaf node (which contains
								just one body), or current node seems
								insignificant from the given body, we consider
								all bodies in that subtree as a single point
								located in the center of mass, and then
								accumulate the acceleration obtained.

								Here we define the insignificance $\theta$ of a
								node to a body as: \centerize{$\theta =
								\dfrac{V}{D^3}$} where $V$ is the volume of
								AABox of the node, and $D$ is the distance
								between CM of the node and the body.

								We also set a threshold $\eta$ empirically, and
								consider nodes whose $\theta < \eta$ as
								insignificant to the given body.  The large
								$\eta$, the faster and imprecise the
								calculation will be.

							\item
								Else consider the acceleration separately on
								each child node.
						\end{enumerate}

						With certain value of $\eta$, we can expect a much less
						time for each body to obtain its acceleration, expecting
						a $O(n \log{n})$ time complexity.

					\item {\bf Body Collision}

						Since we assumed a uniform linear motion in $\Delta t$
						seconds for all bodies, we consider a line segment
						$l_i$ constructed by position of the body $b_i$ at time
						$0$ and time $\Delta t$. Also, we expand the AABox
						within each node to include all bodies on its
						subtree\footnote{This may be optional, for smaller
						running constant you may omit this step}.

						For each body, we find its earliest collision may
						happen with another body in $\Delta t$ time in the tree
						start with root node as follow:
						\begin{enumerate}
							\item construct a new AABox which is the AABox of
								current node but with a expand in width of the
								body's diameter.
							\item if $l_i$ does not intersect the new AABox of
								current node, no further inspection is needed,
								thus return to its root node.
							\item if current node is a leaf node, calculate the
								collision.
							\item if $l_i$ intersect the new AABox of current node,
								inspect both of its child node for further
								investigation.
						\end{enumerate}

						Also, only a few amount of node need to be checked,
						much less time is consumed. If the speed of the body
						is not that large comparing to the volume of AABox,
						this algorithm yields a time complexity of 
						$O(n \log{n})$

						We assume two body $b_i$ and $b_j$ will hit at time
						$t$, then we have equations
						$$
							\left \{
								\begin{aligned}
									\mathbf{p_i \prime} &= \mathbf{p_i} 
										+ \mathbf{v_i} \cdot t \\
									\mathbf{p_j \prime} &= \mathbf{p_j}
										+ \mathbf{v_j} \cdot t \\
									|\mathbf{p_i} - \mathbf{p_j}| &= r_i + r_j
								\end{aligned}
							\right .
						$$
						where $\mathbf{p_i},\mathbf{v_i},r_i$ are the position,
						velocity and radius of $b_i$ respectively.
						This is a quadric equation set, and we can simply
						choose the smallest opposite $t$ as the solution. If
						such $t$ does not exist or $t > \Delta t$, then the two
						body will not collide.

						Finally, when knowing the two body will collide, the
						response to collision of two body $b_1$ and $b_2$ is
						calculated as follow\cite{col_rspns}:
						$$
							\begin{aligned}
								\mathbf{u} &= \dfrac{\mathbf{p_1} - 
								\mathbf{p_2}}{|\mathbf{p_1} - \mathbf{p_2}|} \\
								u_1 &= \mathbf{u} \cdot \mathbf{v_1} \\
								\mathbf{v_{1x}} &= \mathbf{u} \cdot u_1 \\
								\mathbf{v_{1y}} &= \mathbf{v_1} - \mathbf{v_{1x}} \\
								u_2 &= \mathbf{-u} \cdot \mathbf{v_2} \\
								\mathbf{v_{2x}} &= \mathbf{-u} \cdot u_2 \\
								\mathbf{v_{2y}} &= \mathbf{v_2} - \mathbf{v_{2x}} \\
								\mathbf{v_1 \prime} &= 
								\mathbf{v_{1x}} \dfrac{m_1-m_2}{m_1+m_2} 
								+ \mathbf{v_{2x}} \dfrac{2\cdot m_2}{m_1+m_2}
								+ \mathbf{v_{1y}} \\
								\mathbf{v_2 \prime} &= 
								\mathbf{v_{1x}} \dfrac{2\cdot m_1}{m_1+m_2} 
								+ \mathbf{v_{2x}} \dfrac{m_2-m_1}{m_1+m_2} 
								+ \mathbf{v_{2y}} \\
							\end{aligned}
						$$
						where $v_1\prime$ and $v_2\prime$ are velocity after
						collision.

				\end{description}

		\subsection{The Worker}
			Three workers are:
			\subsubsection{\openmp}
				As earlier described, there are two phases in each round, in
				which every body will be inspect for calculation. Due to the
				simplicity of the task, the approach is quite straightforward
				as always: just put parallel instruction ahead of {\it for}
				loops, an everything works like a charm.
			\subsubsection{\pthread}
				For two phases in each round, dynamically distribute
				calculation tasks (both calculation of acceleration and
				collision) among threads using a task scheduler.
			\subsubsection{\mpi}
				The program is running in {\it quasi-master-slave} mode.
				Due to the amount of calculation for each body is roughly equal,
				equal-amount assignment of calculation tasks is hereby employed.
				All processes are responsible for calculation, but only the
				so-called master process plots the result. For continuous rendering
				of frames, slave process stucks in calculation procedure,
				and repeat the configuration receiving, acceleration
				calculation, collision calculation loop until master process
				emit the quit signal\footnote{same as HW2}.


\section{Result \& Analysis}
	For time saving, all workers are limited to a running time of 20 seconds with
	different number of bodies. Efficiency is test by exhaustively calculating
	as much frames as the program can within 20 seconds, then count the frames.

	The initial setting of the body is that, one body with huge mass in the
	center at rest, and the remaining body with small mass lies around the body
	in a circle, with a tangential velocity to the center.

	$\Delta t$ is set to $0.01$ seconds.

	Efficiency of the program with same nubmer of bodies is calculated by
	\centerize{$E = \dfrac{f \cdot m}{F \cdot n} $}
	where $f$ denote the average FPS, $m$ denote minmum number of workers used,
	$F$ denote the average FPS with minimum number of workers and 
	$n$ denote the number of workers used in program.
\clearpage
	\subsection{\openmp}
		\input{data/tex/nbs.openmp.general.fig.tex}
		\input{data/tex/nbs.openmp.table.tex}
		\input{data/tex/nbs.openmp.efcc.table.tex}

		Here \openmp yield greate performance in speeding up program, even faster
		that carefully coded \pthread version. This may due to the more
		variables exposed to the {\it for} loop, and \openmp did optimizations
		that deeply under the hood for variable management during compilation
		which can not be accomplished if completely using \pthread, which stays on
		the function layer.
		\clearpage

	\subsection{\pthread}
		\input{data/tex/nbs.pthread.general.fig.tex}
		\input{data/tex/nbs.pthread.table.tex}
		\input{data/tex/nbs.pthread.efcc.table.tex}
		
		Supprisingly, both performance and efficiency of \pthread is not as I
		expeced as it should be, which is the fastest among three kinds of 
		parallel method. This may be caused by over use of dynamic scheduleing
		scheme which has a great load banlance effect when calculation of
		single element varies a lot, but a burden when calculation on
		elements are roughly equal.
		\clearpage
	\subsection{\mpi}
		\input{data/tex/nbs.mpi.general.fig.tex}
		\input{data/tex/nbs.mpi.table.tex}
		\input{data/tex/nbs.mpi.efcc.table.tex}
		The most unreasonable result comes from program programed with \mpi.
		\mpi runs perfectly with reasonable speed up on single machine,
		but it seems that, \mpi does not work on clusters for some unknown reason.
		It is true that the program actually used desired amount of processes
		(from the logging information), but with {\bf NO} speed up. I run it 
		again and yields the same result. 
		
		Due to this, I re-execute the program on a 12-core test node(c01b04),
		and the result is on the following page
		\clearpage

	\subsection{\mpi~on single machine}
		\input{data/tex/nbs.mpi.s.general.fig.tex}
		\input{data/tex/nbs.mpi.s.table.tex}
		\input{data/tex/nbs.mpi.s.efcc.table.tex}

		\mpi~works well on single machine, and with resonable speed up.  The
		leap in efficiency with 65535 bodies from using 4 proceses to 5
		processes may due to instability of the mathine.  A
		much-higher-than-one efficiency when number of processes from 7 up to
		12 due to the bodies in latter frames needs to be render spread out
		widely in the space, which lead to a more efficient query in BSPTree.

		\clearpage
	\subsection{Brief Summary}
		The execution efficiency by multi-workers varies much, and seems low
		when number of bodies are not big enough. The high efficiency appeared
		in big data configuration is due to after a large amount of collision
		in the begining, bodies disperses much, and a more efficient query in
		BSPTree obtained.

		The high running constant lies in building up the tree every round (
		with find the median, construct the AABox), massive amount of array copy,
		etc. But the overall algotirhm obtained by BSPTree has a much smaller time
		complexity that that of naive method, and of couse, a much smaller running
		time.

\section{Experience}
	Converting a sequential program to parallel program which yiels a high efficiency
	happens much when the algorithm is naive and with lots of simple, similar operations.

	But when algorithm used its self is complex, the efficiency gained by
	parallelize the program may be critically low. But a more efficient algorithm
	is always better than more efficient use of machine, since in any aspect,
	the running time is much smaller than algorithm with high time complextiy,
	and overall consumption on resources are much smaller.

\clearpage
\appendix
\section{Specific Result}
	\input{data/tex/nbs.openmp.spec.fig.tex}
	\input{data/tex/nbs.pthread.spec.fig.tex}
	\input{data/tex/nbs.mpi.spec.fig.tex}
	\input{data/tex/nbs.mpi.s.spec.fig.tex}

\section{Source Code}
	\input{src.tex}


\clearpage
\printbibliography

\end{document}


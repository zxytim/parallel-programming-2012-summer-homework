/*
 * $File: AABox.hh
 * $Date: Wed Aug 29 10:44:18 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __AABOX_HH__
#define __AABOX_HH__

#include "nbody/body.hh"
#include "lib/type.hh"
#include "lib/misc.hh"

struct AABox
{
	Vector min_coord, max_coord;
	inline real_t volume() const {
		real_t ret = 1;
		for (int i = 0; i < 3; i ++)
			ret *= max_coord[i] - min_coord[i];
		return ret;
	}

	void reset() { 
		min_coord = Vector(REAL_MAX, REAL_MAX, REAL_MAX);
		max_coord = Vector(REAL_MIN, REAL_MIN, REAL_MIN);
	}

	bool in_box(const Vector &pos) const
	{
		for (int i = 0; i < 3; i ++)
			if (pos[i] > max_coord[i] || pos[i] < min_coord[i])
				return false;
		return true;
	}

	AABox enhance(real_t width) const
	{
		AABox ret(*this);
		for (int i = 0; i < 3; i ++)
		{
			ret.min_coord.coord(i) -= width;
			ret.max_coord.coord(i) += width;
		}
		return ret;
	}

	// detect whether a line collide with AABox,
	bool collide(const Vector &pos, const Vector &vel, real_t dtime) const
	{
		if (in_box(pos))
			return true;

		Vector veps = vel.unit() * EPS;
		for (int aid = 0; aid < 3; aid ++) // axis id
		{
			real_t v = vel[aid];
			if (fabs(v) < EPS)
				continue;

			real_t dmin = min_coord[aid] - pos[aid],
				   tmin = dmin / v;
			if (tmin > 0 && tmin <= dtime && in_box(pos + vel * tmin + veps))
				return true;

			real_t dmax = max_coord[aid] - pos[aid],
				   tmax = dmax / v;
			if (tmax > 0 && tmax <= dtime && in_box(pos + vel * tmax + veps))
				return true;
		}
		return false;
	}

	bool collide_info(const Body &body, real_t &time, int &axis, int &mag)
	{
		time = REAL_MAX;
		for (int i = 0; i < 3; i ++)
		{
			real_t v = body.v[i];
			if (fabs(v) < EPS)
				continue;
			real_t dmin = body.pos[i] - body.radius - min_coord[i],
				   tmin = dmin / -v;
			if (tmin > 0 && tmin < time)
				time = tmin, axis = i, mag = 0;
			real_t dmax = max_coord[i] - body.pos[i] - body.radius,
				   tmax = dmax / v;
			if (tmax > 0 && tmax < time)
				time = tmax, axis = i ,mag = 1;
		}
		return time != REAL_MAX;
	}
};

#endif // __AABOX_HH__
/**
 * vim: syntax=cpp11 foldmethod=marker
 */


/*
 * $File: collision.hh
 * $Date: Wed Aug 29 10:34:23 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __COLLISION_HH__
#define __COLLISION_HH__

struct Collision
{
	int type; // -1: none 0: body-to-body 1: body-to-wall
	int id0, id1;
	real_t time;
	int axis, mag; // min or max
	Collision() {}
	Collision(int id0, int id1, real_t time) :
		id0(id0), id1(id1), time(time) {}
};

#endif // __COLLISION_HH__
/**
 * vim: syntax=cpp11 foldmethod=marker
 */

/*
 * $File: engine.hh
 * $Date: Wed Aug 29 10:34:31 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __ENGINE_HH__
#define __ENGINE_HH__

#include "nbody/nbody.hh"
#include "collision.hh"

class Engine
{
	public:

		virtual void init(const NBodyConfig &conf) = 0;
		virtual Collision collision_detect(int id, real_t dtime) = 0;

		// let theta be the quotient between the volume represented
		// by a node and the distance cubed between CM and the point.
		// if the quotient lies bellow threshold, the approximation
		// will be used
		virtual void cal_new_velocity(int id, const real_t &threshold, const real_t &dtime) = 0;
};

#endif // __ENGINE_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


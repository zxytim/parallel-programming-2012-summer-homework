/*
 * $File: bsp.cc
 * $Date: Thu Aug 30 20:12:16 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include "bsp.hh"
#include "nbody/const.hh"

#include <cmath>
#include <algorithm>
#include <cstring>
#include <cassert>

using namespace std;

#define For(i, n) for (int i = 0; i < (n); i ++)

static int * alloc_ordered_array(int n)
{ 
	int *ret = new int[n];
	For(i, n) ret[i] = i;
	return ret;
}

BSPTree::BSPTree()
{
	root = NULL;
}


void BSPTree::release(Node *root)
{
	if (!root)
		return;
	release(root->ch[0]);
	release(root->ch[1]);
	delete root;
}

void BSPTree::init(const NBodyConfig &conf)
{
	release(root);

	m_nbody = conf.nbody;
	body = conf.body;
	m_dep_max = log((real_t)m_nbody) / log(2.0) * 4;

	boundary.min_coord = conf.domain.min_coord;
	boundary.max_coord = conf.domain.max_coord;

	//memcpy(m_body, conf.body, sizeof(Body) * m_nbody);
	
	int *ids = alloc_ordered_array(this->m_nbody);
	root = build_tree(ids, m_nbody, 0);
	delete [] ids;
}

static real_t cube(const real_t &x) { return x * x * x; }

AABox BSPTree::constructAABox(int *ids, int n)
{
	AABox box;
	box.reset();
	for (int i = 0; i < n; i ++)
	{
		int id = ids[i];
		for (int j = 0; j < 3; j ++)
		{
			check_min(box.min_coord.coord(j), body[id].pos.coord(j) - body[id].radius);
			check_max(box.max_coord.coord(j), body[id].pos.coord(j) + body[id].radius);
		}
	}

	return box;
}

int BSPTree::find_middle(int *ids, int n, int cid)
{
	//std::sort(ids, ids + n, CmpBody(body, cid));
	//return n / 2;

	int left = 0, right = n;

	int *buf = new int[n];
	int k = (n + 1)/ 2;
	
	while (left + 1 != right)
	{
		int mid = (left + right) >> 1;
		Body &b = body[ids[mid]];
		real_t bm = b.pos.coord(cid); // benchmark
		int head = 0, tail = right - left;
		for (int i = left; i < right; i ++)
			if (body[ids[i]].pos.coord(cid) < bm)
				buf[head ++] = ids[i];
			else buf[-- tail] = ids[i];
		while (tail < n && body[buf[tail]].pos.coord(cid) == body[buf[head]].pos.coord(cid))
			tail ++;
		memcpy(ids + left, buf, sizeof(int) * (right - left));
		if (k < head)
			right = left + head;
		else if (k > tail)
			left = left + tail, k -= tail;
		else
		{
			assert(left <= (n + 1) / 2 && (n + 1) / 2 < right);
			return (n + 1) / 2;
		}
	}

	delete buf;

	return right;
}


void BSPTree::check()
{
	return ;
	for (int i = 0; i < m_nbody; i ++)
	{
		assert(!isnan(body[i].pos.x));
		assert(!isnan(body[i].pos.y));
		assert(!isnan(body[i].pos.z));
	}
}

BSPTree::Node *BSPTree::build_tree(int *ids, int nbody, int dep)
{
	check();
	if (nbody == 0)
		return NULL;

	Node *root = new Node();
	root->dep = dep;
	root->box = constructAABox(ids, nbody);
	root->n = nbody;

	root->cm = Vector(0, 0, 0);
	root->mass = 0;
	For(i, nbody)
	{
		int id = ids[i];
		root->cm += body[id].pos * body[id].mass;
		root->mass += body[id].mass;
	}
	root->cm /= root->mass;

	if (nbody == 1)
	{
		root->id = *ids;
		return root;
	}

	root->plane.axis = Rand(3);

	int mid = find_middle(ids, nbody, root->plane.axis);
	assert(mid != nbody);
	root->plane.coord = (body[ids[mid]].pos[root->plane.axis] + 
			body[ids[mid - 1]].pos[root->plane.axis]) / 2;

	root->ch[0] = build_tree(ids, mid, dep + 1);
	root->ch[1] = build_tree(ids + mid, nbody - mid, dep + 1); 
	return root;
}

Collision BSPTree::collision_detect(int id, real_t dtime)
{
	check();
	Vector pos = body[id].pos + body[id].v * dtime;
	real_t time = REAL_MAX;
	int axis, mag;
	Collision col;
	col.id0 = id;
	col.time = REAL_MAX - 1;
	if (boundary.collide_info(body[id], time, axis, mag))
		if (time > 0 && time < dtime)
		{
			col.id1 = -1;
			col.time = time;
			col.axis = axis;
			col.mag = mag;
		}

	Collision tcol = do_col_dtct(root, id, dtime);
	if (tcol.time > 0 && tcol.time < col.time)
		col = tcol;
	if (col.time > dtime || isinf(col.time))
	{
		col.time = -1;
		col.id1 = -1;
	}
	assert(!isinf(col.time));
	return col;
}

Collision BSPTree::do_col_dtct(Node *root, int id, real_t dtime)
{
	assert(body[id].id == id);
	Body &b = body[id];
	if (root->is_leaf())
		return body_clsdtct(id, root->id, dtime);

	if (!root->box.enhance(b.radius).collide(b.pos, b.v, dtime))
		return Collision(-1, -1, -1);

	int chd = -1;
	if (root->ch[0] == NULL) chd = 1;
	else if (root->ch[1] == NULL) chd = 0;
	else if (b.pos[root->plane.axis] < root->plane.coord) chd = 0;
	else chd = 1;

	Collision ret = do_col_dtct(root->ch[chd], id, dtime);
	if (ret.id0 != -1)
		return ret;
	return do_col_dtct(root->ch[!chd], id, dtime);
}

static bool solve_quadratic_equation(const real_t &a, const real_t &b, const real_t &c,
		real_t &root0, real_t &root1)
{
	if (fabs(a) < EPS)
		return false;
	real_t delta = b * b - 4 * a * c;
	if (delta >= 0)
	{
		real_t sq = sqrt(delta),
			   d = 1 / (2 * a);
		root0 = (-b - sq) * d;
		root1 = (-b + sq) * d;
		return true;
	}
	return false;
}

Collision BSPTree::body_clsdtct(int id0, int id1, real_t dtime)
{
	if (id0 == id1)
		return Collision(-1, -1, -1);
	Body &b0 = body[id0], &b1 = body[id1];
	Vector AB = b1.pos - b0.pos,
		   vab = b1.v - b0.v;
	real_t rab = (b0.radius + b1.radius);
	real_t a = vab.dot(vab),
		   b = 2 * vab.dot(AB),
		   c = AB.dot(AB) - rab * rab;

	real_t t0, t1, t;
	if (!solve_quadratic_equation(a, b, c, t0, t1))
		return Collision(-1, -1, -1);
	t = t0;
	if (t < 0) t = t1;
	if (t < 0 || t > dtime) return Collision(-1, -1, -1);
	if (t > EPS)
		t -= EPS / 2;
	return Collision(id0, id1, t);
}

void BSPTree::cal_new_velocity(int id, const real_t &threshold, const real_t &dtime)
{
	Body &b = body[id];
	Vector a = calc_acceleration(root, id, threshold);
	b.v += a * dtime;
	int asdf = 0;
}

Vector BSPTree::calc_acceleration(Node *root, int id, real_t threshold)
{
	if (root == NULL)
		return Vector(0, 0, 0);
	assert(!isnan(root->cm.x));
	Body &b = body[id];
	
	real_t cube_len = cube((root->cm - b.pos).length());
	real_t t = root->volume() / cube_len;
	if (root->is_leaf() || root->volume() / cube_len < threshold)
		return calc_single_acc(b, root);
	return calc_acceleration(root->ch[0], id, threshold)
		+ calc_acceleration(root->ch[1], id, threshold);
}

Vector BSPTree::calc_single_acc(const Body &a, const Node *b)
{
	if (b->is_leaf() && b->id == a.id)
		return Vector(0, 0, 0);

	Vector dir = b->cm - a.pos;
	real_t dist_sqr = dir.lengthsqr();
	real_t magnitude = Const::G * b->mass / dist_sqr;

	Vector ret = dir / sqrt(dist_sqr) * magnitude;
	if (dir.lengthsqr() < a.radius * a.radius)
		return -ret;

	return ret;
}

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


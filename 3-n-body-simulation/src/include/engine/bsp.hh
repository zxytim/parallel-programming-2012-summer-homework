/*
 * $File: bsp.hh
 * $Date: Thu Aug 30 13:47:52 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __BSP_HH__
#define __BSP_HH__

#include "nbody/nbody.hh"
#include "lib/misc.hh"
#include "engine.hh"
#include "AABox.hh"

class BSPTree : public Engine
{
	public:
		virtual void init(const NBodyConfig &conf);

		virtual Collision collision_detect(int id, real_t dtime);

		// let theta be the quotient between the volume represented
		// by a node and the distance cubed between CM and the point.
		// if the quotient lies bellow threshold, the approximation
		// will be used
		virtual void cal_new_velocity(int id, const real_t &threshold, const real_t &dtime);

		BSPTree();
		~BSPTree(){}

	protected:
		

		struct AAPlane
		{
			int axis;
			real_t coord;
		};

		struct Node
		{
			Node *ch[2];
			int dep;

			Node() { ch[0] = ch[1] = NULL; }
			bool is_leaf() const { return ch[0] == NULL && ch[1] == NULL; }
			inline real_t volume() const { return box.volume(); }

			AAPlane plane;

			AABox box;
			int id, n;

			Vector cm;
			real_t mass;
		};

		void release(Node *root);
		AABox constructAABox(int *ids, int n);

		Node *root;

		int m_nbody;
		int m_dep_max;
		Body *body;

		struct CmpBody
		{
			Body *body;
			int cid;
			CmpBody(Body *body, int cid) : 
				body(body), cid(cid) {}
			bool operator () (int i, int j)
			{
				return body[i].pos[cid] < body[j].pos[cid];
			}
		};

		AABox boundary;
		int find_middle(int *ids, int n, int cid); // return index start from 1

		Node *build_tree(int *ids, int nbody, int dep);

		Collision do_col_dtct(Node *root, int id, real_t dtime);
		Collision body_clsdtct(int id0, int id1, real_t dtime);

		Vector calc_acceleration(Node *root, int id, real_t threshold);

		// the acceleration to a caused by b
		static Vector calc_single_acc(const Body &a, const Node *b);

		void check();
};

#endif // __BSP_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


/*
 * $File: naive.cc
 * $Date: Wed Aug 29 11:01:40 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include "naive.hh"



void NaiveEngine::init(const NBodyConfig &conf)
{
	this->conf = conf;
	boundary.min_coord = conf.domain.min_coord;
	boundary.max_coord = conf.domain.max_coord;

	body = conf.body;
}

Collision NaiveEngine::collision_detect(int id, real_t dtime)
{
	Vector pos = body[id].pos + body[id].v * dtime;
	real_t time = REAL_MAX;
	int axis, mag;
	Collision col;
	col.id0 = id;
	col.time = REAL_MAX;
	if (boundary.collide_info(body[id], time, axis, mag))
		if (time < dtime)
		{
			col.id1 = -1;
			col.time = time;
			col.axis = axis;
			col.mag = mag;
		}

	for (int i = 0; i < conf.nbody; i ++)
		if (i != id)
		{
			Collision tcol = body_clsdtct(body[id], body[i], dtime);
			if (tcol.time < dtime && tcol.time > 0 && tcol.time < col.time)
				col = tcol;
		}
	if (col.time > dtime)
	{
		col.time = -1;
		col.id1 = -1;
	}
	return col;
}

void NaiveEngine::cal_new_velocity(int id, const real_t &, const real_t &dtime)
{
	Vector acc(0,0,0);
	for (int i = 0; i < conf.nbody; i ++)
		if (i != id)
			acc += calc_acc(body[id].pos, body[i].pos, body[i].mass);
	body[id].v += acc * dtime;
}

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


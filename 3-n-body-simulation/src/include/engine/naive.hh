/*
 * $File: naive.hh
 * $Date: Wed Aug 29 10:30:48 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */


#ifndef __ENGINE_NAIVE_HH__
#define __ENGINE_NAIVE_HH__

#include "engine.hh"
#include "AABox.hh"

class NaiveEngine : public Engine
{
	public:
		virtual void init(const NBodyConfig &conf);
		virtual Collision collision_detect(int id, real_t dtime);

		// let theta be the quotient between the volume represented
		// by a node and the distance cubed between CM and the point.
		// if the quotient lies bellow threshold, the approximation
		// will be used
		virtual void cal_new_velocity(int id, const real_t &threshold, const real_t &dtime);

	protected:
		NBodyConfig conf;
		AABox boundary;
		Body *body;
};

#endif // __ENGINE_NAIVE_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


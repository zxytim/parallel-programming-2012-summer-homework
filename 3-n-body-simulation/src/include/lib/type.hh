/*
 * $File: type.hh
 * $Date: Tue Aug 28 12:53:48 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __TYPE_HH__
#define __TYPE_HH__


#include <limits>

#ifdef REAL_T_LONG_DOUBLE
typedef long double real_t;
#define REAL_T_FMT "Lf"
#else
typedef double real_t;
#define REAL_T_FMT "lf"
#endif

const real_t REAL_MIN = -std::numeric_limits<real_t>::max(),
	  REAL_MAX = std::numeric_limits<real_t>::max();

struct Complex
{
	real_t real, imag;
	Complex() {}
	Complex(real_t real, real_t imag) :
		real(real), imag(imag) {}
};

#endif // __TYPE_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


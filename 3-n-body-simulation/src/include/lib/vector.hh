/*
 * $File: vector.hh
 * $Date: Tue Aug 28 12:47:25 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#ifndef __LIB_VECTOR__
#define __LIB_VECTOR__


#include <cmath>

#include "lib/sassert.hh"
#include "lib/type.hh"


#if 0
class Vector
{
	public:
		real_t x, y;
		
		Vector(){}
		Vector(real_t x, real_t y)
			: x(x), y(y) {}

		Vector operator + (const Vector &vec) const
		{ return Vector(x + vec.x, y + vec.y); }

		Vector operator - (const Vector &vec) const
		{ return Vector(x - vec.x, y - vec.y); }

		Vector operator * (const real_t factor) const
		{ return Vector(x * factor, y * factor); }

		Vector operator / (const real_t factor) const
		{ return Vector(x / factor, y / factor); }

		Vector & operator += (const Vector &v) 
		{ x += v.x, y += v.y; return *this; }

		Vector & operator -= (const Vector &v) 
		{ x -= v.x, y -= v.y; return *this; }

		Vector & operator *= (const real_t factor)
		{ x *= factor, y *= factor; return *this; }

		Vector & operator /= (const real_t factor)
		{ x /= factor, y /= factor; return *this; }

		real_t dot(const Vector &v) const
		{ return x * v.x + y * v.y; }

		real_t cross(const Vector &v) const
		{ return x * v.y - y * v.x; }

		real_t length() const
		{ return sqrt(x * x + y * y); }

		real_t lengthsqr() const
		{ return x * x + y * y; }
};
#endif 

/**
 * 3D vector
 */
class Vector
{
	public:
		real_t x, y, z;
		
		Vector(){}
		Vector(real_t x, real_t y, real_t z)
			: x(x), y(y), z(z) {}

		Vector operator + (const Vector &vec) const
		{ return Vector(x + vec.x, y + vec.y, z + vec.z); }

		Vector operator - (const Vector &vec) const
		{ return Vector(x - vec.x, y - vec.y, z - vec.z); }

		Vector operator * (const real_t factor) const
		{ return Vector(x * factor, y * factor, z * factor); }

		Vector operator / (const real_t factor) const
		{ return Vector(x / factor, y / factor, z / factor); }

		Vector & operator += (const Vector &v) 
		{ x += v.x, y += v.y, z += v.z; return *this; }

		Vector & operator -= (const Vector &v) 
		{ x -= v.x, y -= v.y, z -= v.z;  return *this; }

		Vector & operator *= (const real_t factor)
		{ x *= factor, y *= factor, z *= factor; return *this; }

		Vector & operator /= (const real_t factor)
		{ x /= factor, y /= factor, z /= factor; return *this; }

		inline real_t dot(const Vector &v) const
		{ return x * v.x + y * v.y + z * v.z; }

		Vector cross(const Vector &v) const
		{ return Vector(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x); }

		real_t length() const
		{ return sqrt(x * x + y * y + z * z); }

		real_t lengthsqr() const
		{ return x * x + y * y + z * z; }

		Vector operator - () const
		{ return (*this) * (-1); }

		Vector unit() const
		{ return (*this) / length(); }

		Vector &unitize()
		{ return (*this) /= length(); }

		Vector &tolength(real_t len)
		{
			return *this = unit() * len;
			//return unitize() *= len;
		}

		real_t &coord(int axis)
		{
			if (axis == 0)
				return x;
			if (axis == 1)
				return y;
			return z;
		}
		const real_t &coord(int axis) const
		{
			if (axis == 0)
				return x;
			if (axis == 1)
				return y;
			return z;
		}
		real_t &operator [] (int axis)
		{
			if (axis == 0)
				return x;
			if (axis == 1)
				return y;
			return z;
		}
		const real_t &operator [] (int axis) const
		{
			if (axis == 0)
				return x;
			if (axis == 1)
				return y;
			return z;
		}
};

Vector operator * (const real_t factor, const Vector &vec);
Vector operator / (const real_t factor, const Vector &vec);

#endif // __PROTOTYPE_HEADER__

/*
 * $File: body.hh
 * $Date: Thu Aug 30 12:54:00 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __NBODY_BODY_HH__
#define __NBODY_BODY_HH__

#include "lib/vector.hh"
#include "lib/color.hh"
#include "collision.hh"

class Body
{
	public:
		ColorRGB color;

		int id;

		Vector pos, v;
		real_t mass;
		real_t radius;

		bool intersect(const Body &b) const;
		void set_pos(const Vector &pos) { this->pos = pos; }
		void move(const Vector &dsplcmt) { pos += dsplcmt;}
		void advance(const real_t &dtime) { pos += v * dtime; }
		real_t boundary(int cid, int type) // min or max
		{
			real_t t;
			if (cid == 0)
				t = pos.x;
			else if (cid == 1)
				t = pos.y;
			else t = pos.z;
			return t + (type == 0 ? -1 : 1) * radius;
		}
};

bool body_collide(Body &a, Body &b, real_t dtime, bool modify_b = false);

Collision body_clsdtct(Body &id0, Body &id1, real_t dtime);
Vector calc_acc(const Vector &p0, const Vector &p1, real_t m1);

#endif // __NBODY_BODY_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


/*
 * $File: const.hh
 * $Date: Tue Aug 28 21:10:46 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __NBODY_CONST_HH__
#define __NBODY_CONST_HH__

#include "lib/type.hh"

class Const
{
	public:
		static const real_t G = 6.6738480 * 1e1;
};

#endif // __NBODY_CONST_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


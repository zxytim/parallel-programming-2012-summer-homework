/*
 * $File: nbody.hh
 * $Date: Thu Aug 30 11:25:37 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __NBODY_NBODY_HH__
#define __NBODY_NBODY_HH__

#include "lib/type.hh"
#include "nbody/body.hh"


struct NBodyDomain
{
	Vector min_coord,
		   max_coord;
	
	NBodyDomain() {}
	NBodyDomain(const Vector &min, const Vector &max) :
		min_coord(min), max_coord(max) {}
	real_t length(int axis) const
	{ return max_coord[axis] - min_coord[axis]; }
};

struct NBodyConfig
{
	Body *body;
	int nbody;

	NBodyDomain domain;

	void init(const NBodyDomain &domain, int n);
	void init_from_file(const char *fname); // '-' for stdin
	
	NBodyConfig();
	~NBodyConfig();
};

#endif // __NBODY_NBODY_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


/*
 * $File: utils.hh
 * $Date: Mon Aug 27 10:41:12 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include "lib/exception.hh"
#include "lib/log.hh"
#include "lib/misc.hh"
#include "lib/timer.hh"
#include "lib/type.hh"
#include "lib/sassert.hh"


/**
 * vim: syntax=cpp11 foldmethod=marker
 */


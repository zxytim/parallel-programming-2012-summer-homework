/*
 * $File: worker.hh
 * $Date: Thu Aug 30 18:57:13 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __WORKER_HH__
#define __WORKER_HH__

#include <string>

#include "nbody/nbody.hh"
#include "engine.hh"

class Worker
{
	protected:
		int nworker;
		Engine *engine;
	public:
		virtual void init(int argc, char *argv[]) {}
		virtual std::string name() const = 0;
		virtual int get_nworker() const { return nworker; }
		virtual int advance(const NBodyConfig &conf, real_t dtime) = 0;
		void set_nworker(int nworker) { this->nworker = nworker; }
		void set_engine(Engine *engine) { this->engine = engine; }

		virtual ~Worker() {}
};
#endif // __WORKER_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


/*
 * $File: misc.cc
 * $Date: Tue Aug 28 10:49:06 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include "lib/misc.hh"
#include <cstdlib>

int Rand(int max)
{ return rand() / (RAND_MAX + 1.0) * max; }
int RandRange(int min, int max)
{ return min + Rand(max - min + 1); }

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


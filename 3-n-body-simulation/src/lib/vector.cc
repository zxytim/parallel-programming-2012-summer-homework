/*
 * $File: vector.cc
 * $Date: Mon Aug 27 21:57:16 2012 +0800
 * $Author: Xinyu Zhou <zxytim@gmail.com>
 */

#include "lib/vector.hh"


#if 0
Vector operator * (const real_t factor, const Vector &vec) 
{ return Vector(vec.x * factor, vec.y * factor, vec.z * factor); }

Vector operator / (const real_t factor, const Vector &vec)
{ return Vector(vec.x / factor, vec.y / factor, vec.z / factor); }
#endif

/*
 * $File: main.cc
 * $Date: Fri Aug 31 10:10:41 2012 +0800
 */

#include <gtk/gtk.h>

#include <getopt.h>
#include <unistd.h>

#include <string>
#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <cstring>

#include "nbody/nbody.hh"
#include "nbody/body.hh"
#include "worker/openmp.hh"
#include "worker/pthread.hh"
#include "worker/mpi.hh"
#include "utils.hh"
#include "lib/timer.hh"
#include "lib/log.hh"
#include "engine/bsp.hh"
#include "engine/naive.hh"


#define error_exit(exit_code, fmt, ...) \
	do { \
		static const int MSG_LEN_MAX = 1000; \
		static char msg[MSG_LEN_MAX + 1]; \
		sprintf(msg, "error %d: " fmt, exit_code, ##__VA_ARGS__); \
		fprintf(stderr, "%s\n", msg); \
		exit(exit_code); \
	} while (0)

const real_t FPS = 1000;
const char *progname;
bool show_window = true;


struct Camera
{
	Vector sight_pos;
	Vector view_dir, up_dir;
	real_t frustum_dist;
	real_t fwidth, fheight; // configuration of the frustum
};

struct RenderConfig
{
	NBodyConfig nbody;

	Worker *worker;
	Camera camera;

	int wwidth, wheight; // window width and height

	int width() const { return wwidth; }
	int height() const { return wheight; }

	void set_worker(Worker *worker)
	{ this->worker = worker; }
	
	bool advance(real_t dtime)
	{
		if (fabs(dtime) < EPS)
			return true;
		//log_printf("%" REAL_T_FMT "s", dtime);
		return worker->advance(nbody, dtime);
#if 0
		for (int i = 0; i < nbody.nbody; i ++)
		{
			Body &b = nbody.body[i];
			log_printf("id %2d: (%Lf,%Lf,%Lf),(%Lf,%Lf,%Lf)", i,
					b.pos.x, b.pos.y, b.pos.z,
					b.v.x, b.v.y, b.v.z);
		}
#endif
	}

	void init() {}

} rconf;
Timer timer;

long long frame_count;
real_t runtime = 1e100;
Timer gtimer;

FpsCounter fps;

bool rendered = false;
void exit_on_timeout()
{
	if (rendered)
	{
		real_t rtime = gtimer.end() / 1000.0;
		printf("worker: %s\n", rconf.worker->name().c_str());
		printf("nworker: %d\n", rconf.worker->get_nworker());
		printf("running time: %Lf\n", rtime);
		printf("total frames: %lld\n", frame_count);
		printf("average fps: %Lf\n", frame_count / rtime);
	}
	delete rconf.worker;

	exit(0);
}

#ifdef USE_GTK

bool cmpZ(const Body &a, const Body &b)
{
	return a.pos.z + a.radius > b.pos.z + b.radius;
}
static gboolean delete_event(
		GtkWidget *,
		GdkEvent *,
		gpointer )
{
	return FALSE;
}

static void destroy(GtkWidget *, gpointer )
{
	gtk_main_quit();
}

static gboolean cb_timeout(GtkWidget *widget)
{
	if (widget->window == NULL)
		return FALSE;

	gtk_widget_queue_draw_area(widget, 0, 0, widget->allocation.width, widget->allocation.height);
	return TRUE;
}

static gboolean da_expose_callback(
		GtkWidget *widget, 
		GdkEventExpose *, 
		gpointer )
{
	if (timer.end() > EPS)
	{
		fps.count();
		frame_count ++;
		Timer tengine;
		tengine.begin();
		rconf.advance(timer.end() / 1000.0);
		//log_printf("engine time: %llums\n", tengine.end());
		timer.begin();
		if (gtimer.end() > runtime * 1000)
			exit_on_timeout();
	}

	if (!show_window)
		return FALSE;
	//return FALSE;
	cairo_t *cr = gdk_cairo_create(widget->window);

	// clear scene
	cairo_save (cr);
	cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
	cairo_set_operator (cr, CAIRO_OPERATOR_CLEAR);
	cairo_paint (cr);
	cairo_restore (cr);

	NBodyConfig &nbody = rconf.nbody;
	Body *body = new Body[nbody.nbody];
	memcpy(body, nbody.body, sizeof(Body) * nbody.nbody);

	std::sort(body, body + nbody.nbody, cmpZ);
	Camera &camera = rconf.camera;


	/*
	Vector dir = camera.view_dir * camera.frustum_dist,
		   forigin = camera.sight_pos + dist,
		   right_dir = dist.unit().cross(camera.up_dir);
	   */

	NBodyDomain &domain = rconf.nbody.domain;
	for (int i = 0; i < nbody.nbody; i ++)
	{
		Body &b = body[i];
		int x = b.pos.x / domain.length(0) * rconf.width(),
			y = b.pos.y / domain.length(1) * rconf.height();

		cairo_save(cr);

		cairo_new_path(cr);
		cairo_arc(cr, x, y, b.radius, 0, 2 * M_PI);
		cairo_close_path(cr);
		
		ColorRGB &col = body[i].color;

		cairo_set_source_rgb(cr, col.red, col.green, col.blue);
		cairo_fill_preserve(cr);
		cairo_stroke(cr);

		cairo_restore(cr);
	}


	delete [] body;
	cairo_set_source_rgb(cr, 0.0, 0.5, 0.5);;
	cairo_select_font_face(cr, "Purisa",
			CAIRO_FONT_SLANT_NORMAL,
			CAIRO_FONT_WEIGHT_BOLD);
	cairo_set_font_size(cr, 13);
	cairo_move_to(cr, 20, 30);

	char sfps[20];
	snprintf(sfps, 20, "FPS: %.2lf", fps.fps());

	cairo_show_text(cr, sfps); 

	cairo_destroy(cr);
	return FALSE;
}

void show_init(int argc, char *argv[])
{
	gtk_init(&argc, &argv);
}

void show()
{
	GtkWidget *window;
	int border_width = 0;

	int window_width = rconf.width() + border_width * 2,
		window_height = rconf.height() + border_width * 2;
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_container_set_border_width(GTK_CONTAINER(window), border_width);
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size(GTK_WINDOW(window), window_width, window_height);

	//gtk_widget_set_app_paintable(window, TRUE);
	gtk_widget_set_size_request(window, window_width, window_height);

	GtkWidget *da = gtk_drawing_area_new();
	gtk_widget_set_size_request(da, rconf.width(), rconf.height());

	g_signal_connect(window, "delete-event",
			G_CALLBACK(delete_event), NULL);
	g_signal_connect(window, "destroy",
			G_CALLBACK(destroy), NULL);

	/*
	gtk_widget_add_events(da, GDK_BUTTON_PRESS_MASK);
	g_signal_connect(da, "button-press-event", 
			G_CALLBACK(cb_clicked), NULL);
	*/

	g_signal_connect(da, "expose_event",
			G_CALLBACK(da_expose_callback), NULL);

	g_timeout_add(1000 / FPS, (GSourceFunc)cb_timeout, da);

	gtk_container_add(GTK_CONTAINER(window), da);
	gtk_widget_show_all(window);

	timer.begin();
	gtk_main();
}


#endif




Worker *worker_factory(const char *name)
{
	std::string w = name;
#ifdef ENABLE_OPENMP
	if (w == "openmp")
		return new WorkerOpenMP();
#endif
#ifdef ENABLE_PTHREAD
	if (w == "pthread")
		return new WorkerPthread();
#endif
#ifdef ENABLE_MPI
	if (w == "mpi")
		return new WorkerMPI();
#endif
	fprintf(stderr, "Unkown worker: %s\n", name);
	exit(-1);
}


int str2num(const std::string &str)
{
	int ret = 0, sign = 1, start = 0;
	if (str[0] == '-')
		sign = -1, start = 1;
	for (size_t i = start; i < str.length(); i ++)
		ret = ret * 10 + str[i] - '0';
	return ret * sign;
}

real_t str2real(const std::string &str)
{
	real_t ret;
	sscanf(str.c_str(), "%" REAL_T_FMT, &ret);
	return ret;
}

int main(int argc, char *argv[])
{
	srand(2);
	progname = argv[0];

	int nworker = sysconf(_SC_NPROCESSORS_ONLN);
	option long_options[] = {
		{"nworker",		required_argument,	NULL, 'n'},
		{"size",		required_argument,	NULL, 's'},
		{"silent",		no_argument,		NULL, 'w'},
		{"parallel",	required_argument,	NULL, 'p'},
		{"input",		required_argument,	NULL, 'i'},
		{"nstar",		required_argument,	NULL, 'r'},
		{"runtime",		required_argument,	NULL, 't'},
		{"fps-test",	no_argument,		NULL, 'f'},
		{"interval",	required_argument,	NULL, 'l'},
	};

	int width = 600, height = 600;
	std::string para = "openmp", input = "";
	int nstar = 20;
	int opt;
	real_t time_interval = 0.01;
	bool fpstest = false;
	while ((opt = getopt_long(argc, argv, "l:fr:t:n:s:wp:i:", long_options, NULL)) != -1)
	{
		switch (opt)
		{
			case 'l':
				time_interval = str2real(optarg);
				break;
			case 'f':
				fpstest = true;
				break;
			case 't':
				runtime = str2real(optarg);
				break;
			case 'r':
				nstar = str2num(optarg);
				break;
			case 'n':
				nworker = str2num(optarg);
				break;

			case 's':
				{
					int w, h;
					if (sscanf(optarg, "%dx%d", &w, &h) != 2)
						error_exit(-1, "invalid size `%s'", optarg);
					width = w, height = h;
				}
				break;

			case 'w':
				show_window = false;
				break;

			case 'p':
				para = optarg;
				break;

			case 'i':
				input = optarg;
				break;
		}
	}

#ifdef USE_GTK
	show_init(argc, argv);
#endif

	NBodyDomain domain(Vector(0, 0, 0), Vector(600, 600, 100));
	rconf.nbody.init(domain, nstar);
	if (input.length())
		rconf.nbody.init_from_file(input.c_str());

	rconf.nbody.domain = domain;
	rconf.wwidth = width, rconf.wheight = height;

	Worker *worker = worker_factory(para.c_str());
	worker->init(argc, argv);
	worker->set_nworker(nworker);
	worker->set_engine(new BSPTree());
	rconf.set_worker(worker);
	rconf.init();
	gtimer.begin();
	if (fpstest)
	{
		while (true)
		{
			if (gtimer.end() > runtime * 1000)
				break;
			frame_count ++;
			if (!rconf.advance(time_interval))
				break;
			rendered = true;
		}
	}
#ifdef USE_GTK
	else if (rconf.advance(0.01))
		show();
#endif
	exit_on_timeout();
	return 0;
}


/**
 * vim: syntax=cpp11 foldmethod=marker
 */


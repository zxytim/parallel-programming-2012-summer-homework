/*
 * $File: body.cc
 * $Date: Thu Aug 30 12:53:24 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */


#include "nbody/body.hh"

#include "lib/sassert.hh"
#include "lib/misc.hh"
#include "lib/log.hh"

#include "nbody/const.hh"

static real_t sqr(real_t x) { return x * x; }
bool body_collide(Body &a, Body &b_, real_t dtime, bool modify_b)
{
//	log_printf("col: (%d,%d), %Lf", a.id, b.id, dtime);
	a.advance(dtime);
	Body *b, btmp;
	if (modify_b)
		b = &b_;
	else b = &btmp, btmp = b_;

	b->advance(dtime);

	real_t dist = (a.pos - b->pos).lengthsqr(),
		   rsum = sqr(a.radius + b->radius);

	if (dist <= rsum)
	{
		real_t delta = (rsum - dist) / 2 * 1.1 + EPS;
		Vector dir01 = b->pos - a.pos;
		a.set_pos(a.pos - dir01 * delta);
		b->set_pos(b->pos + dir01 * delta);
	}
	//assert(dist > rsum);
	assert(!a.intersect(*b));

	real_t m1, m2, x1, x2;
	Vector v1, v2, v1x, v2x, v1y, v2y, x(a.pos - b->pos);


	x.unitize();
	v1 = a.v;
	x1 = x.dot(v1);
	v1x = x * x1;
	v1y = v1 - v1x;
	m1 = a.mass;

	x = x * -1;
	v2 = b->v;
	x2 = x.dot(v2);
	v2x = x * x2;
	v2y = v2 - v2x;
	m2 = b->mass;

	real_t s = m1 + m2;
	a.v = v1x * (m1 - m2) / s + v2x * (2 * m2) / s + v1y;
	b->v = v1x * (2 * m1) / s + v2x * (m2 - m1) / s + v2y;

	return true;
}

bool Body::intersect(const Body &b) const
{
	real_t distsqr = (pos - b.pos).lengthsqr(),
		   rsumsqr = sqr(radius + b.radius);
	return distsqr < rsumsqr;
}
static bool solve_quadratic_equation(const real_t &a, const real_t &b, const real_t &c,
		real_t &root0, real_t &root1)
{
	if (fabs(a) < EPS)
		return false;
	real_t delta = b * b - 4 * a * c;
	if (delta >= 0)
	{
		real_t sq = sqrt(delta),
			   d = 1 / (2 * a);
		root0 = (-b - sq) * d;
		root1 = (-b + sq) * d;
		return true;
	}
	return false;
}

Collision body_clsdtct(Body &b0, Body &b1, real_t dtime)
{
	int id0 = b0.id,
		id1 = b1.id;
	if (id0 == id1)
		return Collision(-1, -1, -1);
	Vector AB = b1.pos - b0.pos,
		   vab = b1.v - b0.v;
	real_t rab = (b0.radius + b1.radius);
	real_t a = vab.dot(vab),
		   b = 2 * vab.dot(AB),
		   c = AB.dot(AB) - rab * rab;

	real_t t0, t1, t;
	if (!solve_quadratic_equation(a, b, c, t0, t1))
		return Collision(-1, -1, -1);
	t = t0;
	if (t < 0) t = t1;
	if (t < 0 || t > dtime) return Collision(-1, -1, -1);
	if (t > EPS)
		t -= EPS / 2;
	if (t > dtime)
		t = -1;
	return Collision(id0, id1, t);
}

Vector calc_acc(const Vector &p0, const Vector &p1, real_t m1)
{
	Vector dir = p1 - p0;
	real_t dist_sqr = dir.lengthsqr(),
		   magnitude = Const::G * m1 / dist_sqr;

	return dir / sqrt(dist_sqr) * magnitude;
}

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


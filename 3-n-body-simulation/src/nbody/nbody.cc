/*
 * $File: nbody.cc
 * $Date: Thu Aug 30 17:30:42 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include <vector>

#include <cstdlib>
#include <cstring>

#include "nbody/body.hh"
#include "nbody/nbody.hh"

static real_t RandReal(real_t max)
{
	return max * (rand() / (RAND_MAX + 1.0));
}
static real_t RandRealRange(real_t lo, real_t hi)
{
	return lo + RandReal(hi - lo);
}

static real_t sqr(real_t x)
{
	return x * x;
}

static ColorRGB random_color()
{
	return ColorRGB(RandReal(1), RandReal(1), RandReal(1));
}

void NBodyConfig::init(const NBodyDomain &domain, int n)
{
	Vector center = (domain.min_coord + domain.max_coord) / 2;

	nbody = n, this->domain = domain;
	body = new Body[n];

#if 0
	for (int i = 0; i < n; i ++)
	{
		body[i].id = i;
		do
		{
			static const real_t MASS_MAX = 1000;
#if 0
			body[i].pos = Vector(RandReal(domain.length(0)), RandReal(domain.length(1)), RandReal(domain.length(2)));
			body[i].v = Vector(RandRealRange(-20, 20), RandRealRange(-20, 20), RandRealRange(-20, 20));
#else
			body[i].pos = Vector(RandReal(domain.length(0)), RandReal(domain.length(1)), 0);
			body[i].v = Vector(RandRealRange(-20, 20), RandRealRange(-20, 20), 0);
#endif
			body[i].v = Vector(0, 0, 0);
			body[i].mass = RandReal(MASS_MAX);
			body[i].radius = pow(body[i].mass, 1 / 3.0);
			body[i].color = random_color();

			Body &b = body[i];

			bool collide = false;
			for (int j = 0; !collide && j < i - 1; j ++)
				if ((body[j].pos - b.pos).lengthsqr() < sqr(body[j].radius + b.radius))
					collide = true;
			if (collide == false)
				break;
		} while (true);
	}
#endif

#if 1
	body[0].id = 0;
	body[0].pos = center;
	body[0].v = Vector(0, 0, 0);
//	body[0].v = Vector(30, 0, 0);
	body[0].mass = 100000;
	body[0].radius = 20; //pow(body[0].mass, 1 / 3.0);
	body[0].color = random_color();

	real_t R = 200, V = 150;
	int start = 1;
	for (int i = start; i < n; i ++)
	{
		real_t angle = 2 * M_PI / (n - (start)) * i;
		body[i].pos = center + Vector(
				R * cos(angle), 
				R * sin(angle),
				0);
		body[i].v = Vector(
				V * cos(angle + M_PI / 2), 
				V * sin(angle + M_PI / 2),
				0);
		//body[i].v = Vector(0, 0, 0);
		body[i].mass = 50;
		body[i].radius = pow(body[i].mass, 1 / 3.0);
		body[i].color = random_color();
		body[i].id = i;
	}
#endif
}

NBodyConfig::NBodyConfig()
{
	body = NULL;
}

NBodyConfig::~NBodyConfig()
{
	if (body)
		delete [] body;
}

void NBodyConfig::init_from_file(const char *fname)
{
	FILE *fptr;
	if (!strcmp(fname, "-"))
		fptr = stdin;
	else fptr= fopen(fname, "r");

	std::vector<Body> bvec;
	// center 3
	// v 3
	// mass radius 2
	Body b;
	nbody = 0;
	while (fscanf(fptr, "%" REAL_T_FMT "%" REAL_T_FMT "%" REAL_T_FMT "%" REAL_T_FMT "%" REAL_T_FMT "%" REAL_T_FMT "%" REAL_T_FMT "%" REAL_T_FMT , &b.pos.x, &b.pos.y, &b.pos.z, &b.v.x, &b.v.y, &b.v.z, &b.mass, &b.radius) == 8)
	{
		b.color = random_color();
		b.id = nbody;
		bvec.push_back(b);
		nbody ++;
	}
	body = new Body[nbody];
	for (int i = 0; i < bvec.size(); i ++)
		body[i] = bvec[i];

	if (fptr != stdin)
		fclose(fptr);
}


/**
 * vim: syntax=cpp11 foldmethod=marker
 */


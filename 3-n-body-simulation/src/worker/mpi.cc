/*
 * $File: mpi.cc
 * $Date: Thu Aug 30 19:00:30 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifdef ENABLE_MPI

#include "mpi.hh"

#include <mpi.h>

const int ROOT_PROC = 0;

#ifdef DEBUG
#define CHECK_NAN \
	do { \
		for (int i = 0; i < nbconf.nbody; i ++) \
		{ \
			assert(!isnan(body[i].pos.x)); \
			assert(!isnan(body[i].pos.y)); \
			assert(!isnan(body[i].pos.z)); \
		} \
	} while (0)
#else
#define CHECK_NAN
#endif

WorkerMPI::WorkerMPI()
{
}

void WorkerMPI::init(int argc, char *argv[])
{
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &nproc);
	nworker = nproc;
	MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);

	MPI_Type_contiguous(sizeof(TaskConf), MPI_CHAR, &MPI_TYPE_CONF);
	MPI_Type_commit(&MPI_TYPE_CONF);

	MPI_Type_contiguous(sizeof(Body), MPI_CHAR, &MPI_TYPE_BODY);
	MPI_Type_commit(&MPI_TYPE_BODY);

	body = NULL;
}

int WorkerMPI::get_task_per_proc(int ntask)
{
	int task_per_proc = ntask / nproc,
		task_remain = ntask - task_per_proc * nproc;

	if (task_remain)
		task_per_proc ++;
	return task_per_proc;
}

int WorkerMPI::decompose_task(int ntask, int &left, int &right)
{
	if (ntask < nproc)
	{
		if (proc_id < ntask)
		{
			left = proc_id;
			right = proc_id + 1;
			return 1;
		}
		else
			return 0;
	}
	
	int task_per_proc = get_task_per_proc(ntask);

	left = task_per_proc * proc_id;
	right = task_per_proc * (proc_id + 1);
	if (right > ntask)
		right = ntask;
	return right - left;
}

void WorkerMPI::work_velocity(real_t dtime)
{
	//SCOPED_LOGGER("");
	ntask_assigned = decompose_task(nbody, task_start, task_end);
	real_t threshold = 0.5;
	for (int i = task_start; i < task_end; i ++)
		engine->cal_new_velocity(i, threshold, dtime); // dump
}

void WorkerMPI::work_collision(real_t dtime)
{
	Body *new_body = new Body[task_end - task_start];
	memcpy(new_body, body + task_start, sizeof(Body) * (task_end - task_start));
	for (int i = task_start; i < task_end; i ++)
	{
		int p = i - task_start;
		Collision col = engine->collision_detect(i, dtime);
		if (col.id1 != -1)
		{
			if (body_collide(new_body[p], body[col.id1], col.time))
				new_body[p].advance(dtime - col.time);
			else new_body[p].advance(dtime);
		}
		else if (col.time > 0)
		{
			Body &b = new_body[p];
			b.advance(col.time);
			b.v[col.axis] *= -1;
			b.advance(dtime - col.time);
		}
		else new_body[p].advance(dtime);
	}
	CHECK_NAN;
	memcpy(body + task_start, new_body, sizeof(Body) * (task_end - task_start));
	CHECK_NAN;
	delete [] new_body;
}

void WorkerMPI::gather_body()
{
	//SCOPED_LOGGER("");
	int task_per_proc = get_task_per_proc(nbody);
	MPI_Allgather(body + task_start, task_per_proc, MPI_TYPE_BODY, 
			body, task_per_proc, MPI_TYPE_BODY,
			MPI_COMM_WORLD);
}

void WorkerMPI::all_advance(real_t dtime)
{
	for (int i = 0; i < nbody; i ++)
		body[i].advance(dtime);
}

int WorkerMPI::advance(const NBodyConfig &conf, real_t dtime)
{
	if (proc_id == ROOT_PROC)
	{
		if (body == NULL)
		{
			int real_nbody = get_task_per_proc(conf.nbody) * nproc;
			body = new Body[real_nbody];
		}
		memcpy(body, conf.body, sizeof(Body) * conf.nbody);

		nbody = conf.nbody;


		TaskConf tconf;
		tconf.type = CONF_TYPE_NEW_CONF;
		tconf.nbody = conf.nbody;
		tconf.dtime = dtime;

		nbconf = conf;
		nbconf.body = body;

		CHECK_NAN;
		mst_bcast_conf(tconf, nbconf);
		engine->init(nbconf);

		work_velocity(tconf.dtime);
		gather_body();

#if 1
		CHECK_NAN;
		work_collision(tconf.dtime);
		CHECK_NAN;
		gather_body();
#endif

		memcpy(conf.body, body, sizeof(Body) * nbody);

		return true;
	}
	else
	{
		TaskConf tconf;

		// velocity calculation
		for (; slv_recv_conf(tconf, nbconf); )
		{
			engine->init(nbconf);
			work_velocity(tconf.dtime);

			gather_body();
#if 1
			work_collision(tconf.dtime);
			gather_body();
#endif
		}
		return false;
	}
}

bool WorkerMPI::mst_bcast_conf(TaskConf conf, const NBodyConfig &nbconf)
{
	//SCOPED_LOGGER("");
	//printf("!!!nbody: %d\n", conf.nbody);
	MPI_Bcast(&conf, 1, MPI_TYPE_CONF, ROOT_PROC, MPI_COMM_WORLD);
	if (conf.type == CONF_TYPE_EXIT)
		return true;

	MPI_Bcast(body, nbconf.nbody, MPI_TYPE_BODY, ROOT_PROC, MPI_COMM_WORLD);

	return true;
}


/*
 * slave
 */
bool WorkerMPI::slv_recv_conf(TaskConf &conf, NBodyConfig &nbconf)
{
	//SCOPED_LOGGER("");
	MPI_Bcast(&conf, 1, MPI_TYPE_CONF, ROOT_PROC, MPI_COMM_WORLD);
	if (conf.type == CONF_TYPE_EXIT)
		return false;

	nbconf.nbody = conf.nbody;
	nbody = nbconf.nbody;

	if (body == NULL)
	{
		body = new Body[get_task_per_proc(conf.nbody) * nproc];
		nbconf.body = body;
	}
	MPI_Bcast(body, nbconf.nbody, MPI_TYPE_BODY, ROOT_PROC, MPI_COMM_WORLD);
	return true;
}

WorkerMPI::~WorkerMPI()
{
	if (proc_id == ROOT_PROC)
	{
		TaskConf conf;
		conf.type = CONF_TYPE_EXIT;
		mst_bcast_conf(conf, nbconf);
	}
	MPI_Finalize();
}

#endif
/**
 * vim: syntax=cpp11 foldmethod=marker
 */


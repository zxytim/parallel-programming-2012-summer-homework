/*
 * $File: mpi.hh
 * $Date: Thu Aug 30 18:48:34 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __WORKER_MPI_HH__
#define __WORKER_MPI_HH__

#ifdef ENABLE_MPI

#include "worker.hh"

#include <mpi.h>

const int CONF_TYPE_NEW_CONF = 1,
	  CONF_TYPE_EXIT = 0;

struct TaskConf
{
	int type, nbody;
	real_t dtime;
};


enum MPITaskType
{
	MPI_TT_VELOCITY,
	MPI_TT_POSITION,
	MPI_TT_TASK_END
};

struct MPITask
{
	MPITaskType type;
	int left, right;
};


class WorkerMPI : public Worker
{
	public:
		virtual void init(int argc, char *argv[]);
		virtual int advance(const NBodyConfig &conf, real_t dtime);
		virtual std::string name() const { return "MPI"; }
		virtual int get_nworker() const { return nproc; }

		WorkerMPI();
		~WorkerMPI();
	protected:
		MPI_Datatype MPI_TYPE_CONF,
					 MPI_TYPE_BODY;

		NBodyConfig nbconf;
		int nproc, proc_id;
		int ntask_assigned;

		int task_start, task_end;
		int decompose_task(int ntask, int &left, int &right);
		int get_task_per_proc(int ntask);
		void work_velocity(real_t dtime);
		void work_collision(real_t dtime);
		void all_advance(real_t dtime);
		void gather_body();

		/*
		 * master
		 */
		bool mst_bcast_conf(TaskConf conf, const NBodyConfig &nbconf);

		Body *body;
		int nbody;
		/*
		 * slave
		 */
		bool slv_recv_conf(TaskConf &conf, NBodyConfig &nbconf);
		bool slv_recv_task(MPITask &task);
};

#endif
#endif // __WORKER_MPI_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


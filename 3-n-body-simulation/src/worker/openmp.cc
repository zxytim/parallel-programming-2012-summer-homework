/*
 * $File: openmp.cc
 * $Date: Fri Aug 31 10:09:47 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifdef ENABLE_OPENMP
#include "openmp.hh"

#include <cstring>

int WorkerOpenMP::advance(const NBodyConfig &conf, real_t dtime)
{
	int ncollide = 0;
	real_t threshold = 0.85;

#if 1
	real_t time_remain = dtime;
	while (time_remain)
	{
		assert(time_remain > 0);
		engine->init(conf);

#pragma omp parallel for num_threads(nworker) 
		for (int i = 0; i < conf.nbody; i ++)
			engine->cal_new_velocity(i, threshold, dtime);

		Collision min_col;
		min_col.time = REAL_MAX;
#pragma omp parallel for num_threads(nworker) 
		for (int i = 0; i < conf.nbody; i ++)
		{
			Collision col =
				engine->collision_detect(i, dtime);
			if (col.id0 == -1)
				continue;
			if (col.time > 0 && col.time < min_col.time)
				min_col = col;
		}
		if (min_col.time < 0 || min_col.time > time_remain)
			break;
		ncollide ++;
		time_remain -= min_col.time;

#pragma omp parallel for num_threads(nworker)
		for (int i = 0; i < conf.nbody; i ++)
			if (i != min_col.id0 && i != min_col.id1)
				conf.body[i].advance(min_col.time);
		if (min_col.id1 == -1) // hit wall
			conf.body[min_col.id0].v[min_col.axis] *= -1;
		else
			body_collide(conf.body[min_col.id0], conf.body[min_col.id1], min_col.time, true);
	}
#pragma omp parallel for num_threads(nworker)
	for (int i = 0; i < conf.nbody; i ++)
		conf.body[i].advance(time_remain);

#else
	engine->init(conf);

#pragma omp parallel for num_threads(nworker) 
	for (int i = 0; i < conf.nbody; i ++)
		engine->cal_new_velocity(i, threshold, dtime);

	Body *new_body = new Body[conf.nbody];

	memcpy(new_body, conf.body, sizeof(Body) * conf.nbody);
#pragma omp parallel for num_threads(nworker) 
	for (int i = 0; i < conf.nbody; i ++)
	{
		int p = i; 
		Collision col =
			engine->collision_detect(i, dtime);
		if (col.id1 != -1)
		{
			if (body_collide(new_body[p], conf.body[col.id1], col.time))
			{
				ncollide ++;
				new_body[p].advance(dtime - col.time);
			}
			else new_body[p].advance(dtime);
		}
		else if (col.time > 0)
		{
			ncollide ++;
			Body &b = new_body[p];
			b.advance(col.time);
			b.v[col.axis] *= -1;
			b.advance(dtime - col.time);
		}
		else new_body[p].advance(dtime);
	}

	memcpy(conf.body, new_body, sizeof(Body) * conf.nbody);

	delete [] new_body;

#endif
	//if (ncollide)
	//	log_printf("ncollide: %d", ncollide);

	return true;
}

#endif
/**
 * vim: syntax=cpp11 foldmethod=marker
 */


/*
 * $File: openmp.hh
 * $Date: Thu Aug 30 18:48:52 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __WORKER_OPENMP_HH__
#define __WORKER_OPENMP_HH__

#ifdef ENABLE_OPENMP

#include "worker.hh"
#include "engine.hh"

class WorkerOpenMP : public Worker
{
	protected:
		bool *hash;
		Body *new_body;
		int work(const NBodyConfig &conf, int id, real_t dtime);
	public:
		virtual int advance(const NBodyConfig &conf, real_t dtime);
		virtual std::string name() const { return "openmp"; }
};

#endif
#endif // __WORKER_OPENMP_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


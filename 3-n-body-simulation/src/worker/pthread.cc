/*
 * $File: pthread.cc
 * $Date: Thu Aug 30 18:51:28 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifdef ENABLE_PTHREAD

#include "pthread.hh"

#include <cstring>
#include <pthread.h>
#include "lib/misc.hh"


struct PthreadArg
{
	WorkerPthread *worker;
	int task_type;
};

static void *__pthread_call_wrapper(void *arg_)
{
	PthreadArg *arg = static_cast<PthreadArg *>(arg_);
	if (arg->task_type == 0)
		arg->worker->threaded_calc_velocity();
	else
		arg->worker->threaded_calc_collision();
	pthread_exit(NULL);
}

void WorkerPthread::threaded_calc_velocity()
{
	int left, right;
	while (taskpool->fetch_task(left, right))
		for (int i = left; i < right; i ++)
			engine->cal_new_velocity(i, threshold, dtime);
}

void WorkerPthread::threaded_calc_collision()
{
	int left, right;
	Body *body = conf->body;
	Body *new_body = body_buf;
	while (taskpool->fetch_task(left, right))
		for (int i = left; i < right; i ++)
		{
			int p = i; 
			Collision col =
				engine->collision_detect(i, dtime);
			if (col.id1 != -1)
			{
				if (body_collide(new_body[p], body[col.id1], col.time))
				{
					ncollide ++;
					new_body[p].advance(dtime - col.time);
				}
				else new_body[p].advance(dtime);
			}
			else if (col.time > 0)
			{
				ncollide ++;
				Body &b = new_body[p];
				b.advance(col.time);
				b.v[col.axis] *= -1;
				b.advance(dtime - col.time);
			}
			else new_body[p].advance(dtime);
		}
}


int WorkerPthread::advance(const NBodyConfig &conf, real_t dtime)
{
	taskpool = new TaskScheduler();
	this->conf = &conf;
	this->dtime = dtime;
	threshold = 1;

	engine->init(conf);

	pthread_t *threads = new pthread_t[nworker];

	PthreadArg arg;
	arg.worker = this;

	arg.task_type = 0;

	int ntf_max = conf.nbody / nworker / 8;
	//printf("ntf_max: %d\n", ntf_max);
	check_min(ntf_max, 10);
	check_max(ntf_max, 100);

	taskpool->init(conf.nbody, ntf_max);
	for (int i = 0; i < nworker; i ++)
		pthread_create(threads + i, NULL, __pthread_call_wrapper, &arg);
	for (int i = 0; i < nworker; i ++)
		pthread_join(threads[i], NULL);

//	for (int i = 0; i < conf.nbody; i ++)
//		conf.body[i].advance(dtime);
#if  1
	ncollide = 0;
	arg.task_type = 1;
	taskpool->init(conf.nbody, ntf_max);
	body_buf = new Body[conf.nbody];
	memcpy(body_buf, conf.body, sizeof(Body) * conf.nbody);
	for (int i = 0; i < nworker; i ++)
		pthread_create(threads + i, NULL, __pthread_call_wrapper, &arg);
	for (int i = 0; i < nworker; i ++)
		pthread_join(threads[i], NULL);
	memcpy(conf.body, body_buf, sizeof(Body) * conf.nbody);
	//if (ncollide)
	//	log_printf("ncollide: %d", ncollide);

#endif
	delete threads;
	delete taskpool;
	delete body_buf;

	return true;
}


#endif

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


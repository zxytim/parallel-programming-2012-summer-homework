/*
 * $File: pthread.hh
 * $Date: Thu Aug 30 18:49:09 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __WORKER_PTHREAD_HH__
#define __WORKER_PTHREAD_HH__


#ifdef ENABLE_PTHREAD

#include "worker.hh"

#include <pthread.h>

class WorkerPthread : public Worker
{
	public:
		virtual int advance(const NBodyConfig &conf, real_t dtime);
		virtual std::string name() const { return "pthread"; }

		void threaded_calc_velocity();
		void threaded_calc_collision();
	protected:
		struct TaskScheduler
		{
			public:
				pthread_mutex_t mutex;
				int cur, ntask, ntf_max;
				void init(int ntask, int ntf_max)
				{ this->ntask = ntask; cur = 0; this->ntf_max = ntf_max; }
				int fetch_task(int &left, int &right)
				{
					pthread_mutex_lock(&mutex);
					int n = ntask - cur;
					if (n > ntf_max)
						n = ntf_max;
					left = cur;
					right = cur + n;
					cur += n;
					pthread_mutex_unlock(&mutex);
					return n;
				}
		};
		TaskScheduler *taskpool;
		real_t dtime;
		real_t threshold;
		int ncollide;
		const NBodyConfig *conf;

		Body *body_buf;
};

#endif

#endif // __WORKER_PTHREAD_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


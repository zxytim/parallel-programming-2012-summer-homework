#!/usr/bin/python2
# -*- coding: utf-8 -*-
# $File: gen-plot.py
# $Date: Wed Sep 05 01:28:14 2012 +0800
# $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>

import os, sys

title = ""
data_file = ""
log_x = ""
log_y = ""

log_proc = ""

procs = set()
nnumbers = set()
data = dict()

latex_dir = "../tex"
latex_spec_file = latex_dir + "/spec.tex"
latex_general_file = latex_dir + "/general.tex"

img_prefix = ""

def convert(img_file):
    os.system("convert -crop 640x330+0+190 {0} {0}" . format(img_file))
    
def gen_data(directory):
    global procs, nnumbers, data

    procs = set()
    nnumbers = set()
    data = dict()

    for fname in os.popen("ls " + directory + "/*.log").readlines():
        nnumber = int(fname.split('-')[0].split('/')[-1])
        nproc = int(fname.split('-')[1].split('.')[0])
        print nnumber, nproc
        with open(fname[0:-1]) as f:
            print fname
            time = float(f.readlines()[0])
        procs.add(nproc)
        nnumbers.add(nnumber)
        data[(nnumber, nproc)] = time 

    procs = list(procs)
    procs.sort()
    nnumbers = list(nnumbers)
    nnumbers.sort()
    
    
    for nnumber in nnumbers:
        output = ""
        for nproc in procs:
            key = (nnumber, nproc)
            if key in data:
                output += "{0} {1}\n" . format(nproc, data[key]);
            else:
                print "result of np = {0} and nn = {1} not found." .  format(nproc, nnumber)
                #output += "{0} {1}\n" . format(nproc, '/')
                data[key] = '/'
        with open((directory + "/{0}.data") . format(nnumber), "w") as f:
            f.write(output)


def MIN(l):
    ret = 10000000000000
    for i in l:
        if i < ret:
            ret = i
    return ret

def MAX(l):
    ret = -10000000000000
    for i in l:
        if i > ret:
            ret = i
    return ret

def gen_img(directory, prefix):
    global procs, nnumbers, data, title, log_x, log_y, log_proc, img_prefix
    
    max_proc = MAX(procs) + 1;
    print "max_proc", max_proc
    template = """
#!/usr/bin/gnuplot
reset
set terminal pdf size 5,2.472

set xlabel "{xlabel}"
set ylabel "{ylabel}"
set title "{title}"

set key reverse Right outside
set grid

set style data linespoints
#set size 1,0.618

set xrange [0:{x_range}]

{log_x}
{log_y}

plot  {slots}
    """ 
    output = template . format(
            xlabel = "Number of workers" + log_proc,
            ylabel = "Average FPS, log scale",
            title = title,
            log_x = log_x,
            log_y = log_y,
            x_range = max_proc,
            slots = ",".join(
                ["\"{data_file}\" using 1:{col} title \"{title}\"" .
                    format(
                        data_file = directory + "/{0}.data" . format(nnumbers[i]),
                        col = 2,
                        title = str(nnumbers[i]))
                    for i in xrange(len(nnumbers))]
                ),
            )

    fname = directory + "/plot.all.gp"
    with open(fname, "w") as f:
        f.write(output)
    
    img_prefix = "../img/" + prefix;
    img_file = img_prefix + ".pdf"
    os.system("gnuplot {fname} > {img_file}" . format(
            fname = fname,
            img_file = img_file))
    #convert(img_file)

    for number in nnumbers:
        output = template . format(
            xlabel = "Number of workers" + log_proc,
            ylabel = "Frames rendered, log scale",
            title = title,
            log_x = log_x,
            log_y = log_y,
            x_range = max_proc,
            slots = "\"{data_file}\" using 1:2 title \"{title}\"" . format(
                data_file = directory + "/{0}.data" . format(number),
                title = str(number))
            );


        fname = directory + "/plot.{0}.gp" . format(number)
        img_file  = img_prefix + "-{0}.pdf" . format(number)
        with open(fname, "w") as f:
            f.write(output)
        os.system("gnuplot {fname} > {img_file}" . format(
                fname = fname, img_file = img_file))
        #convert(img_file)


def gen_latex(prefix, caption):
    global latex_dir

    latex_template = """
\\begin{{figure}}[!ht]
\centering
\includegraphics[width=0.8\\textwidth]{{{img_file}}}
\caption{{{caption}}}
\end{{figure}}
    """

    latex_general_file = "../tex/" + prefix + "-general-fig.tex"
    img_tex_prefix = "data/img/" + prefix
    with open(latex_general_file, "w") as f:
        f.write(latex_template . format(
            img_file = img_tex_prefix +  ".pdf",
            caption = caption))

    cnt = 0
    latex_spec_file = "../tex/" + prefix + "-spec-fig.tex"
    with open(latex_spec_file, "w") as f:
        f.write("");

    for number in nnumbers:
        cnt = cnt + 1
        img_file = img_tex_prefix +  "-{0}.pdf" . format(number)
        with open(latex_spec_file, "a") as f:
            f.write(latex_template . format(
                img_file = img_file,
                caption = caption))
            if cnt % 3 == 0:
                f.write("\clearpage\n")

    with open(latex_spec_file, "a") as f:
        f.write("\clearpage\n")

# execution time using one processor
def etuop(number):
    global data, procs
    for i in procs:
        if (number, i) in data:
            np = i
            break
    rtime = data[(number, np)]
    return rtime

def is_number(num):
    t = num
    try:
        t += 1
    except TypeError:
        return False
    return True

def gen_effeciency_table(prefix, caption):
    global latex_dir, nnumbers, procs
    
    fname = latex_dir + "/" + prefix + ".efcc.table.tex"
    #         <nproc0> <nproc1>
    #  <size> 
    print prefix


    start_np = 1;
    for i in procs:
        if (nnumbers[0], i) in data:
            start_np = i
            break

    def efficienty(etuop, rtime, nproc):
        if not is_number(rtime):
            return "NaN"
        #eff = etuop / (rtime * (nproc + 1 - start_np)) * 100;
        eff = 1.0 / (etuop / rtime * nproc / start_np)
        print eff
        return "%3.2f" % (eff)


    content = """
\\begin{{table}}[!ht]
    \\centering
    \\begin{{tabular}}{{{tfmt}}}
        {th}
        {data}
    \\end{{tabular}}
    \\caption{{{caption}}}
\\end{{table}}
    """ . format(
            tfmt = "|".join([
                ">{\\centering\\arraybackslash}p{0.3in}"
                for i in xrange(len(procs) + 1)
                ]),
            th = "& {head} \\\\\\hline" . format(head = "&".join([
                str(proc) for proc in procs
                ])),
            data = "\\\\\\hline\n" . join([
                "{size} & {data}" . format(
                size = number,
                data = "&".join([
                    str(efficienty(etuop(number), data[(number, nproc)],
                        nproc)) for nproc in procs
                    ]))
                for number in nnumbers]),
            caption = "Efficiency of " + caption + ", in percent"
            )

    print content
    with open(fname, "w") as f:
        f.write(content)

def gen_table(prefix, caption):
    global latex_dir, nnumbers, procs
    
    fname = latex_dir + "/" + prefix + ".table.tex"
    #         <nproc0> <nproc1>
    #  <size> 
    print prefix
    
    def to_number(num):
        t = num
        try:
            t += 1
        except TypeError:
            return "NaN"
        return "%.4g" % (num)

    content = """
\\begin{{table}}[!ht]
    \\centering
    \\begin{{tabular}}{{{tfmt}}}
        {th}
        {data}
    \\end{{tabular}}
    \\caption{{{caption}}}
\\end{{table}}
    """ . format(
            tfmt = "|".join([
                ">{\\centering\\arraybackslash}p{0.27in}"
                for i in xrange(len(procs) + 1)
                ]),
            th = "& {head} \\\\\\hline" . format(head = "&".join([
                str(proc) for proc in procs
                ])),
            data = "\\\\\\hline\n" . join(["{size} & {data}" . format(
                size = number,
                data = "&".join([
                    to_number(data[(number, nproc)])
                     for nproc in procs
                    ])) 
                for number in nnumbers]),
            caption = "Average FPS in " + caption 
            )

    print content
    with open(fname, "w") as f:
        f.write(content)

def gen_plot(directory, prefix, caption):
    gen_data(directory)
    gen_img(directory, prefix)
    gen_latex(prefix, caption)
    gen_table(prefix, caption)
    gen_effeciency_table(prefix, caption)


def main():

    global title, log_x, log_y, log_proc

    name = "Heat Distribution Simulation"
    title_prefix = name

    params = [
            (0, 1, title_prefix + " using MPI", "hds-mpi",
             "../trimed/mpi",  name + " using MPI"),
                (0, 1, title_prefix + " using Pthread", "hds-pthread",
                    "../trimed/pthread/", name + " using pthread"),
                (0, 1, title_prefix + " using OpenMP", "hds-openmp",
                    "../trimed/openmp/", name + " using OpenMP"),
                (0, 1, title_prefix + " using Pthread", "hds-pthread-clst",
                    "../trimed/pthread.clst/", name + " using pthread"),
                (0, 1, title_prefix + " using OpenMP", "hds-openmp-clst",
                    "../trimed/openmp.clst/", name + " using OpenMP")
                ]

    # clear latex file
    with open(latex_spec_file, "w") as f:
        f.write("");
    with open(latex_general_file, "w") as f:
        f.write("");

    for (lx, ly, title, prefix, directory, caption) in params:
        title = title
        log_x = ""
        log_proc = ""
        if lx == 1:
            log_x = "set log x 2"
            log_proc = ", log scale"

        log_y = "set log y 2"
        if ly == 0:
            log_y = ""
        print ly, log_y

        gen_plot(directory, prefix, caption)


if __name__ == "__main__":
    main()



#!/bin/bash

for DIR in "../log/mpi" ; do
	for fname in `cd $DIR && ls *.stdout`; do
		real_file=$DIR/$fname
		if ! grep speed $real_file >/dev/null; then
			echo $fname
			rm $real_file
		else
			frame=`grep speed $real_file`
			echo $frame
			echo $frame > $real_file
		fi
	done
done

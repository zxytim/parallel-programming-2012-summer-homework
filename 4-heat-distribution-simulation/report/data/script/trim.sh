#!/bin/bash

DIR=../log
OUTDIR=../trimed
for para in openmp pthread mpi openmp.clst pthread.clst; do
	echo $para
	dir=$DIR/$para
	[ ! -d "$dir" ] && continue
	outdir=$OUTDIR/$para
	mkdir -p $outdir
	for fname in  `cd $dir && ls *.stdout`; do
		np=`echo $fname | cut -d '-' -f 7 | cut -d '.' -f1`
		size=`echo $fname | cut -d'-' -f 5`
		echo $size-$np
		frames=`cat $dir/$fname | grep speed | sed -e 's/[^0-9\.]//g'`
		echo $frames > $outdir/$size-$np.log
	done
done

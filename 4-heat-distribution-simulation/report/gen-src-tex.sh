#!/bin/bash

for f in `find -L src -name "*.cc" -o -name "*.hh" -o -name "*.c" -o -name "*.h"\)`; do
	echo "\\cppsrc{$f}"
done > src.tex

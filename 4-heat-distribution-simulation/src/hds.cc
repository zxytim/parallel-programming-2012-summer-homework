/*
 * $File: hds.cc
 * $Date: Sun Sep 02 18:13:45 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include "hds.hh"

#include <cstring>

HDSConfig::HDSConfig(const Domain &domain)
{
	this->domain = domain;
	data = new heat_t[domain.size()];
	memset(data, 0, sizeof(heat_t) * domain.size());
	data[domain.size() / 2] = 1.0;
}

HDSConfig::~HDSConfig()
{
	delete [] data;
}


void HDSConfig::fill(const Rectangle &rect, real_t heat)
{
	int x0 = rect.x, y0 = rect.y,
		x1 = rect.x + rect.width,
		y1 = rect.y + rect.height;

	if (x0 < 0) x0 = 0;
	if (y0 < 0) y0 = 0;
	if (x1 > (int)domain.width)
		x1 = domain.width;
	if (y1 > (int)domain.height)
		y1 = domain.height;

	for (int i = x0; i < x1; i ++)
		for (int j = y0; j < y1; j ++)
			data[domain.index(i, j)] = heat;
}

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


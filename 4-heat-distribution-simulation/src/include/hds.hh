/*
 * $File: hds.hh
 * $Date: Sun Sep 02 18:09:08 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __HDS_HH__
#define __HDS_HH__

#include "lib/type.hh"
#include <cstdlib>

typedef real_t heat_t;

class HeatDiffusionType
{
	public:
		static const int FOUR_DIR = 0,
					 EIGHT_DIR = 1;

		/*
		 * FOUR_DIR:
		 *		0     0.25     0
		 *		0.25     x  0.25
		 *		0     0.25     0
		 *
		 * EIGHT_DIR:
		 *      1/12   1/6   1/12
		 *      1/6      x    1/6
		 *      1/12   1/6   1/12  
		 *		
		 */
};

/*
 * Domain size decides the precision of simulation,
 * but also there's a trade-off between precision
 * and time consumption
 */
struct Domain
{
	size_t width, height;
	size_t size() const { return width * height; }
	Domain() {}
	Domain(size_t width, size_t height) : 
		width(width), height(height) {}
	size_t index(int x, int y) const
	{ return x * height + y; }
};

struct Rectangle
{
	int x, y;
	int width, height;
	Rectangle(int x, int y, int width, int height) :
		x(x), y(y), width(width), height(height) {}
};

/*
 * Heat Distribution Simulation
 */
class HDSConfig
{
	private:
		HDSConfig(const HDSConfig&);
		HDSConfig &operator = (const HDSConfig &);

	public:
		Domain domain;
		heat_t *data;
		HDSConfig(const Domain &domain);
		~HDSConfig();
		void fill(const Rectangle &rect, real_t heat);
};

#endif // __HDS_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


/*
 * $File: image.hh
 * $Date: Sat Sep 01 15:53:00 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __IMAGE_HH__
#define __IMAGE_HH__

#include "lib/type.hh"
#include "lib/color.hh"

typedef ColorRGB ColorGray;

struct Image
{
	int width, height;
	ColorRGB *data;
	Image(int width, int height);
	void color(int x, int y, const ColorRGB &color);
	ColorRGB color(int x, int y);
	~Image();
};

#endif // __IMAGE_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


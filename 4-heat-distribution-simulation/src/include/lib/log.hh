/*
 * $File: log.hh
 * $Date: Sun Sep 02 17:22:00 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __LOG_HH__
#define __LOG_HH__

#include <cstdio>
#include <string>
#include <iostream>

#define log_printf(fmt, ...) \
	do { \
		static const int MSG_LEN_MAX = 1000; \
		static char msg[MSG_LEN_MAX + 1]; \
		sprintf(msg, "--[%s@%s:%d]: " fmt, __PRETTY_FUNCTION__, __FILE__, __LINE__, ##__VA_ARGS__); \
		fprintf(stderr, "%s\n", msg); \
	} while (0)


class ScopedLogger
{
	protected:
		std::string msg;
		void print(std::string suffix)
		{ std::cout << msg << suffix << std::endl; }
	public:
		ScopedLogger() {}
		void set_msg(std::string msg)
		{ this->msg = msg; print(" -- start"); }
		ScopedLogger(std::string msg) : 
			msg(msg) { print(" -- start"); }
		~ScopedLogger()
		{ print(" -- end"); }
};

#define SCOPED_LOGGER(fmt, ...) \
	ScopedLogger __a_very_long_name_logger__##__LINE__; \
	do { \
		static const int MSG_LEN_MAX = 1000; \
		static char msg[MSG_LEN_MAX + 1]; \
		sprintf(msg, "--[%s@%s:%d]: " fmt, __PRETTY_FUNCTION__, __FILE__, __LINE__, ##__VA_ARGS__); \
		__a_very_long_name_logger__##__LINE__.set_msg(msg); \
	} while (0)


#endif // __LOG_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


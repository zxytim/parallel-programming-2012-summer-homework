/*
 * $File: misc.hh
 * $Date: Mon Sep 03 23:20:02 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __MISC_HH__
#define __MISC_HH__

#include "type.hh"

#include <cstdlib>

const real_t EPS = 1e-6;

int Rand(int max = RAND_MAX);
int RandRange(int min, int max);

template<typename T>
void check_min(T &a, const T &b)
{ if (a > b) a = b; }
template<typename T>
void check_max(T &a, const T &b)
{ if (a < b) a = b; }


#endif // __MISC_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


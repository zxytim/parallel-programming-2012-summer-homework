/*
 * $File: type.hh
 * $Date: Sat Sep 01 15:08:13 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __TYPE_HH__
#define __TYPE_HH__


#include <limits>

#ifdef REAL_T_LONG_DOUBLE
typedef long double real_t;
#define REAL_T_FMT "Lf"
#else
typedef double real_t;
#define REAL_T_FMT "lf"
#endif

const real_t REAL_MIN = -std::numeric_limits<real_t>::max(),
	  REAL_MAX = std::numeric_limits<real_t>::max();

#endif // __TYPE_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


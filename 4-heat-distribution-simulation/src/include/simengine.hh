/*
 * $File: simengine.hh
 * $Date: Mon Sep 03 21:30:10 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __SIMENGINE_HH__
#define __SIMENGINE_HH__

#include "hds.hh"
#include <cstdio>
#include <string>

struct SimEngineConf 
{
	SimEngineConf()
	{
		in_test = false;
	}

	int heat_diff_type;
	Domain domain;
	HDSConfig *hdsconf;

	int nworker;
	bool in_test;

	int argc;
	char **argv;
};

/*
 * Simulation Engine of Heat Distribution
 */
class SimEngine
{
	public:
		virtual std::string name() const = 0;
		virtual int init(SimEngineConf &seconf) = 0;
		virtual int iterate(HDSConfig *hdsconf) = 0;
		virtual int get_nworker() const  = 0;
		virtual ~SimEngine() {
			printf("destructor\n");
		}
};

#endif // __SIMENGINE_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


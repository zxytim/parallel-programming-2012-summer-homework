/*
 * $File: mpi.hh
 * $Date: Mon Sep 03 21:43:38 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __SIMENGINE_MPI_HH__
#define __SIMENGINE_MPI_HH__
#ifdef ENABLE_MPI

#include "simengine.hh"
#include <mpi.h>

class MPISimEngine : public SimEngine
{
	public:
		virtual std::string name() const { return "mpi"; }
		virtual int init(SimEngineConf &seconf);
		virtual int iterate(HDSConfig *hdsconf);
		virtual int get_nworker() const { return nproc; }

		MPISimEngine();
		virtual ~MPISimEngine();

	protected:
		SimEngineConf seconf;

		int proc_id, nproc;

		struct Partition
		{
			int ncol, nrow;
			int row_id, col_id;
			int height, width, domain_size;
			int start_x, start_y, end_x, end_y;
			int index(int x, int y)
			{
				return x * height + y;
			}
		};
		Domain exd; // extended domain

		Partition ptt;

		Partition *ptt_other;

		heat_t *data, *data_new;
		Partition calculate_partition(int proc_id, int nproc);

		static const int DIR_UP = 0,
					 DIR_DOWN = 1,
					 DIR_LEFT = 2,
					 DIR_RIGHT = 3;

		int neighbor(int dir);

		struct MPITag
		{
			static const int TAG_EXCHANGE = 1;
		};
		struct CommandType
		{
			static const int START_NEXT_ITER = 1,
						 EXIT = 2;

		};

		struct Command
		{
			int type;
		};

		MPI_Datatype MPI_TYPE_CMD,
					 MPI_TYPE_HEAT_T;

		MPI_Status status_dummy;

		void exchange_data();
		void do_iterate();
		void gather_data();
		void exchange(int nb, int nd);
		size_t get_ex_size(int dir);
		void fill_exdata(heat_t *exdata, int ex_to_dir);
		void fill_data(heat_t *exdata, int ex_from_dir);

		void send_data(heat_t *data, size_t size, int pid);
		void recv_data(heat_t *data, size_t size, int pid);

		/*
		 * master
		 */
		void mst_bcast_cmd(Command &cmd);
		heat_t *recvbuf;
		int *recvcnts, *displs;
		void convert_gathered_data(HDSConfig *hdsconf);

		/*
		 * slave
		 */
		int slv_worker(HDSConfig *hdsconf);
		void slv_recv_cmd(Command &cmd);

		/*
		 * debug
		 */
		void print_partition();
};


#endif // ENABLE_MPI
#endif // __SIMENGINE_MPI_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


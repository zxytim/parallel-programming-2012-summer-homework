/*
 * $File: openmp.hh
 * $Date: Sun Sep 02 23:04:13 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __SIMENGINE_OPENMP_HH__
#define __SIMENGINE_OPENMP_HH__

#include "simengine.hh"

class OpenMPSimEngine : public SimEngine
{
	public:
		virtual std::string name() const { return "openmp"; }
		virtual int init(SimEngineConf &seconf);
		virtual int iterate(HDSConfig *hdsconf);
		virtual int get_nworker() const { return nworker; }

	protected:
		int nworker;
		SimEngineConf seconf;
		real_t *data;
};

#endif // __SIMENGINE_OPENMP_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


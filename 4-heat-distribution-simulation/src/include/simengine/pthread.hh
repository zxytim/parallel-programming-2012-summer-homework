/*
 * $File: pthread.hh
 * $Date: Wed Sep 05 00:07:56 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __SIMENGINE_PTHREAD_HH__
#define __SIMENGINE_PTHREAD_HH__
#ifdef ENABLE_PTHREAD

#include "simengine.hh"
#include <unistd.h>
#include <pthread.h>

struct PthreadArg;

class PthreadSimEngine : public SimEngine
{
	public:
		virtual std::string name() const { return "pthread"; }
		virtual int init(SimEngineConf &seconf);
		virtual int iterate(HDSConfig *hdsconf);
		virtual int get_nworker() const { return nworker; }
		void set_cpu_affinity(bool affinity) { cpu_affinity = affinity; }

		PthreadSimEngine();
		~PthreadSimEngine();

		void thread_worker(int tid);
	protected:
		bool cpu_affinity;
		int nworker;
		struct CommandType
		{
			static const int WAIT = 0,
						 NEW_TASK = 1,
						 EXIT = 2;
		};

		struct Command
		{
			int tid;
			int type;
			int start, end;
		};

		class Commander
		{
			protected:
				pthread_mutex_t mutex;
				pthread_cond_t cond;

				int ntask, cur, ntf_max;
				
				static const int STATUS_STARTUP = 0,
							 STATUS_EXIT = 1,
							 STATUS_NO_TASK = 2,
							 STATUS_DIST_TASK = 3;
				int status;
				int nworker;

				static const int WSTATUS_RUNNING = 0,
							 WSTATUS_FINISHED = 1;
				int *worker_status;
				int nworker_finished;
			public:
				Commander();
				~Commander();
				void set_nworker(int nworker);
				void set_ntf_max(int ntf);
				void wait_for_new_task_finish(const Domain &domain);
				void set_exit();
				void get_command(Command &cmd);
		} commander;

		SimEngineConf seconf;
		HDSConfig *hdsconf;

		real_t *data, *hdata;

		pthread_t *threads;
		pthread_attr_t *attr;
		cpu_set_t *cpuset;
		void on_exit();
		PthreadArg *targs;

};


struct PthreadArg
{
	PthreadSimEngine *engine;
	int tid;
};

#endif // ENABLE_PTHREAD
#endif // __SIMENGINE_PTHREAD_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


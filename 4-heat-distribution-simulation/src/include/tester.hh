/*
 * $File: tester.hh
 * $Date: Sun Sep 02 18:14:53 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __TESTER_HH__
#define __TESTER_HH__

#include "simengine.hh"
#include <string>

struct TestConfig
{
	HDSConfig *hdsconf;
	SimEngineConf seconf;
	SimEngine *engine; 

	real_t test_duration;
};

struct TestResult
{
	bool valid;
	Domain domain;
	std::string engine_name;
	int nworker;
	size_t niter;
	real_t test_duration;
	real_t niter_per_sec;
};

class Tester
{
	public:
		TestResult test(TestConfig &config);
};

#endif // __TESTER_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


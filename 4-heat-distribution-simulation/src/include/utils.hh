/*
 * $File: utils.hh
 * $Date: Sat Sep 01 16:49:51 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifndef __UTILS_HH__
#define __UTILS_HH__

#include "lib/exception.hh"
#include "lib/color.hh"
#include "lib/sassert.hh"
#include "lib/type.hh"
#include "lib/timer.hh"
#include "lib/misc.hh"
#include "lib/log.hh"
#include "lib/image.hh"

#include <cstdio>
#include <cstring>
#include <cstdlib>

#endif // __UTILS_HH__

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


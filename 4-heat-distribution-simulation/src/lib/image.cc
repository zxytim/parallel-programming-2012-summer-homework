/*
 * $File: image.cc
 * $Date: Sat Sep 01 15:53:19 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include "lib/image.hh"

Image::Image(int width, int height) :
	width(width), height(height)
{
	data = new ColorRGB[width * height];
}

Image::~Image()
{
	delete[] data;
}

void Image::color(int x, int y, const ColorRGB &color)
{
	data[x * width + y] = color;
}

ColorRGB Image::color(int x, int y)
{
	return data[x * width + y];
}

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


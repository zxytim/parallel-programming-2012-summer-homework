/*
 * $File: main.cc
 * $Date: Wed Sep 05 00:05:45 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */


#include <cmath>
#include "utils.hh"

#include <getopt.h>
#include <unistd.h>
#include <cstdlib>

#ifdef USE_GTK
#include <cairo.h>
#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#endif

#include "utils.hh"

#include "simengine.hh"
#include "tester.hh"

#ifdef ENABLE_OPENMP
#include "simengine/openmp.hh"
#endif

#ifdef ENABLE_PTHREAD
#include "simengine/pthread.hh"
#endif

#ifdef ENABLE_MPI
#include "simengine/mpi.hh"
#endif

#define error_exit(exit_code, fmt, ...) \
	do { \
		static const int MSG_LEN_MAX = 1000; \
		static char msg[MSG_LEN_MAX + 1]; \
		sprintf(msg, "error %d: " fmt, exit_code, ##__VA_ARGS__); \
		fprintf(stderr, "%s\n", msg); \
		exit(exit_code); \
	} while (0)

#ifdef USE_GTK
GdkPixbuf *image_to_pixbuf(Image *image);
#endif

struct RenderConfig
{
	HDSConfig *hdsconf;
	SimEngine *engine;


	static const int COLSCHM_GRAY,
				 COLSCHM_RGB;
	bool color_scheme;
	Image *image;

	size_t n_iter_finished;

	int n_iter_per_sec;
#ifdef USE_GTK
	GdkPixbuf *pixbuf; 
#endif

	RenderConfig()
	{
		memset(this, 0, sizeof(RenderConfig));
		color_scheme = COLSCHM_GRAY;
		n_iter_per_sec = 1000;
		n_iter_finished = 0;
	}

	void set_color_scheme(int scheme)
	{
		color_scheme = scheme;
	}

	void set_n_iter_per_sec(int n)
	{ n_iter_per_sec = n; }

	~RenderConfig()
	{
#ifdef USE_GTK
		if (pixbuf)
			g_object_unref(pixbuf);
#endif
	}

	int width () const { return hdsconf->domain.width; }
	int height() const { return hdsconf->domain.height; }

	heat_t heat_max;
	void setHDSConfig(HDSConfig *hdsconf)
	{
		this->hdsconf = hdsconf;
		if (image)
			delete image;
		image = new Image(hdsconf->domain.width, 
				hdsconf->domain.height);

		heat_max = -1;
		for (size_t i = 0; i < hdsconf->domain.size(); i ++)
			check_max(heat_max, hdsconf->data[i]);

#ifdef USE_GTK
		to_pixbuf();
#endif
	}

#ifdef USE_GTK
	void release_pixbuf()
	{
		if (pixbuf)
		{
			g_object_unref(pixbuf);
			pixbuf = NULL;
		}
	}
#endif

	size_t get_n_iter_finished() const
	{ return n_iter_finished; }

	bool render(real_t dtime)
	{
		if (dtime < 0.001)
			return false;
		for (int niter = n_iter_per_sec * dtime; niter --; )
			n_iter_finished += engine->iterate(hdsconf);

#ifdef USE_GTK
		to_pixbuf();
#endif
		return true;
	}

	void heat2image()
	{
		const Domain &domain = hdsconf->domain;
		heat_t *data = hdsconf->data;
		for (size_t i = 0, p = 0; i < domain.width; i ++)
			for (size_t j = 0; j < domain.height; j ++, p ++)
			{
				//printf("%.2lf\n", data[p]);
				if (color_scheme == COLSCHM_GRAY)

					image->color(i, j, ColorGray(data[p] / heat_max));
				else
				{
					real_t k = data[p] / heat_max;
					ColorRGB rgb(
							k / 0.32 - 0.78125,
							sin(M_PI * k),
							cos(M_PI / 2 * k));
#define CHECK_COLOR(c) do { if ((c) < 0) (c) = 0; if ((c) > 1) (c) = 1; } while (0)
					CHECK_COLOR(rgb.red);
					CHECK_COLOR(rgb.green);
					CHECK_COLOR(rgb.blue);
					image->color(i, j, rgb);
				}
			}
	}

#ifdef USE_GTK
	void to_pixbuf()
	{
		if (hdsconf->data)
		{
			heat2image();
			release_pixbuf();
			pixbuf = image_to_pixbuf(image);
		}
	}
#endif
};
const int RenderConfig::COLSCHM_GRAY = 0,
	  RenderConfig::COLSCHM_RGB = 1;


RenderConfig rconf;
Timer draw_timer,
	  render_timer;
FpsCounter fps_cnt;

#ifdef USE_GTK
/* Gtk callbacks {{{ */
static gboolean delete_event(
		GtkWidget *,
		GdkEvent *,
		gpointer )
{
	return FALSE;
}

static void destroy(GtkWidget *, gpointer )
{
	gtk_main_quit();
}


GdkPixbuf *image_to_pixbuf(Image *image)
{
	GdkPixbuf *pixbuf = gdk_pixbuf_new(
			GDK_COLORSPACE_RGB, false, 8, image->width, image->height);
	int n_channels = gdk_pixbuf_get_n_channels(pixbuf),
		rowstride = gdk_pixbuf_get_rowstride(pixbuf);

	guchar *pixel = gdk_pixbuf_get_pixels(pixbuf);
	for (int i = 0; i < image->width; i ++)
		for (int j = 0; j < image->height; j ++)
		{
			guchar *p = pixel + j * rowstride + i * n_channels;
			ColorRGB *c = image->data + i * image->height + j;
			p[0] = c->red * 255;
			p[1] = c->green * 255;
			p[2] = c->blue * 255;
			//printf("%d %d %d\n", pixel[0], pixel[1], pixel[2]);
		}
	return pixbuf;
}

static gboolean da_expose_callback(
		GtkWidget *widget, 
		GdkEventExpose *, 
		gpointer )
{
	if (rconf.render(draw_timer.end() / 1000.0))
		draw_timer.begin();

	fps_cnt.count();

	GdkPixbuf *pixbuf = rconf.pixbuf;

	cairo_t *cr = gdk_cairo_create(widget->window);

	gdk_cairo_set_source_pixbuf(cr, pixbuf, 0, 0);

	cairo_paint(cr);


	cairo_set_source_rgb(cr, 0.1, 0.5, 1.0);
	cairo_select_font_face(cr, "Purisa",
			CAIRO_FONT_SLANT_NORMAL,
			CAIRO_FONT_WEIGHT_BOLD) ;
	cairo_set_font_size(cr, 13);

	int pos = 0, stride = 15;
	char sengine[20];
	snprintf(sengine, 19, "engine: %s", rconf.engine->name().c_str());
	cairo_move_to(cr, 20, pos += stride);
	cairo_show_text(cr, sengine);

	char snworker[20];
	snprintf(snworker, 19, "nworker: %d", rconf.engine->get_nworker());
	cairo_move_to(cr, 20, pos += stride);
	cairo_show_text(cr, snworker);

	char sfps[20];
	snprintf(sfps, 19, "FPS: %.2lf", fps_cnt.fps());
	cairo_move_to(cr, 20, pos += stride);
	cairo_show_text(cr, sfps);

	char sniter[30];
	snprintf(sniter, 29, "iteration: %lu", rconf.get_n_iter_finished());
	cairo_move_to(cr, 20, pos += stride);
	cairo_show_text(cr, sniter);

	char stime[30];
	snprintf(stime, 29, "time: %.2lfs", render_timer.end() / 1000.0);
	cairo_move_to(cr, 20, pos += stride);
	cairo_show_text(cr, stime);

	char sspeed[30];
	snprintf(sspeed, 29, "speed: %.2lf iter/sec", 
			rconf.get_n_iter_finished() / (render_timer.end() / 1000.0));
	cairo_move_to(cr, 20, pos += stride);
	cairo_show_text(cr, sspeed);

	cairo_destroy(cr);
	return FALSE;
}

static gboolean cb_timeout(GtkWidget *widget)
{
	if (widget->window == NULL)
		return FALSE;

	gtk_widget_queue_draw_area(widget, 0, 0, widget->allocation.width, widget->allocation.height);
	return TRUE;
}

real_t zoom_scale = 2.0;

static gboolean cb_clicked(GtkWidget *, GdkEventButton *,
		gpointer )
{
	return TRUE;
}


void window_init(int argc, char *argv[])
{
	gtk_init(&argc, &argv);
}

void window_show()
{
	/* Gtk window {{{ */
	GtkWidget *window;

	int border_width = 0;
	int window_width = rconf.width() + border_width * 2,
		window_height = rconf.height() + border_width * 2;
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_container_set_border_width(GTK_CONTAINER(window), border_width);
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size(GTK_WINDOW(window), window_width, window_height);

	//gtk_widget_set_app_paintable(window, TRUE);
	gtk_widget_set_size_request(window, window_width, window_height);

	GtkWidget *da = gtk_drawing_area_new();
	gtk_widget_set_size_request(da, rconf.width(), rconf.height());

	g_signal_connect(window, "delete-event",
			G_CALLBACK(delete_event), NULL);
	g_signal_connect(window, "destroy",
			G_CALLBACK(destroy), NULL);

	gtk_widget_add_events(da, GDK_BUTTON_PRESS_MASK);
	g_signal_connect(da, "button-press-event", 
			G_CALLBACK(cb_clicked), NULL);

	g_signal_connect(da, "expose_event",
			G_CALLBACK(da_expose_callback), NULL);

	g_timeout_add(1, (GSourceFunc)cb_timeout, da);

	gtk_container_add(GTK_CONTAINER(window), da);
	gtk_widget_show_all(window);

	draw_timer.begin();
	gtk_main();

	/* }}} */
}


#endif

/*
 * '-' indicates stdout
 */
void image_save_to_ppm(Image *image, const string &filename)
{

	FILE *fout;
	if (filename == "-")
		fout = stdout;
	else fout = fopen((filename).c_str(), "w");

	fprintf(fout, "P6\n%d %d\n255\n", image->width, image->height);
	unsigned char pixel[3];
	for (int j = 0; j < image->height; j ++)
		for (int i = 0; i < image->width; i ++)
		{
			ColorRGB *c = image->data + i * image->height + j;
			pixel[0] = c->red * 255;
			pixel[1] = c->green * 255;
			pixel[2] = c->blue * 255;
			fwrite(pixel, sizeof(pixel), 1, fout);
		}

	if (fout != stdout)
		fclose(fout);
}

void image_save_to_pgm(Image *image, const string &filename)
{

	FILE *fout;
	if (filename == "-")
		fout = stdout;
	else fout = fopen((filename).c_str(), "w");

	fprintf(fout, "P5\n%d %d\n255\n", image->width, image->height);
	unsigned char pixel;
	for (int j = 0; j < image->height; j ++)
		for (int i = 0; i < image->width; i ++)
		{
			ColorRGB *c = image->data + i * image->height + j;
			pixel = c->red * 255;
			fwrite(&pixel, sizeof(char), 1, fout);
		}

	if (fout != stdout)
		fclose(fout);
}


/* }}} */

	template<typename T>
void check_lower_bound(T &num, T lower_bound)
{
	if (num < lower_bound)
		num = lower_bound;
}

int str2num(const std::string &str)
{
	int ret = 0, sign = 1, start = 0;
	if (str[0] == '-')
		sign = -1, start = 1;
	for (size_t i = start; i < str.length(); i ++)
		ret = ret * 10 + str[i] - '0';
	return ret * sign;
}

real_t str2real(const std::string &str)
{
	real_t ret;
	sscanf(str.c_str(), "%" REAL_T_FMT, &ret);
	return ret;
}

const char *progname;

SimEngine *engine_factory(const std::string &engine_name)
{
#ifdef ENABLE_OPENMP
	if (engine_name == "openmp")
		return new OpenMPSimEngine();
#endif

#ifdef ENABLE_PTHREAD
	if (engine_name == "pthread")
		return new PthreadSimEngine();
#endif

#ifdef ENABLE_MPI
	if (engine_name == "mpi")
		return new MPISimEngine();
#endif

	return NULL;
}

void print_test_result(const TestResult &rst)
{
	if (!rst.valid)
		return;
	printf("------ Test Result -------\n");
	printf("name: %s\n", rst.engine_name.c_str());
	printf("nworker: %d\n", rst.nworker);
	printf("size: %lux%lu\n", rst.domain.width, rst.domain.height);
	printf("niter: %lu\n", rst.niter);
	printf("duration: %.2Lf\n", rst.test_duration);
	printf("speed: %.2Lf iter/sec\n", rst.niter_per_sec);
}

#include "cmdline.h"

Domain get_domain(const char *size)
{
	int width, height;
	if (sscanf(size, "%dx%d", &width, &height) != 2)
	{
		fprintf(stderr, "invalid size `%s' given, abort\n", size);
		exit(-1);
	}
	return Domain(width, height);
}

int main(int argc, char *argv[])
{
	progname = argv[0];

	gengetopt_args_info arginfo;
	if (cmdline_parser(argc, argv, &arginfo) != 0)
		exit(-1);

	int nworker = arginfo.nworker_arg;
	if (nworker <= 0)
		nworker =  sysconf(_SC_NPROCESSORS_ONLN);

#ifdef USE_GTK
	window_init(argc, argv);
#endif

	Domain domain(get_domain(arginfo.size_arg));
	HDSConfig *hdsconf;
	if (arginfo.test_flag)
		hdsconf = NULL;
	else hdsconf = new HDSConfig(domain);
	if (!arginfo.test_flag)
	{
		//hdsconf->fill(Rectangle(0, 0, domain.width, domain.height), 1);

#if 0
		for (size_t i = 0, p = 0; i < domain.width; i ++)
			for (size_t j = 0; j < domain.height; j ++, p ++)
				hdsconf->fill(Rectangle(i, j, 1, 1),  
						(real_t)(p) / domain.size());
#endif


		real_t ratio;
		ratio = 0.05;
		ratio = 0.5;
		hdsconf->fill(Rectangle(domain.width * (0.5 - ratio / 2), domain.height * (0.5 - ratio / 2), domain.width * ratio, domain.height * ratio), 1);
	}
	std::string engine_name = arginfo.parallel_arg;

	SimEngineConf seconf;
	seconf.heat_diff_type = HeatDiffusionType::FOUR_DIR;
	seconf.domain = domain;
	seconf.hdsconf = hdsconf;
	seconf.argc = argc;
	seconf.argv = argv;
	seconf.nworker = nworker;
	seconf.in_test = false;
	rconf.set_color_scheme((string)(arginfo.disp_arg) == "rgb" ? RenderConfig::COLSCHM_RGB : RenderConfig::COLSCHM_GRAY);

	if (arginfo.test_flag)
	{
		seconf.in_test = true;
		TestConfig testconf;
		testconf.hdsconf = hdsconf;
		testconf.seconf = seconf;
		testconf.engine = engine_factory(engine_name);
		testconf.test_duration = arginfo.duration_arg;

		Tester tester;
		print_test_result(tester.test(testconf));

	}
	else
	{
		rconf.engine = engine_factory(engine_name);
		rconf.engine->init(seconf);
		rconf.setHDSConfig(hdsconf);
		rconf.set_n_iter_per_sec(1000);

		render_timer.begin();

		for (int i = 0; i --; )
			rconf.engine->iterate(hdsconf);

#ifdef USE_GTK
		if (rconf.engine->iterate(hdsconf))
		{
			rconf.to_pixbuf();
			window_show();
		}
#else
		fprintf(stderr, "Compiled without GTK, can not show window.\n");
		exit(-1);
#endif

	}
	delete hdsconf;
	delete rconf.engine;
#ifdef ENABLE_MPI
	if (engine_name == "mpi")
		MPI_Finalize();
#endif
	return 0;
}

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


/*
 * $File: mpi.cc
 * $Date: Tue Sep 04 00:54:17 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifdef ENABLE_MPI
#include "simengine/mpi.hh"

#include "utils.hh"

#define MPI_SCPLGR(fmt, ...) \
	SCOPED_LOGGER("<worker %d>" fmt, proc_id, ##__VA_ARGS__)
#define mpi_printf(fmt, ...) \
	log_printf("<worker %d>" fmt, proc_id, ##__VA_ARGS__)

static const int ROOT_PROC = 0;

static const int DIR[4][2] = {
	{0, -1}, {0, 1}, {-1, 0}, {1, 0}
};

int MPISimEngine::init(SimEngineConf &seconf)
{
	this->seconf = seconf;
	MPI_Init(&this->seconf.argc, &this->seconf.argv);
	MPI_Comm_size(MPI_COMM_WORLD, &nproc);
	MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);

	seconf.nworker = nproc;

	ptt = calculate_partition(proc_id, nproc);

	print_partition();

	exd.height = ptt.height + 2;
	exd.width = ptt.width + 2;
	if (ptt_other)
		delete [] ptt_other;
	ptt_other = new Partition[nproc];
	for (int i = 0; i < nproc; i ++)
		ptt_other[i] = calculate_partition(i, nproc);

	if (data)
		delete [] data;
	if (data_new)
		delete [] data_new;
	data = new heat_t[exd.height * exd.width];
	data_new = new heat_t[ptt.height * ptt.width];

	// ensure border leftovers are 0
	memset(data, 0, sizeof(data));

	if (seconf.in_test)
	{
		for (int i = 0; i < ptt.width; i ++)
			for (int j = 0; j < ptt.height; j ++)
				data[exd.index(i + 1, j + 1)] = 1.0;
	}
	else
	{
		for (int i = ptt.start_x, px = 1; i < ptt.end_x; i ++, px ++)
			for (int j = ptt.start_y, py = 1; j < ptt.end_y; j ++, py ++)
			{
				data[exd.index(px, py)] = 
					seconf.hdsconf->data[seconf.hdsconf->domain.index(i, j)];
			}
	}

	if (proc_id == ROOT_PROC && !seconf.in_test)
	{
		if (recvbuf)
			delete recvbuf;
		recvbuf = new heat_t[seconf.hdsconf->domain.size()];
	}

	if (recvcnts)
		delete recvcnts;
	recvcnts = new int[nproc];
	if (displs)
		delete displs;
	displs = new int[nproc];

	for (int i = 0; i < nproc; i ++)
	{
		if (i > 0)
			displs[i] = displs[i - 1] + recvcnts[i - 1];
		else displs[i] = 0;

		recvcnts[i] = ptt_other[i].domain_size;
	}

	MPI_Type_contiguous(sizeof(Command), MPI_CHAR, &MPI_TYPE_CMD);
	MPI_Type_commit(&MPI_TYPE_CMD);

	MPI_Type_contiguous(sizeof(heat_t), MPI_CHAR, &MPI_TYPE_HEAT_T);
	MPI_Type_commit(&MPI_TYPE_HEAT_T);

	return true;
}

int MPISimEngine::neighbor(int dir)
{
	int x = ptt.col_id + DIR[dir][0],
		y = ptt.row_id + DIR[dir][1];
	if (x < 0 || x >= ptt.ncol || y < 0 || y >= ptt.nrow)
		return -1;
	return proc_id + DIR[dir][0] * ptt.nrow
		+ DIR[dir][1];
}


void MPISimEngine::mst_bcast_cmd(Command &cmd)
{
	MPI_Bcast(&cmd, 1, MPI_TYPE_CMD, ROOT_PROC, MPI_COMM_WORLD);
}

void MPISimEngine::slv_recv_cmd(Command &cmd)
{
	MPI_Bcast(&cmd, 1, MPI_TYPE_CMD, ROOT_PROC, MPI_COMM_WORLD);
}

int MPISimEngine::iterate(HDSConfig *hdsconf)
{
	if (proc_id != ROOT_PROC)
		return slv_worker(hdsconf);
	Command cmd;
	cmd.type = CommandType::START_NEXT_ITER;
	mst_bcast_cmd(cmd);

	exchange_data();
	do_iterate();
	gather_data();
	if (!seconf.in_test)
	{
		convert_gathered_data(hdsconf);
	}

	return true;
}

void MPISimEngine::convert_gathered_data(HDSConfig *hdsconf)
{
	heat_t *hdata = hdsconf->data;

	for (int i = 0; i < nproc; i ++)
	{
		Partition &p = ptt_other[i];
		heat_t *buf = recvbuf + *(displs + i);
		for (int x = p.start_x, pos = 0; x < p.end_x; x ++)
			for (int y = p.start_y; y < p.end_y; y ++, pos ++)
			{
				hdata[hdsconf->domain.index(x, y)]
					= buf[pos];
			}
	}
}

int MPISimEngine::slv_worker(HDSConfig *)
{
	Command cmd;
	for (; ;)
	{
		slv_recv_cmd(cmd);
		if (cmd.type == CommandType::EXIT)
			break;
		exchange_data();
		do_iterate();
		gather_data();
	}
	return false;
}

void MPISimEngine::exchange_data()
{
	// up-down exchange
	//   even down <-> odd up
	//   odd down <-> even up
	// left-right exchange
	//   even right <-> odd left
	//   odd right <-> even left
	//   
	//   DIR: up down left right

	for (int d = 0; d < 2; d ++) // dir
	{
		int st = d * 2; // start
		int nd = st + (proc_id & 1); // neighbor dir
		int nb = neighbor(nd);
		//mpi_printf("nb:%d,nd:%d", nb, nd);
		if (nb != -1)
			exchange(nb, nd);
		nd = st + ((proc_id + 1) & 1);
		nb = neighbor(nd);
		//mpi_printf("nb:%d,nd:%d", nb, nd);
		if (nb != -1)
			exchange(nb, nd);
	}
}


void MPISimEngine::do_iterate()
{
	// iterate
	for (int i = 1, pnew = 0; i <= ptt.width; i ++)
		for (int j = 1; j <= ptt.height; j ++, pnew ++)
		{
			sassert(pnew < ptt.domain_size);

			heat_t &heat = data_new[pnew];
			heat = 0;
			for (int k = 0; k < 4; k ++)
			{
				int x = i + DIR[k][0],
					y = j + DIR[k][1];
				heat += data[exd.index(x, y)];
			}
			heat *= 0.25;
		}

	for (int i = 0; i < ptt.width; i ++)
		for (int j = 0; j < ptt.height; j ++)
			data[exd.index(i + 1, j + 1)] = data_new[ptt.index(i, j)];
}

void MPISimEngine::gather_data()
{
	if (seconf.in_test)
		return;

	MPI_Gatherv(data_new, ptt.domain_size, MPI_TYPE_HEAT_T,
			recvbuf, recvcnts, displs, MPI_TYPE_HEAT_T,
			ROOT_PROC, MPI_COMM_WORLD);
}

size_t MPISimEngine::get_ex_size(int dir)
{
	if (dir == DIR_UP  || dir == DIR_DOWN)
		return ptt.width;
	return ptt.height;
}

void MPISimEngine::fill_exdata(heat_t *exdata, int ex_to_dir)
{
	const int start_pos[4][2] = {
		{1, 1}, {1, ptt.height}, // up down
		{1, 1}, {ptt.width, 1} // left right
	};
	const int fill_dir[4][2] = {
		{1, 0}, {1, 0},
		{0, 1}, {0, 1}
	};

	size_t p = 0;
	for (int x = start_pos[ex_to_dir][0], y = start_pos[ex_to_dir][1];
			x <= ptt.width && y <= ptt.height;
			x += fill_dir[ex_to_dir][0], y += fill_dir[ex_to_dir][1])
	{
		exdata[p ++] = data[exd.index(x, y)];
	}
	sassert(p == get_ex_size(ex_to_dir));
}

void MPISimEngine::fill_data(heat_t *exdata, int ex_from_dir)
{
	const int start_pos[4][2] = {
		{1, 0}, {1, ptt.height + 1}, // up down
		{0, 1}, {ptt.width + 1, 1} // left right
	};
	const int fill_dir[4][2] = {
		{1, 0}, {1, 0},
		{0, 1}, {0, 1}
	};

	size_t p = 0, amount = get_ex_size(ex_from_dir);
	int dx = fill_dir[ex_from_dir][0],
		dy = fill_dir[ex_from_dir][1];
	for (int x = start_pos[ex_from_dir][0], y = start_pos[ex_from_dir][1];
			p < amount;
			x += dx, y += dy)
	{
		data[exd.index(x, y)] = exdata[p ++];
	}

#ifdef DEBUG
	if (p != get_ex_size(ex_from_dir))
	{
		int x = start_pos[ex_from_dir][0], y = start_pos[ex_from_dir][1];
		mpi_printf("x: %d, y: %d, dx: %d, dy: %d\n", x, y, fill_dir[ex_from_dir][0], fill_dir[ex_from_dir][1]);
		mpi_printf("ptt.width: %d, ptt.height: %d\n", ptt.width, ptt.height);
		mpi_printf("%d, %lu, %lu\n", ex_from_dir, p, get_ex_size(ex_from_dir));
	}
#endif
	sassert(p == get_ex_size(ex_from_dir));
}

void MPISimEngine::send_data(heat_t *data, size_t size, int pid)
{
	MPI_Send(data, size, MPI_TYPE_HEAT_T, pid, MPITag::TAG_EXCHANGE, MPI_COMM_WORLD);
}

void MPISimEngine::recv_data(heat_t *data, size_t size, int pid)
{
	MPI_Recv(data, size, MPI_TYPE_HEAT_T, pid, MPITag::TAG_EXCHANGE, MPI_COMM_WORLD, &status_dummy);
}

void MPISimEngine::exchange(int nb, int nd)
{
	//MPI_SCPLGR();
	size_t data_size = get_ex_size(nd);
	heat_t *exdata = new heat_t[data_size];

	//mpi_printf("proc: %d, nb: %d, nd: %d", proc_id, nb, nd);
	if (proc_id < nb)
	{
		fill_exdata(exdata, nd);
		send_data(exdata, data_size, nb);
		recv_data(exdata, data_size, nb);
		fill_data(exdata, nd);
	}
	else
	{
		recv_data(exdata, data_size, nb);
		fill_data(exdata, nd);
		fill_exdata(exdata, nd);
		send_data(exdata, data_size, nb);
	}

	delete [] exdata;
}

MPISimEngine::Partition MPISimEngine::calculate_partition(int proc_id, int nproc)
{
	Partition part;
	real_t min_cost = std::numeric_limits<real_t>::max();

	const Domain &domain = seconf.domain;
	for (int p = 1; p * p <= nproc; p ++)
		if (nproc % p == 0)
		{
			int q = nproc / p;
			real_t cost = domain.height * (p - 1) + 
				domain.width * (q - 1);
			if (cost < min_cost)
			{
				min_cost = cost;
				part.ncol = p;
				part.nrow = q;
			}
			cost = domain.height * (q - 1) + 
				domain.width * (p - 1);
			if (cost < min_cost)
			{
				min_cost = cost;
				part.ncol = q;
				part.nrow = p;
			}
		}

	part.height = domain.height / part.nrow;
	part.width = domain.width / part.ncol;
	//mpi_printf("part.height: %d, part.width: %d\n", part.height, part.width);

	part.col_id = proc_id / part.nrow;
	part.row_id = proc_id % part.nrow;

	part.start_y = part.height * part.row_id;
	part.start_x = part.width * part.col_id;
	part.end_y = part.height * (part.row_id + 1);
	part.end_x = part.width * (part.col_id + 1);

	if (part.col_id == part.ncol - 1)
		part.end_x = domain.width;
	if (part.row_id == part.nrow - 1)
		part.end_y = domain.height;

	part.height = part.end_y - part.start_y;
	part.width = part.end_x - part.start_x;
	//mpi_printf("part.height: %d, part.width: %d\n", part.height, part.width);

	part.domain_size = part.height * part.width;

	return part;
}

void MPISimEngine::print_partition()
{
	printf("----- Print Partition Start -----\n");
	printf("proc: %d\n", proc_id);
	printf("ncol: %d, nrow: %d\n", ptt.ncol, ptt.nrow);
	printf("col_id: %d, row_id: %d\n", ptt.col_id, ptt.row_id);
	printf("width: %d, height: %d\n", ptt.width, ptt.height);
	printf("start_x: %d, start_y: %d\n", ptt.start_x, ptt.start_y);
	printf("end_x: %d, end_y: %d\n", ptt.end_x, ptt.end_y);
	printf("----- Print Partition End -----\n");
}

MPISimEngine::MPISimEngine()
{
	ptt_other = NULL;
	data = NULL;
	data_new = NULL;
	recvbuf = NULL;
	recvcnts = NULL;
	displs = NULL;
}

MPISimEngine::~MPISimEngine()
{
	mpi_printf("Finalize");
	Command cmd;
	cmd.type = CommandType::EXIT;
	mst_bcast_cmd(cmd);

	delete [] ptt_other;
	delete [] data;
	delete [] data_new;
	delete [] recvbuf;
	delete [] recvcnts;
	delete [] displs;

	MPI_Finalize();
}

#endif
/**
 * vim: syntax=cpp11 foldmethod=marker
 */


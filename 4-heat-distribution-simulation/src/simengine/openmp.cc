/*
 * $File: openmp.cc
 * $Date: Tue Sep 04 23:34:26 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifdef ENABLE_OPENMP
#include "simengine/openmp.hh"

#include <cstdio>
#include <cstring>


int OpenMPSimEngine::init(SimEngineConf &seconf)
{
	this->seconf = seconf;
	data = new heat_t[seconf.domain.size()];
	nworker = seconf.nworker;

	return true;
}

int OpenMPSimEngine::iterate(HDSConfig *hdsconf)
{
	const Domain &domain = seconf.domain;

#define IN_DOMAIN(x, y) \
	((x) >= 0 && (y) >= 0 && (x) < (int)domain.width && (y) < (int)domain.height)

	heat_t *hdata;
	if (seconf.in_test)
	{
		hdata = data;
	}
	else hdata = hdsconf->data;
	static const int dir[8][2] = {
		{1, 0}, {0, 1}, {0, -1}, {-1, 0},
		{1, -1}, {-1, 1}, {1, 1}, {-1, -1}
	};

	size_t i;
#pragma omp parallel for num_threads(seconf.nworker) schedule(dynamic) 
	for (i = 0; i < domain.width; i ++)
	{
		//for (size_t j = 0; j < domain.height; j ++) 
		size_t j = 0;
		while (j < domain.height)
		{
			do
			{
				int p = domain.index(i, j);
				heat_t heat = 0;
				for (int k = 0; k < 4; k ++)
				{
					int x = i + dir[k][0],
						y = j + dir[k][1];
					if (IN_DOMAIN(x, y))
						heat += hdata[domain.index(x, y)];
				}
				if (seconf.heat_diff_type == HeatDiffusionType::FOUR_DIR)
				{
					data[p] = heat * 0.25;
					break;
				}

				for (int k = 4; k < 8; k ++)
				{
					int x = i + dir[k][0],
						y = j + dir[k][1];
					if (IN_DOMAIN(x, y))
						heat += hdata[domain.index(x, y)] * 0.5;
				}
				data[p] = heat * (1.0 / 6.0);
			} while (0);
			j ++;
		}
	}

	if (!seconf.in_test)
		memcpy(hdata, data, sizeof(heat_t) * domain.size());

	return true;
}

#endif // ENABLE_OPENMP
/**
 * vim: syntax=cpp11 foldmethod=marker
 */


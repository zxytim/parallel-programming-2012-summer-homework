/*
 * $File: pthread.cc
 * $Date: Wed Sep 05 01:57:44 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#ifdef ENABLE_PTHREAD

#include "simengine/pthread.hh"

#ifdef DEBUG
#include "simengine/openmp.hh"
#endif

#include "lib/log.hh"

#include <pthread.h>
#include <unistd.h>
#include <cstdio>
#include <cassert>
#include <cmath>
#include <cstring>
#include <cstdlib>

int PthreadSimEngine::init(SimEngineConf &seconf)
{
	this->seconf = seconf;
	data = new heat_t[seconf.domain.size()];
	nworker = seconf.nworker;

	return true;
}
static void *__pthread_call_wrapper(void *arg_)
{
	PthreadArg *arg = static_cast<PthreadArg *>(arg_);
	arg->engine->thread_worker(arg->tid);
	pthread_exit(NULL);
}

static int get_nCPU()
{
	return sysconf(_SC_NPROCESSORS_ONLN);
}

int PthreadSimEngine::iterate(HDSConfig *hdsconf)
{
	this->hdsconf = hdsconf;
	const int &nworker = seconf.nworker;
	const Domain &domain = seconf.domain;
	if (threads == NULL)
	{
		commander.set_ntf_max((int)floor(ceil(domain.size() / (real_t)nworker)));
		commander.set_nworker(seconf.nworker);

		threads = new pthread_t[nworker];
		targs = new PthreadArg[nworker];
		attr = new pthread_attr_t[nworker];
		cpuset = new cpu_set_t[nworker];

		for (int i = 0; i < nworker; i ++)
		{
			PthreadArg *arg = targs + i;
			arg->engine = this;
			arg->tid = i;

			pthread_attr_init(&attr[i]);
			if (cpu_affinity)
			{
				if (nworker < get_nCPU())
				{
					// assign thread to cpu
					CPU_ZERO(&cpuset[i]);
					CPU_SET(i, &cpuset[i]);
					pthread_attr_setaffinity_np(&attr[i], sizeof(cpu_set_t), &cpuset[i]);
				}
			}
			pthread_create(threads + i, &attr[i],
					__pthread_call_wrapper, arg);
		}
	}

	if (seconf.in_test)
		hdata = data;
	else 
	{
		hdata = hdsconf->data;
	}
	commander.wait_for_new_task_finish(domain);
	if (!seconf.in_test)
	{
		memcpy(hdata, data, sizeof(heat_t) * domain.size());
	}

	return true; 
}

void PthreadSimEngine::on_exit()
{
	commander.set_exit();
	for (int i = 0; i < seconf.nworker; i ++)
		pthread_join(threads[i], NULL);
	delete [] targs;
	if (attr)
		delete [] attr;
	if (cpuset)
		delete [] cpuset;
	delete [] threads;
}

static void check_hdata(HDSConfig *conf)
{
	for (size_t i = 0; i < conf->domain.size(); i ++)
		assert(conf->data[i] > 0.99);
}

void PthreadSimEngine::thread_worker(int tid)
{
	Command cmd;
	cmd.tid = tid;
	const Domain &domain = seconf.domain;

	static const int dir[8][2] = {
		{1, 0}, {0, 1}, {0, -1}, {-1, 0},
		{1, -1}, {-1, 1}, {1, 1}, {-1, -1}
	};

#define IN_DOMAIN(x, y) \
	((x) >= 0 && (y) >= 0 && (x) < (int)domain.width && (y) < (int)domain.height)

	//char str[100];
	//sprintf(str, "tid-%d.log", tid);
	//FILE *f = fopen(str, "w");
	for (; ;)
	{
		commander.get_command(cmd);
		if (cmd.type == CommandType::NEW_TASK)
		{
			//log_printf("tid: %d, type: %d, %d %d\n", tid, cmd.type, cmd.start, cmd.end);
			size_t i = cmd.start / domain.height,
				   j = cmd.start - i * domain.height,
				   p = cmd.start;
			while ((int)p < cmd.end)
			{
				do
				{
					heat_t heat = 0;
					for (int k = 0; k < 4; k ++)
					{
						int x = i + dir[k][0],
							y = j + dir[k][1];
						if (IN_DOMAIN(x, y))
							heat += hdata[domain.index(x, y)];
					}
					if (seconf.heat_diff_type == HeatDiffusionType::FOUR_DIR)
					{
						data[p] = heat * 0.25;
						break;
					}

					for (int k = 4; k < 8; k ++)
					{
						int x = i + dir[k][0],
							y = j + dir[k][1];
						if (IN_DOMAIN(x, y))
							heat += hdata[domain.index(x, y)] * 0.5;
					}
					data[p] = heat * (1.0 / 6.0);
				} while (0);

				j ++;
				if (j == domain.height)
					i ++, j = 0;
				p ++;
			}
		}
		else if (cmd.type == CommandType::EXIT)
		{
			log_printf("exit command received");
			break;
		}
	}
	//fclose(f);
}

PthreadSimEngine::PthreadSimEngine()
{
	cpu_affinity = true;
	attr = NULL;
	cpuset = NULL;
	threads = NULL;
}

PthreadSimEngine::~PthreadSimEngine()
{
	log_printf("PthreadSimEngine destructor called");
	on_exit();
}

void PthreadSimEngine::Commander::set_ntf_max(int ntf)
{
	ntf_max = ntf;
}

void PthreadSimEngine::Commander::set_nworker(int nworker)
{
	this->nworker = nworker;
	if (worker_status != NULL)
		delete [] worker_status;
	worker_status = new int[nworker];
}

void PthreadSimEngine::Commander::wait_for_new_task_finish(const Domain &domain)
{
	pthread_mutex_lock(&mutex);

	ntask = domain.size();
	cur = 0;
	status = STATUS_DIST_TASK;
	memset(worker_status, 0, sizeof(worker_status[0]) * nworker);
	nworker_finished = 0;

	pthread_cond_wait(&cond, &mutex);

	pthread_mutex_unlock(&mutex);
}

void PthreadSimEngine::Commander::set_exit()
{
	pthread_mutex_lock(&mutex);
	status = STATUS_EXIT;
	pthread_mutex_unlock(&mutex);
}

void PthreadSimEngine::Commander::get_command(Command &cmd)
{
	pthread_mutex_lock(&mutex);
	if (status == STATUS_DIST_TASK)
	{
		cmd.type = CommandType::NEW_TASK;
		cmd.start = cur;
		cmd.end = (cur += ntf_max);
		//log_printf("%d %d", cmd.start, cmd.end);
		if (cmd.end > ntask)
		{
			cmd.end = ntask;
			status = STATUS_NO_TASK;
			if (cmd.start == cmd.end)
				cmd.type = CommandType::WAIT;
		}
	}
	else if (status == STATUS_STARTUP)
	{
		cmd.type = CommandType::WAIT;
	}
	else if (status == STATUS_NO_TASK)
	{
		cmd.type = CommandType::WAIT;
		if (worker_status[cmd.tid] != WSTATUS_FINISHED)
		{
			worker_status[cmd.tid] = WSTATUS_FINISHED;
			nworker_finished ++;
			if (nworker_finished == nworker)
				pthread_cond_signal(&cond);
		}
	}
	else // STATUS_EXIT
	{
		cmd.type = CommandType::EXIT;
	}
	pthread_mutex_unlock(&mutex);
}

PthreadSimEngine::Commander::Commander()
{
	pthread_mutex_init(&mutex, NULL);
	pthread_cond_init(&cond, NULL);
	status = STATUS_STARTUP;
	worker_status = NULL;
}

PthreadSimEngine::Commander::~Commander()
{
	pthread_mutex_destroy(&mutex);
	pthread_cond_destroy(&cond);
	delete [] worker_status;
}


#endif // ENABLE_PTHREAD
/**
 * vim: syntax=cpp11 foldmethod=marker
 */


/*
 * $File: tester.cc
 * $Date: Mon Sep 03 21:22:38 2012 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include "tester.hh"

#include "lib/timer.hh"
#include "lib/log.hh"

TestResult Tester::test(TestConfig &config)
{
	config.engine->init(config.seconf);

	TestResult rst;
	rst.niter = 0;
	rst.engine_name = config.engine->name();
	rst.nworker = config.seconf.nworker;
	rst.domain = config.seconf.domain;

	Timer timer;
	timer.begin();

	log_printf("test start!");
	log_printf("parallel lib: %s", config.engine->name().c_str());
	log_printf("nworker: %d", config.engine->get_nworker());
	log_printf("duration: %.2Lfs", config.test_duration);
	while (timer.end() < config.test_duration * 1000)
	{
		bool ret = config.engine->iterate(config.hdsconf);
		if (!ret)
		{
			rst.valid = false;
			break;
		}
		rst.niter ++;
	}

	rst.valid = true;
	rst.test_duration = timer.end() / 1000.0;
	rst.niter_per_sec = rst.niter / rst.test_duration;

	return rst;
}

/**
 * vim: syntax=cpp11 foldmethod=marker
 */


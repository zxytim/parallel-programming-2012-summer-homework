# Parallel Programming 2012 Summer #

Overview
========

	This is the homework compilation for Parallel Programming
	course in 2012 (legendary) summer semester, Tsinghua University.

	Teacher is [**Chung**](http://www.cs.nthu.edu.tw/~ychung/)
	from **National Tsinghua University**, Taiwan

	Course resources are available [here](http://www.cs.nthu.edu.tw/~ychung/syllabus/para_programming.htm) (Heat Distribution Simulation may miss out)
